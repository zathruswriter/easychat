authors: [martinambrus]
description: go crazy with your chat!
main: com.martinambrus.easyChat.EasyChat
name: EasyChat
load: postworld
version: 1.0.8
api-version: 1.13
depend: []
softdepend: [Vault, PlaceholderAPI, Multiverse-Core, Jobs, DiscordSRV]
permissions:
  ec.join_channel:
    default: true
    description: Allows joining channels and listing available channels on the server.
  ec.subscribe:
    default: true
    description: Parent permission for subscribe and unsubscribe commands.
    children:
      ec.subscribe_channel: true
      ec.unsubscribe_channel: true
  ec.subscribe_channel:
    default: true
    description: Allows players to subscribe to channels with this option enabled in config.
  ec.unsubscribe_channel:
    default: true
    description: Allows players to unsubscribe from channels to which they were previously subscribed.
  ec.isolate:
    default: true
    description: Allows players to focus on a single channel only, disabling output from all other channels they are subscribed to.
  ec.tell:
    default: true
    description: Allows sending private messages to another player.
  ec.ignore:
    default: true
    description: Allows players to ignore other players.
  ec.rules.show:
    default: true
    description: Allows showing rules of the server via /ec_rules (or /rules) command.
  ec.motd.show:
    default: true
    description: Allows showing MOTD (Message of the Day) via /ec_motd (or /motd) command.
  ec.mention-sound-notification:
    default: true
    description: Allows receiving sound notification when private messaged or mentioned in chat.
  ec.say:
    default: op
    description: Allows broadcasting messages to all players on the server.
  ec.ban:
    default: op
    description: Allows banning players from channels.
  ec.unban:
    default: op
    description: Allows un-banning players from channels.
  ec.mute:
    default: op
    description: Allows muting players in channels.
  ec.unmute:
    default: op
    description: Allows unmuting players in channels.
  ec.mutec:
    default: op
    description: Allows muting channels.
  ec.unmutec:
    default: op
    description: Allows unmuting channels.
  ec.reload:
    default: op
    description: Allows reloading of EasyChat configuration.
  ec.notifynewversion:
    default: op
    description: Allows for chat notifications on player join when a new version of EasyChat is available for download.
  ec.showversion:
    default: op
    description: Allows displaying current EasyChat version via ec_version.
  ec.bypass:
    default: op
    description: Parent permission for all following bypass permissions in EasyChat.
    children:
      ec.bypass.muted: true
      ec.bypass.player.muted: true
      ec.bypass.player.banned: true
      ec.bypass.cooldown: true
      ec.bypass.password: true
      ec.bypass.command-spy-commands: true
      ec.bypass.move-to-chat: true
      ec.bypass.channel-join-leave-notification: true
  ec.bypass.muted:
    default: op
    description: Allows talking in muted channels.
  ec.bypass.player.muted:
    default: op
    description: Allows talking in channels where the player is muted.
  ec.bypass.player.banned:
    default: op
    description: Allows joinning channels from which the player is banned.
  ec.bypass.cooldown:
    default: op
    description: Allows ignoring messages cooldowns in channels.
  ec.bypass.password:
    default: op
    description: Allows joining password-protected channels without using a password.
  ec.bypass.command-spy-commands:
    default: op
    description: Allows seeing commands that are excluded from the list of commands to spy on with the command spy module.
  ec.bypass.move-to-chat:
    default: op
    description: Allows sending chat and commands before a player has moved on the server and the move-to-chat module is active.
  ec.bypass.channel-join-leave-notification:
    default: op
    description: Suppresses join and leave channel notifications for this user.
  ec.commandspy:
    default: op
    description: Allows receiving info about commands run by other players.
  ec.channels.spy.subscribe:
    default: op
    description: Allows subscribing to the global Spy channel.
  ec.silent.join:
    default: op
    description: Suppresses the join message when player joins the server, essentially hiding them joining the server.
  ec.silent.leave:
    default: op
    description: Suppresses the leave message when player leaves the server, essentially hiding them leaving the server.
  ec.debug:
    default: op
    description: Allows usage of the /ec_debug command.
commands:
  ec_join:
    description: Joins a channel (with a password if one is required).
    usage: /<command> <channel> [password]
    permission: ec.join_channel
    permission-message: This is currently disabled.
    aliases:
    - join
    - ch
  ec_subscribe:
    description: Subscribes to a channel, so you can hear messages from it while chatting in a different channel.
    usage: /<command> <channel> [password]
    permission: ec.subscribe_channel
    permission-message: This is currently disabled.
    aliases:
      - subscribe
      - sub
  ec_unsubscribe:
    description: Unsubscribes from a channel.
    usage: /<command> <channel>
    permission: ec.unsubscribe_channel
    permission-message: This is currently disabled.
    aliases:
      - unsubscribe
      - unsub
  ec_isolate:
    description: Puts you into a single channel, pausing (or cancelling - based on server settings) all your subscriptions. Brings everything back when used without a parameter (and if enabled on server).
    usage: /<command> [channel]
    permission: ec.isolate
    permission-message: This is currently disabled.
    aliases:
      - isolate
      - focus
  ec_say:
    description: Broadcasts a message to all players on the server.
    usage: /<command> <message>
    permission: ec.say
    permission-message: This is currently disabled.
    aliases:
      - say
  ec_tell:
    description: Sends a private message to another player.
    usage: /<command> <Player> <message>
    permission: ec.tell
    permission-message: This is currently disabled.
    aliases:
      - tell
      - msg
  ec_ban:
    description: Bans player from accessing a channel.
    usage: /<command> <Channel> <Player>
    permission: ec.ban
    permission-message: This is currently disabled.
    aliases:
      - eban
  ec_unban:
    description: Un-bans player from a channel.
    usage: /<command> <Channel> <Player>
    permission: ec.unban
    permission-message: This is currently disabled.
    aliases:
      - eunban
  ec_mute:
    description: Mutes player in channel.
    usage: /<command> <Channel> <Player>
    permission: ec.mute
    permission-message: This is currently disabled.
    aliases:
      - mute
  ec_unmute:
    description: Un-mutes player in channel.
    usage: /<command> <Channel> <Player>
    permission: ec.unmute
    permission-message: This is currently disabled.
    aliases:
      - unmute
  ec_mutec:
    description: Mutes a channel.
    usage: /<command> <Channel>
    permission: ec.mutec
    permission-message: This is currently disabled.
    aliases:
      - mutech
  ec_unmutec:
    description: Un-mutes a channel.
    usage: /<command> <Channel>
    permission: ec.unmutec
    permission-message: This is currently disabled.
    aliases:
      - unmutech
  ec_ignore:
    description: Ignores or un/ignores a player.
    usage: /<command> <Player>
    permission: ec.ignore
    permission-message: This is currently disabled.
    aliases:
      - ignore
  ec_rules:
    description: Shows server rules.
    usage: /<command>
    permission: ec.rules.show
    permission-message: This is currently disabled.
    aliases:
      - rules
  ec_motd:
    description: Shows Message of the Day.
    usage: /<command>
    permission: ec.motd.show
    permission-message: This is currently disabled.
    aliases:
      - motd
  ec_version:
    description: Shows current EasyChat version.
    usage:  /<command>
    permission: ec.showversion
    permission-message: This is currently disabled.
    aliases: []
  ec_reload:
    description: Reloads EasyChat configuration
    usage:  /<command>
    permission: ec.reload
    permission-message: This is currently disabled.
    aliases: []
  ec_debug:
    description: Turns debugging for EasyChat on or off.
    usage:  /<command>
    permission: ec.debug
    permission-message: This is currently disabled.
    aliases: []