package com.martinambrus.easyChat;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Constants used across EasyChat.
 *
 * @author Martin Ambrus
 */
public enum Constants implements Iterable<String> {

    /**
     * Regex to check if String is a number.
     */
    INT_REGEX(
        "(-)?(\\d){1,10}(\\.(\\d){1,10})?" //NON-NLS
            );

    /**
     * The string value of an ENUM.
     */
    private String constantValue;

    /**
     * The list of strings value of an ENUM.
     */
    private List<String> constantListValue;

    /**
     * Constructor for String ENUMs.
     * @param value String value for the ENUM.
     */
    Constants(final String value) {
        this.constantValue = value;
    } //end method

    /**
     * Constructor for list of strings ENUMs.
     * @param value List value for the ENUM.
     */
    Constants(final List<String> value) {
        this.constantListValue = value;
    } //end method

    /**
     * Returns the iterator for a list-type constant.
     *
     * @return Returns the iterator for a list-type constant.
     */
    @Override
    public Iterator<String> iterator() {
        return this.constantListValue.iterator();
    } //end method

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return this.constantValue;
    } // end method

    /**
     * Returns a list-type constant.
     *
     * @return Returns a List of strings which is the value of the given requested config element.
     */
    public List<String> getValues() {
        return Collections.unmodifiableList(this.constantListValue);
    } // end method

    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
        throw new java.io.NotSerializableException("com.martinambrus.easyChat.Constants");
    }
} // end class