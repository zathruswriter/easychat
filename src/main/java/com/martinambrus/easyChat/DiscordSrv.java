package com.martinambrus.easyChat;

import com.martinambrus.easyChat.events.ECChatEvent;
import github.scarsz.discordsrv.DiscordSRV;
import github.scarsz.discordsrv.api.ListenerPriority;
import github.scarsz.discordsrv.api.Subscribe;
import github.scarsz.discordsrv.api.events.DiscordGuildMessagePreProcessEvent;
import github.scarsz.discordsrv.api.events.DiscordReadyEvent;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import github.scarsz.discordsrv.util.DiscordUtil;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Implementing DiscordSRV communication.
 *
 * @author Martin Ambrus
 */
@SuppressWarnings({"OverlyComplexClass", "VariableNotUsedInsideIf"})
final class DiscordSrv implements Listener {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * Holds EasyChat channel to DiscordSrv channel relations,
     * so we can send out messages send via EasyChat.
     */
    private final Map<String, TextChannel> ec_channels_to_discord_channels = new HashMap<>();

    /**
     * Constructor.
     * @param ec Instance of {@link EasyChat EasyChat}.
     */
    DiscordSrv( final Plugin ec) {
        plugin = ec;

        try {
            DiscordSRV.api.subscribe( this );
            EC_API.startRequiredListener( "discordsrv", this );

            // link channels only if DiscordSRV already has them loaded
            // ... this is the case when we're reloading EasyChat, so we don't get to
            //     the DiscordReadyEvent event below
            if ( null != DiscordSRV.getPlugin().getMainTextChannel() ) {
                this.pair_channels();
            }

        } catch (NoClassDefFoundError ex) {
            EC_API.generateConsoleWarning(
                "\n\n"
                    + "**************************************************\n"
                    + "** " + EC_API.__("error.discordsrv-not-found.1") + "\n"
                    + "** " + EC_API.__("error.discordsrv-not-found.2") + '\n'
                    + "**************************************************\n");
        }
    } //end method

    /**
     * Pairs EasyChat and DiscordSrv channels,
     * so chat can be sent out from MC server to Discord server.
     */
    private void pair_channels() {
        for ( Channel channel : EC_API.get_all_channels() ) {
            TextChannel textChannel = DiscordSRV.getPlugin().getDestinationTextChannelForGameChannelName( channel.getName() );
            if (null != textChannel) {
                this.ec_channels_to_discord_channels.put( channel.getName(), textChannel );
            } else {
                // let the user know they have an EasyChat channel that doesn't have
                // a matching DiscordSrv channel
                EC_API.generateConsoleWarning( EC_API.__("error.discordsrv-channel-not-found", channel.getName() ) );
            }
        }
    } // end method

    /**
     * When the DiscordSrv bot is ready to receive messages,
     * we'll tell owner in console.
     * Also, we'll try to match all of our channels with DiscordSrv ones.
     *
     * @param event The DiscordSrv ready event we're waiting for.
     */
    @Subscribe
    public void discordReadyEvent( DiscordReadyEvent event) {
        Bukkit.getLogger().info( "[" + EC_API.getEcName() + "] " + EC_API.__("discordsrv.connection-ready", DiscordUtil.getJda().getUsers().size() ) );
        this.pair_channels();
    } // end method

    /**
     * Reacts to messages received from Discord and makes sure
     * that they only get delivered to users in the right channels.
     *
     * @param event The Discord message receive event to work with.
     */
    @Subscribe(priority = ListenerPriority.HIGHEST)
    public void discordMessageIncoming( DiscordGuildMessagePreProcessEvent event) {
        // implicitly cancel the message, so it doesn't get displayed in chat
        event.setCancelled( true );

        // try to match it with a channel for it to go to
        TextChannel discordChannel = event.getChannel();

        // we *could* use Discord's formatting options and replace their placeholders with relevant
        // information but for now, let's keep things simple...
        String message = "[&bDiscord&r][&e" + event.getChannel().getName() + "&r] " + event.getAuthor().getName() + " > " + event.getMessage().getContentDisplay();
        message = Utils.translate_chat_colors( message );

        if ( this.ec_channels_to_discord_channels.containsValue( discordChannel ) ) {
            Channel channel = null;
            for ( Map.Entry<String, TextChannel> entry : this.ec_channels_to_discord_channels.entrySet() ) {
                if ( entry.getValue().equals( discordChannel ) ) {
                    channel = EC_API.get_channel_by_name( entry.getKey() );
                    break;
                }
            }

            // could not find a suitable channel
            if ( null == channel ) {
                return;
            }

            // get all players on server and check whether to send this message to them
            for ( Player player : Bukkit.getOnlinePlayers() ) {
                UUID player_uuid = player.getUniqueId();
                if (
                    EC_API.get_player_channel( player_uuid ).equals( channel )
                    ||
                    EC_API.is_subscribed_to_channel( player_uuid, channel )
                ) {
                    player.sendMessage( message );
                }
            }

            // log this message into console
            Bukkit.getConsoleSender().sendMessage( message );
        }
    } // end method

    /***
     * Sends out the message received from chat to DiscordSrv bot.
     *
     * @param e The actual event to work with.
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void sendOutChat(final ECChatEvent e) {
        if ( e.isCancelled() ) {
            return;
        }

        String channel_name = e.getChannelName();
        if ( this.ec_channels_to_discord_channels.containsKey( channel_name ) ) {
            this.ec_channels_to_discord_channels.get( channel_name ).sendMessage( "[" + e.getPlayer().getName() + "] " + e.getOriginalMessage() ).queue();
        }
    } // end method

    /**
     * Disconnects EasyChat from DiscordSrv on plugin disable.
     */
    public void onDisable() {
        DiscordSRV.api.unsubscribe( this );
    } // end method

} // end class