package com.martinambrus.easyChat.listeners;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.EasyChat;
import com.martinambrus.easyChat.Utils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

import java.io.File;

/**
 * Listens to player joining and displays a MOTD,
 * if enabled.
 *
 * @author Martin Ambrus
 */
public class motdJoinListener implements Listener {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * Constructor, saves the plugin instance locally and loads
     * excluded commands, if any are set.
     */
    public motdJoinListener( final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * Display MOTD message for the player, if enabled.
     *
     * @param e The actual player join event to work with.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void updateJoinMessage(final PlayerJoinEvent e) {
        // don't update anything if the module is disabled
        if ( !EC_API.isModuleEnabled( "motd" ) ) {
            return;
        }

        // if the MOTD file does not exist yet, save it from our plugin's resources
        String motd_file_name = ( e.getPlayer().hasPlayedBefore() ? "motd.txt" : "motd-first-join.txt" );
        File motd_file = new File( EC_API.getEcDataDir(), motd_file_name );
        if ( !motd_file.exists() ) {
            this.plugin.saveResource(motd_file_name, true);
            EC_API.generateConsoleWarning( EC_API.__( e.getPlayer().hasPlayedBefore() ? "motd.creating-new-file" : "motd.creating-new-first-join-file" ) );
        }

        // read MOTD from the file
        String motd = Utils.read_file( EC_API.getEcDataDir() + File.separatorChar + motd_file_name, true );
        if ( !"".equals( motd ) ) {
            // replace translation placeholders
            motd = Utils.replace_translation_placeholders( motd );

            // replace our own + PAPI placeholders
            motd = EC_API.replace_placeholders( motd, e.getPlayer(), null );

            // send the MOTD
            e.getPlayer().sendMessage( motd );
        }
    } // end method

} // end class