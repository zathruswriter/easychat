package com.martinambrus.easyChat.listeners;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.EasyChat;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.util.*;

/**
 * Listens to player chat events as well as command events
 * from players and disallows them if the player did not
 * move from the moment they joined the server.
 *
 * @author Martin Ambrus
 */
public class moveToChatListener implements Listener {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * Map of players and their initial login position in the world.
     */
    Map<UUID, Location> initial_player_positions = new HashMap<>();

    /**
     * List of commands allowed before player moves.
     */
    List<String> allowed_pre_move_commands = new ArrayList<>();

    /**
     * Constructor, saves the plugin instance locally and loads
     * excluded commands, if any are set.
     */
    public moveToChatListener( final Plugin ec ) {
        this.plugin = ec;
        this.allowed_pre_move_commands = EC_API.getConfigStringList( "modules.move-to-chat.allowed-commands" );

        // make sure the commands all begin with a /
        for ( int i = 0; i < this.allowed_pre_move_commands.size(); i++ ) {
            if ( !this.allowed_pre_move_commands.get( i ).startsWith( "/" ) ) {
                this.allowed_pre_move_commands.set( i,  "/" + this.allowed_pre_move_commands.get( i ) );
            }
        }
    } // end method

    /***
     * Save initial player position.
     *
     * @param e The actual player join event to work with.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void saveJoinLocation(final PlayerJoinEvent e) {
        this.initial_player_positions.put( e.getPlayer().getUniqueId(), e.getPlayer().getLocation() );
    } // end method

    /***
     * Remove initial player position on player leave.
     *
     * @param e The actual player quit event to work with.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void removeJoinLocation(final PlayerQuitEvent e) {
        this.initial_player_positions.remove( e.getPlayer().getUniqueId() );
    } // end method

    /***
     * Listen to player chat and disallow it if the location is the same
     * as the initial player join location.
     *
     * @param e The actual player chat event to work with.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void checkPlayerMovedChat(final AsyncPlayerChatEvent e) {
        // don't worry about anything if player has a bypass permission
        if ( EC_API.checkPerms( e.getPlayer(), "ec.bypass.move-to-chat", false ) ) {
            return;
        }

        Player   player           = e.getPlayer();
        Location initial_location = this.initial_player_positions.get( player.getUniqueId() );

        // if this player is not in the initial map for any reason, scream into console (in debug mode)
        if ( null == initial_location ) {
            if ( EC_API.getDebug() ) {
                EC_API.generateConsoleWarning( "Initial player position for player " + player.getName() + " (" + player.getUniqueId() + ") not found!" );
            }

            return;
        }

        // player didn't move, tell them to do so first
        if ( initial_location.equals( player.getLocation() ) ) {
            player.sendMessage( EC_API.__( "move-to-chat.you-must-move-first" ) );
            e.setCancelled( true );
        }
    } // end method

    /***
     * Listen to player command event and disallow it if the location is the same
     * as the initial player join location.
     *
     * @param e The actual player command event to work with.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void checkPlayerMovedCommand(final PlayerCommandPreprocessEvent e) {
        // don't worry about anything if player has a bypass permission
        if ( EC_API.checkPerms( e.getPlayer(), "ec.bypass.move-to-chat", false ) ) {
            return;
        }

        Player   player           = e.getPlayer();
        Location initial_location = this.initial_player_positions.get( player.getUniqueId() );

        // if this player is not in the initial map for any reason, scream into console (in debug mode)
        if ( null == initial_location ) {
            if ( EC_API.getDebug() ) {
                EC_API.generateConsoleWarning( "Initial player position for player " + player.getName() + " (" + player.getUniqueId() + ") not found!" );
            }

            return;
        }

        // player didn't move, check if they're performing an allowed command
        // and if not, tell them to move
        if ( initial_location.equals( player.getLocation() ) ) {
            String[] cmd = e.getMessage().split( " " );
            cmd[0] = cmd[0].toLowerCase();

            if ( !this.allowed_pre_move_commands.contains( cmd[0] ) ) {
                player.sendMessage( EC_API.__( "move-to-chat.you-must-move-first" ) );
                e.setCancelled( true );
            }
        }
    } // end method

} // end class