package com.martinambrus.easyChat.listeners;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.EasyChat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Listens to player commands and displays them in chat
 * for players with permission.
 *
 * @author Martin Ambrus
 */
public class commandSpy implements Listener {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * A fast-search map of all commands that are not to be spied on,
     * unless the ec.bypass.command-spy-commands is used.
     */
    Map<String, Boolean> exclusions = new HashMap<>();

    /**
     * Constructor, saves the plugin instance locally and loads
     * excluded commands, if any are set.
     */
    public commandSpy(final Plugin ec ) {
        this.plugin = ec;

        for ( String exclusion : EC_API.getConfigStringList("modules.command-spy.excluded-commands") ) {
            this.exclusions.put( exclusion, true );
        }
    } // end method

    /***
     * Display commands executed to players with permission
     * and optionally log them into console as well.
     *
     * @param e The actual player chat event to work with.
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void logCommand(final PlayerCommandPreprocessEvent e) {

        // send this command to each of the players on the server
        // with the right permission
        String command_runner_name = e.getPlayer().getName();

        String clear_command       = e.getMessage().substring(1);
        if ( clear_command.indexOf(' ') > -1 ) {
            clear_command = clear_command.substring(0, clear_command.indexOf(' '));
        }

        String message             = ChatColor.GRAY + "" + ChatColor.ITALIC + "[" + command_runner_name + "] " + e.getMessage();

        for ( Player player : Bukkit.getOnlinePlayers() ) {
            if (
                EC_API.checkPerms( player, "ec.commandspy", false ) &&
                !player.getName().equals( e.getPlayer().getName() ) &&
                (
                    this.exclusions.isEmpty()
                    ||
                    !this.exclusions.containsKey( clear_command )
                    ||
                    EC_API.checkPerms( player, "ec.bypass.command-spy-commands", false )
                )
            ) {
                player.sendMessage( message );
            }
        }

    } // end method

} // end class