package com.martinambrus.easyChat.listeners;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.EasyChat;
import com.martinambrus.easyChat.Utils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

/**
 * Listens to player joining and leaving the server
 * and replaces the join/leave message by the one
 * defined in config, or suppresses it altogether
 * if a user has the right permission.
 *
 * @author Martin Ambrus
 */
public class customJoinLeaveMessage implements Listener {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * Constructor, saves the plugin instance locally and loads
     * excluded commands, if any are set.
     */
    public customJoinLeaveMessage( final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * Custom join/first join message handling.
     *
     * @param e The actual player join event to work with.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void updateJoinMessage(final PlayerJoinEvent e) {
        // handle suppressing the join message if player has the correct permission
        if ( EC_API.checkPerms( e.getPlayer(), "ec.silent.join", false ) ) {
            e.setJoinMessage( "" );
            return;
        }

        if ( EC_API.isModuleEnabled( "custom-join-message" ) ) {
            if ( e.getPlayer().hasPlayedBefore() ) {
                // normal join message
                e.setJoinMessage( EC_API.replace_placeholders( Utils.translate_chat_colors( EC_API.getConfigString( "modules.custom-join-message.format" ) ), e.getPlayer(), null ) );
            } else {
                // first join message
                e.setJoinMessage( EC_API.replace_placeholders( Utils.translate_chat_colors( EC_API.getConfigString( "modules.custom-join-message.format-first-join" ) ), e.getPlayer(), null ) );
            }
        }
    } // end method

    /***
     * Custom leave message handling.
     *
     * @param e The actual player leave event to work with.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void updateLeaveMessage(final PlayerQuitEvent e) {
        // handle suppressing the leave message if player has the correct permission
        if ( EC_API.checkPerms( e.getPlayer(), "ec.silent.leave", false ) ) {
            e.setQuitMessage( "" );
            return;
        }

        if ( EC_API.isModuleEnabled( "custom-leave-message" ) ) {
            e.setQuitMessage( EC_API.replace_placeholders( Utils.translate_chat_colors( EC_API.getConfigString( "modules.custom-leave-message.format" ) ), e.getPlayer(), null ) );
        }
    } // end method

} // end class