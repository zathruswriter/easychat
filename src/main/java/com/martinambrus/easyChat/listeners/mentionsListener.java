package com.martinambrus.easyChat.listeners;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.EasyChat;
import com.martinambrus.easyChat.events.ECChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Listens to player chat events, parses them and notifies
 * all the players mentioned in these messaged.
 *
 * @author Martin Ambrus
 */
public class mentionsListener implements Listener {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * The pattern to use to check for player mentions.
     */
    private Pattern mention_pattern;

    /**
     * Sound to play when a player is mentioned in a message.
     */
    private Sound mention_sound;

    /**
     * Constructor, saves the plugin instance locally and loads
     * excluded commands, if any are set.
     */
    public mentionsListener( final Plugin ec ) {
        this.plugin = ec;

        String mention_character = EC_API.getConfigString( "modules.mention-sound-notification.mention-character" );
        if ( null == mention_character || "".equals( mention_character ) ) {
            mention_character = "@";
        }

        this.mention_pattern = Pattern.compile( "(?i)(?U)" + mention_character + "([^ ]+)" );

        // load the notification sound
        String sound_name = EC_API.getConfigString( "modules.mention-sound-notification.sound" );
        try {
            if ( null != sound_name && !"".equals( sound_name ) && null != Sound.valueOf( sound_name ) ) {
                this.mention_sound = Sound.valueOf( sound_name );
            }
        } catch ( IllegalArgumentException ex ) {
            // sound not found, play a default one
            EC_API.generateConsoleWarning( EC_API.__( "error.chat-sound-notification-not-found", sound_name ) );
            // MC 1.9+
            try {
                this.mention_sound = Sound.valueOf( "BLOCK_NOTE_BLOCK_PLING" );
            } catch ( IllegalArgumentException ex2 ) {
                // MC 1.8
                this.mention_sound = Sound.valueOf( "NOTE_PLING" );
            }
        }
    } // end method

    /***
     * Listen to player chat, check for mentions and notify all relevant players.
     *
     * @param e The actual player chat event to work with.
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void checChatForMentions(final AsyncPlayerChatEvent e) {
        // bail out if the event is cancelled
        if ( e.isCancelled() ) {
            return;
        }

        Matcher matcher = this.mention_pattern.matcher( e.getMessage() );
        while (matcher.find()) {
            Player player = Bukkit.getPlayer( matcher.group( 1 ) );
            if ( null != player && player.isOnline() && EC_API.checkPerms( player, "ec.mention-sound-notification", false ) ) {
                // play a custom sound effect
                player.playSound( player.getEyeLocation(), this.mention_sound, 1, 1 );
            }
        }
    } // end method

    /***
     * Listen to our custom ECChatEvent event, check for mentions and notify all relevant players.
     *
     * @param e The actual ECChatEvent event to work with.
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void checECChatForMentions(final ECChatEvent e) {
        // bail out if the event is cancelled
        if ( e.isCancelled() ) {
            return;
        }

        Matcher matcher = this.mention_pattern.matcher( e.getOriginalMessage() );
        while (matcher.find()) {
            Player player = Bukkit.getPlayer( matcher.group( 1 ) );
            if ( null != player && player.isOnline() && EC_API.checkPerms( player, "ec.mention-sound-notification", false ) ) {
                // play a custom sound effect
                player.playSound( player.getEyeLocation(), this.mention_sound, 1, 1 );
            }
        }
    } // end method

} // end class