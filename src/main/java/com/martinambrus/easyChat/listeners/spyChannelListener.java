package com.martinambrus.easyChat.listeners;

import com.martinambrus.easyChat.Channel;
import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.EasyChat;
import com.martinambrus.easyChat.Utils;
import com.martinambrus.easyChat.events.ECChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;

import java.util.HashSet;
import java.util.Set;

/**
 * Listens to non-cancelled server chat events as well as our own
 * chat events and displays them all to people with appropriate permissions
 * who are subscribed ot switched to the global spy channel.
 *
 * @author Martin Ambrus
 */
public class spyChannelListener implements Listener {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * Constructor. Required due to the way listeners autoloading works.
     */
    public spyChannelListener( final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * Forwards uncancelled message from ordinary MC chat event
     * to the Spy channel to display.
     *
     * @param e The actual player chat event to work with.
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void forwardToSpyChannel(final AsyncPlayerChatEvent e) {
        if ( e.isCancelled() ) {
            return;
        }

        // this has to be run synchronously in main task for newer Spigot versions
        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
            @Override
            public void run() {
                Channel spy_channel = EC_API.get_spy_channel();
                String spy_message = EC_API.replace_placeholders( spy_channel.getFormat(), e.getPlayer(), spy_channel );
                String spy_message_clear = EC_API.replace_placeholders( spy_channel.getFormat_clear(), e.getPlayer(), spy_channel );
                spy_message = spy_message.replace( "{MESSAGE}", e.getMessage() );
                spy_message_clear = spy_message_clear.replace( "{MESSAGE}", e.getMessage() );

                // prepare list of recipients subscribed to or talking in the Spy channel
                Set<Player> recipients = new HashSet<>();
                for ( Player player : Bukkit.getOnlinePlayers() ) {
                    if (
                        // don't show to sender
                        !player.equals( e.getPlayer() ) &&
                        // don't show if this player is also a recipient
                        e.getRecipients().contains( player ) &&
                        // player needs to be subscribed to the global Spy channel
                        EC_API.is_subscribed_to_channel( player.getUniqueId(), spy_channel )
                    ) {
                        recipients.add( player );
                    }
                }

                if ( recipients.size() > 0 ) {
                    ECChatEvent chatEvent = new ECChatEvent( spy_message, spy_message_clear, e.getMessage(), e.getPlayer(), recipients, spy_channel.getName() );
                    Bukkit.getPluginManager().callEvent( chatEvent );
                }
            }
        });
    } // end method

    /***
     * Forwards all of our own chat messages to the Spy channel to display.
     *
     * @param e The actual event to work with.
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void sendOutChat(final ECChatEvent e) {
        if (
            e.isCancelled() ||
            e.getChannelName().equals( EC_API.get_spy_channel().getName() ) ||
            // no need to spy on chat that's visible to everyone
            e.getChannelName().equals( EC_API.get_broadcast_channel().getName() )
        ) {
            return;
        }

        // this has to be run synchronously in main task for newer Spigot versions
        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
            @Override
            public void run() {
                Channel spy_channel = EC_API.get_spy_channel();
                boolean is_private_channel = EC_API.get_private_channel().equals( EC_API.get_channel_by_name( e.getChannelName() ) );
                String spy_message = Utils.translate_chat_colors( EC_API.replace_placeholders( spy_channel.getFormat(), e.getPlayer(), spy_channel ) );
                String spy_message_clear = Utils.translate_chat_colors( EC_API.replace_placeholders( spy_channel.getFormat_clear(), e.getPlayer(), spy_channel ) );
                spy_message = spy_message.replace( "{MESSAGE}", Utils.remove_chat_color_values( e.getLogMessage() ) );
                spy_message_clear = spy_message_clear.replace( "{MESSAGE}", Utils.remove_chat_color_values( e.getLogMessage() ) );

                // prepare list of recipients subscribed to or talking in the Spy channel
                Set<Player> recipients = new HashSet<>();
                for ( Player player : Bukkit.getOnlinePlayers() ) {
                    if (
                        // don't show to sender
                        !player.equals( e.getPlayer() ) &&
                        // don't show if this player is also a recipient
                        !e.getRecipients().contains( player ) &&
                        // player needs to be subscribed to the global Spy channel
                        EC_API.is_subscribed_to_channel( player.getUniqueId(), spy_channel ) &&
                        // don't show if this is a private message via /tell or /msg and we can see the command
                        (
                            // not a private channel, show this message
                            !is_private_channel ||
                            // private channel but command spy disabled, show this message
                            !EC_API.isModuleEnabled( "command-spy" ) ||
                            // command spy enabled, check that we can see the command for this message
                            (

                                (
                                    EC_API.getConfigStringList( "command-spy.excluded-commands" ).contains( "tell" ) ||
                                    EC_API.getConfigStringList( "command-spy.excluded-commands" ).contains( "msg" )
                                ) &&
                                !EC_API.checkPerms( player, "ec.bypass.command-spy-commands", false )
                            )
                        )
                    ) {
                        recipients.add( player );
                    }
                }

                if ( recipients.size() > 0 ) {
                    ECChatEvent chatEvent = new ECChatEvent( spy_message, spy_message_clear, e.getMessage(), e.getPlayer(), recipients, spy_channel.getName() );
                    Bukkit.getPluginManager().callEvent( chatEvent );
                }
            }
        });
    } // end method

} // end class