package com.martinambrus.easyChat.listeners;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import com.martinambrus.easyChat.*;
import com.martinambrus.easyChat.events.ECChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.PluginMessageListener;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;

/**
 * Listens to chat events and works the magic
 * to display the correct formatting to the correct
 * people, or to replace chat text by predefined
 * patterns etc.
 *
 * @author Martin Ambrus
 */
public class asyncChatListener implements Listener, PluginMessageListener {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * List of players for who we should ignore cooldown on their next
     * chat message, since the next one will be coming from our own
     * redirect routine.
     */
    private Map<String, Boolean> ignore_cooldown_after_redirect = new HashMap<>();

    /**
     * Constructor. Required due to the way listeners autoloading works.
     */
    public asyncChatListener( final Plugin ec ) {
        this.plugin = ec;

        // register EasyChat as Bungee listener and sender
        Bukkit.getServer().getMessenger().registerOutgoingPluginChannel(this.plugin, "BungeeCord");
        Bukkit.getServer().getMessenger().registerIncomingPluginChannel(this.plugin, "BungeeCord", this);
    } // end method

    private boolean checkChatReplacements( AsyncPlayerChatEvent e ) {
        boolean cancel_chat_event = false;
        String message = e.getMessage();

        // check if our chat conforms to any of the chat replacements (aliases) configured
        ChatPattern pattern = EC_API.chatMessageMatchesPattern( e.getPlayer().getWorld().getName(), message );
        if (null != pattern ) {
            // a match was found, let's see it we should cancel this chat message for it
            if ( pattern.should_cancel_chat_message() ) {
                cancel_chat_event = true;
            }

            // now let's either replace text or run a command
            if ( pattern.getReplacement_type().equals("text") ) {
                // send out the new chat message through Bukkit's own chat system,
                // so it can be intercepted here again and possibly re-formatted,
                // as it's basically a new message that we're sending now
                Bukkit.getScheduler().runTask( this.plugin, new Runnable() {
                    @Override
                    public void run() {
                        // replace message with our own text
                        final Matcher matcher = pattern.getPattern().matcher( message );

                        String new_text = matcher.replaceAll( pattern.getReplacement_value() );
                        new_text = Utils.translate_chat_colors( new_text );
                        new_text = EC_API.replace_placeholders( new_text, e.getPlayer(), null );
                        new_text = new_text.replace( "{MESSAGE}", e.getMessage() );

                        // add player to redirect ignores, so we actually can send the next message
                        // without the cooldown check stopping us
                        ignore_cooldown_after_redirect.put( e.getPlayer().getName(), true );

                        // send the new message into chat
                        e.getPlayer().chat( new_text );
                    }
                });

            } else {
                // run command as the player who sent this message
                String cmd = pattern.getReplacement_value();

                // remove leading slash, if present
                if ( cmd.startsWith("/") ) {
                    cmd = cmd.substring(1);
                }

                final Matcher matcher = pattern.getPattern().matcher( message );
                cmd = matcher.replaceAll( cmd );
                cmd = cmd.replace( "{MESSAGE}", e.getMessage() );

                final String final_cmd = EC_API.replace_placeholders( cmd, e.getPlayer(), null );

                Bukkit.getScheduler().runTask( this.plugin, new Runnable() {
                    @Override
                    public void run() {
                        // run command as the player who sent this message
                        e.getPlayer().performCommand( final_cmd );
                    }
                });
            }

            // if we wanted to send a response to this player, send it
            if ( null != pattern.getResponse() ) {
                Bukkit.getScheduler().runTask( this.plugin, new Runnable() {
                    @Override
                    public void run() {
                        // send response to player
                        String new_text = pattern.getResponse();
                        new_text = Utils.translate_chat_colors( new_text );
                        new_text = EC_API.replace_placeholders( new_text, e.getPlayer(), null );
                        new_text = new_text.replace( "{MESSAGE}", e.getMessage() );
                        e.getPlayer().sendMessage( new_text );
                    }
                });
            }
        }

        return cancel_chat_event;
    } // end method

    private Channel check_message_prefix( String message, Channel player_channel, Player sending_player ) {
        Channel quick_prefix_channel = EC_API.check_quick_chat_prefix( message );
        String player_name = sending_player.getName();
        if ( null != quick_prefix_channel ) {
            // we've found out that the message should go into another chat channel,
            if (
                // if we're sending message to the same channel but prefixed, we're done with checks
                player_channel.getName().equals( quick_prefix_channel.getName() )
                ||
                (
                    // does this player have the permission to access the other channel?
                    (
                        quick_prefix_channel.getOne_time_msg_permissions().isEmpty()
                        ||
                        EC_API.checkPerms( sending_player, Utils.implode( quick_prefix_channel.getOne_time_msg_permissions(), " OR " ), false )
                    )
                        &&
                        // check if player is not muted or banned from that channel
                        !quick_prefix_channel.getMutelist().contains( player_name )
                        &&
                        !quick_prefix_channel.getBanlist().contains( player_name )
                )
            ) {
                // switch player's channel to it
                player_channel = quick_prefix_channel;
            }
        }

        return player_channel;
    } // end method

    /***
     * Rewrite chat based on EasyChat configuration.
     *
     * @param e The actual player chat event to work with.
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void rewriteChat(final AsyncPlayerChatEvent e) {
        if ( e.isCancelled() ) {
            return;
        }

        // only rewrite chat output if we have at least a single channel
        // or we've enabled default chat formatting
        boolean rewrite_chat = (
            (
                EC_API.isModuleEnabled( "channels" )
                &&
                EC_API.channels_count() > 0
            )
            ||
            EC_API.isModuleEnabled( "default-chat-format" )
            );

        boolean channels_enabled = EC_API.isModuleEnabled( "channels" );
        boolean cancel_original_message = false;

        // if this becomes true, we'll add the sending player to the channel they're
        // quick-chatting to, since otherwise they wouldn't actually see their own message,
        // potentially leading to the assumption that their message wasn't sent
        boolean channel_switched = false;

        // check if our chat conforms to any of the chat replacements (aliases) configured
        if ( EC_API.isModuleEnabled("chat-aliasing") ) {
            cancel_original_message = this.checkChatReplacements(e);
        }

        if ( rewrite_chat ) {
            Channel player_channel = EC_API.get_player_channel( e.getPlayer().getUniqueId() );
            Player sending_player = e.getPlayer();

            // if no alias was found for this message or we shouldn't cancel it,
            // check it against channel quick-chat prefixes
            if ( !cancel_original_message ) {
                Channel quick_message_channel = this.check_message_prefix( e.getMessage(), player_channel, sending_player );

                // we're quick-chatting into another channel, change player's channel
                if ( !player_channel.equals( quick_message_channel ) ) {
                    player_channel = quick_message_channel;

                    // remove the prefix from message
                    e.setMessage( e.getMessage().substring( player_channel.getOne_time_msg_pattern().length() ) );
                    channel_switched = true;
                }
            }

            // skip this if we're using default chat
            if ( channels_enabled ) {
                // check that the channel is enabled in player's current world
                if (!EC_API.check_channel_world_enabled( player_channel.getName(), sending_player.getWorld().getName() ) ) {
                    sending_player.sendMessage(EC_API.__("channel.disabled-in-world", player_channel.getColor() + player_channel.getName() + "&r"));
                    e.setCancelled(true);
                    return;
                }
            }

            // check that the channel isn't muted
            if ( player_channel.isMuted() && !EC_API.checkPerms( sending_player, "ec.bypass.muted", false ) ) {
                if ( channels_enabled ) {
                    sending_player.sendMessage( EC_API.__("channel.is-muted", player_channel.getColor() + player_channel.getName() + "&r" ) );
                } else {
                    sending_player.sendMessage( EC_API.__("chat.is-muted" ) );
                }
                e.setCancelled(true);
                return;
            }

            // check that the player isn't muted in this channel
            if ( player_channel.getMutelist().contains( sending_player.getName() ) && !EC_API.checkPerms( sending_player, "ec.bypass.player.muted", false ) ) {
                if ( channels_enabled ) {
                    sending_player.sendMessage( EC_API.__("channel.you-are-muted", player_channel.getColor() + player_channel.getName() + "&r" ) );
                } else {
                    sending_player.sendMessage( EC_API.__("chat.you-are-muted" ) );
                }
                e.setCancelled(true);
                return;
            }

            // skip this if we're using default chat
            if ( channels_enabled ) {
                // check that the player isn't banned in this channel
                if (player_channel.getBanlist().contains( sending_player.getName() ) && !EC_API.checkPerms( sending_player, "ec.bypass.player.banned", false) ) {
                    sending_player.sendMessage(EC_API.__("channel.you-are-banned", player_channel.getColor() + player_channel.getName() + "&r"));
                    e.setCancelled(true);
                    return;
                }
            }

            // check cooldown
            if ( !ignore_cooldown_after_redirect.containsKey( sending_player.getName() ) ) {
                int cooldown = player_channel.check_cooldown( sending_player.getUniqueId() );
                if ( cooldown > 0 && !EC_API.checkPerms( sending_player, "ec.bypass.cooldown", false ) ) {
                    if ( channels_enabled ) {
                        sending_player.sendMessage( EC_API.__("channel.wait-before-sending-next-message", cooldown, player_channel.getColor() + player_channel.getName() + "&r" ) );
                    } else {
                        sending_player.sendMessage( EC_API.__("chat.wait-before-sending-next-message", cooldown ) );
                    }
                    e.setCancelled(true);
                    return;
                }
            } else {
                // remove the player from cooldown ignore map
                ignore_cooldown_after_redirect.remove( sending_player.getName() );
            }



            // we'll only care for message formatting and who hears us if we're not showing only chat bubbles,
            // in which case we're not sending anything to chat, so we'll skip this section altogether
            if ( !player_channel.show_only_chat_bubbles() ) {
                // update format of the message to send
                String message_final;
                String message_log;

                message_final = Utils.translate_chat_colors( player_channel.getFormat() );
                message_log = Utils.translate_chat_colors( player_channel.getFormat_clear() );

                message_final = EC_API.replace_placeholders( message_final, sending_player, player_channel );
                message_log = EC_API.replace_placeholders( message_log, sending_player, player_channel );

                message_final = message_final.replace( "{MESSAGE}", e.getMessage().replace("\"", "\\\"") );
                message_log = message_log.replace( "{MESSAGE}", e.getMessage().replace("\"", "\\\"") );

                // update set of players which we'll be sending this message
                Set<Player> updated_recipients = e.getRecipients();



                // only manage recipients if we're using channels
                if ( channels_enabled ) {
                    // remove any ignored players
                    Set<Player> tmp_recipients = new HashSet<>();
                    for ( Player p : updated_recipients ) {
                        if ( !EC_API.is_player_ignored( p, sending_player ) ) {
                            tmp_recipients.add( p );
                        }
                    }

                    if ( tmp_recipients.size() != updated_recipients.size() ) {
                        updated_recipients = tmp_recipients;
                    }

                    // remove players not in this channel and not subscribed to it
                    tmp_recipients = new HashSet<>();
                    for ( Player p : updated_recipients ) {
                        UUID player_uuid = p.getUniqueId();
                        if (
                            ( channel_switched && p.equals( sending_player ) )
                            ||
                            EC_API.get_player_channel( player_uuid ).equals( player_channel )
                            ||
                            EC_API.is_subscribed_to_channel( player_uuid, player_channel )
                        ) {
                            tmp_recipients.add( p );
                        }
                    }

                    if ( tmp_recipients.size() != updated_recipients.size() ) {
                        updated_recipients = tmp_recipients;
                    }



                    // if this channel is limited to certain worlds, update target players
                    // to include only those in the given worlds
                    if ( !player_channel.getExcluded_worlds().isEmpty() ) {
                        tmp_recipients = new HashSet<>();
                        // iterate over all enabled worlds
                        for ( String world_name : player_channel.getExcluded_worlds() ) {
                            // iterate over all receiving players and remove those that should not get this message
                            for ( Player p : updated_recipients ) {
                                if (
                                    // if excluded worlds is a whitelist, add players that are in those worlds
                                    ( player_channel.getInvert_excluded_worlds() && p.getWorld().getName().equals( world_name ) )
                                    ||
                                    // if excluded worlds is a blacklist, add players that are not in those worlds
                                    ( !player_channel.getInvert_excluded_worlds() && !p.getWorld().getName().equals( world_name ) )
                                ) {
                                    tmp_recipients.add( p );
                                }
                            }
                        }

                        if ( tmp_recipients.size() != updated_recipients.size() ) {
                            updated_recipients = tmp_recipients;
                        }
                    }



                    // check if this channel is local, in which case only include players in the same world
                    // that are at most as far away as is the distance set in config
                    if ( player_channel.getDistance() > 0 ) {
                        // first, exclude players worlds different to our players'
                        tmp_recipients = new HashSet<>();
                        String sender_world_name = sending_player.getWorld().getName();
                        for ( Player p : updated_recipients ) {
                            if ( p.getWorld().getName().equals( sender_world_name ) ) {
                                tmp_recipients.add( p );
                            }
                        }

                        if ( tmp_recipients.size() != updated_recipients.size() ) {
                            updated_recipients = tmp_recipients;
                        }

                        // next, find all players within the given radius
                        tmp_recipients = new HashSet<>();
                        int      local_radius = player_channel.getDistance();
                        Location sender_location = sending_player.getLocation();
                        for ( Player p : updated_recipients ) {
                            if ( p.getLocation().distance( sender_location ) <= local_radius ) {
                                tmp_recipients.add( p );
                            }
                        }

                        if ( tmp_recipients.size() != updated_recipients.size() ) {
                            updated_recipients = tmp_recipients;
                        }

                        // if there are no players who can hear the player, let's inform them
                        if ( updated_recipients.size() == 1 ) {
                            sending_player.sendMessage( Utils.translate_chat_colors( player_channel.getColor() ) + "[" + ( !player_channel.getShortcut().equals("") ? player_channel.getShortcut() : player_channel.getName() ) + "] " + EC_API.__("channel.noone-hears-you", player_channel.getColor() + player_channel.getName()) );

                            if ( null != EC_API.get_default_channel() ) {
                                sending_player.sendMessage(EC_API.__("channel.join-global-channel", EC_API.get_default_channel().getName()));
                            }
                        }
                    }



                    // check if conversation in this channel can be overheard through walls
                    if (
                        player_channel.getDistance() > 0
                        && player_channel.getHear_over_solid_blocks_distance() > -1
                        && updated_recipients.size() > 1
                    ) {
                        // make sure that our overhear radius is not higher than local chat radius
                        int overhear_distance = player_channel.getHear_over_solid_blocks_distance();
                        if ( player_channel.getHear_over_solid_blocks_distance() > player_channel.getDistance() ) {
                            overhear_distance = player_channel.getDistance();
                        }

                        // iterate over all nearby players and check that there is at least 1 block of air
                        // between the head of the sending player and the receiving one, making this search
                        // 3 blocks wide
                        tmp_recipients = new HashSet<>();
                        for ( Player p : updated_recipients ) {
                            // ignore sender with sender comparison
                            if ( sending_player.getName().equals( p.getName() ) ) {
                                tmp_recipients.add( p );
                                continue;
                            }

                            Vector senderHead   = sending_player.getLocation().add(0, 1, 0).toVector();
                            Vector receiverHead = p.getLocation().add(0, 1, 0).toVector();
                            Vector         dir  = receiverHead.subtract( senderHead );
                            BlockIterator bIterator = new BlockIterator( p.getWorld(), senderHead, dir, 0, (int) Math.ceil( dir.length() ) );
                            boolean obstructed = false;
                            int blocks_count_beyond_obstruction = 0;

                            while ( bIterator.hasNext() ) {
                                if ( obstructed ) {
                                    blocks_count_beyond_obstruction++;
                                }

                                Block b = bIterator.next();
                                if ( !b.getType().equals( Material.AIR ) ) {
                                    obstructed = true;
                                }
                            }

                            if ( !obstructed || blocks_count_beyond_obstruction <= overhear_distance ) {
                                tmp_recipients.add( p );
                            }
                        }

                        if ( tmp_recipients.size() != updated_recipients.size() ) {
                            updated_recipients = tmp_recipients;
                        }

                        // if there are no players who can hear the player through a wall, let's inform them
                        if ( updated_recipients.size() == 1 && e.getRecipients().size() > 1 ) {
                            sending_player.sendMessage( Utils.translate_chat_colors( player_channel.getColor() ) + "[" + ( !player_channel.getShortcut().equals("") ? player_channel.getShortcut() : player_channel.getName() ) + "] " + EC_API.__("channel.noone-hears-you-through-walls", player_channel.getColor() + player_channel.getName()) );

                            if ( null != EC_API.get_default_channel() ) {
                                sending_player.sendMessage(EC_API.__("channel.join-global-channel", EC_API.get_default_channel().getName()));
                            }
                        }
                    }
                }

                // cancel this event if we're rewriting chat with our own formatting
                // or we need to cancel the original message through chat aliasing
                if ( rewrite_chat || cancel_original_message  ) {
                    e.setCancelled(true);
                }

                // we wanted the old message to get through as well, fire up event which will display it
                if ( !cancel_original_message && rewrite_chat ) {
                    e.setCancelled(true);

                    if ( updated_recipients.size() > 0 ) {
                        // send a Bungee message with details about our chat event,
                        // if the message is not coming in from a local channel
                        if ( player_channel.getDistance() == 0 ) {
                            ByteArrayDataOutput out = ByteStreams.newDataOutput();
                            out.writeUTF( "Forward" );
                            out.writeUTF( "ALL" );
                            out.writeUTF( "EasyChatMartinAmbrus" );

                            ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
                            DataOutputStream      msgout   = new DataOutputStream( msgbytes );
                            try {
                                msgout.writeUTF( player_channel.getName() );
                                msgout.writeUTF( sending_player.getDisplayName() );
                                msgout.writeUTF( sending_player.getUniqueId().toString() );
                                msgout.writeUTF( e.getMessage() );
                            } catch ( IOException exception ) {
                                if ( EC_API.getDebug() ) {
                                    exception.printStackTrace();
                                }
                            }

                            out.writeShort( msgbytes.toByteArray().length );
                            out.write( msgbytes.toByteArray() );

                            sending_player.sendPluginMessage( this.plugin, "BungeeCord", out.toByteArray() );
                        }

                        // this has to be run synchronously in main task for newer Spigot versions
                        String      finalMessage_final      = message_final;
                        String      finalMessage_log        = message_log;
                        Set<Player> finalUpdated_recipients = updated_recipients;
                        Channel     finalPlayer_channel     = player_channel;
                        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
                            @Override
                            public void run() {
                                Bukkit.getPluginManager().callEvent(new ECChatEvent( finalMessage_final, finalMessage_log, e.getMessage(), sending_player, finalUpdated_recipients, finalPlayer_channel.getName() ));
                            }
                        });
                    }
                }
            } else {
                // we're only showing chat bubbles for this channel, send out the event with original message
                e.setCancelled( true );
                Channel finalPlayer_channel1 = player_channel;
                Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
                    @Override
                    public void run() {
                        Bukkit.getPluginManager().callEvent(new ECChatEvent( e.getMessage(), e.getMessage(), e.getMessage(), sending_player, e.getRecipients(), finalPlayer_channel1.getName() ));
                    }
                });
            }
        } else {
            if ( cancel_original_message ) {
                e.setCancelled( true );
            }
        }
    } // end method

    /***
     * Sends out formatted chat messages via our custom ECChatEvent.
     *
     * @param e The actual event to work with.
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void sendOutChat(final ECChatEvent e) {
        if ( e.isCancelled() ) {
            return;
        }

        ConsoleCommandSender consoleSender = Bukkit.getConsoleSender();

        // if we're only showing chat bubbles, ignore this event
        if ( !EC_API.get_channel_by_name( e.getChannelName() ).show_only_chat_bubbles() ) {
            // send chat message to all required players
            for ( Player target : e.getRecipients() ) {
                String message      = e.getMessage();
                String extra_prefix = e.getPlayerMessagePrefix( target );

                // check if we don't have a prefix to add to the message for this player
                if ( null != extra_prefix ) {
                    // this can be a simple message that does not have the array JSON notation
                    // but only a single JSON element
                    if ( message.startsWith( "[\"\"," ) ) {
                        // array JSON notation
                        message = message.replace( "[\"\",", "[\"\"," + extra_prefix + "," );
                    } else {
                        // single element JSON notation - add the array notation ourselves
                        message = "[\"\"," + extra_prefix + "," + message + "]";
                    }
                }

                Bukkit.dispatchCommand( consoleSender, "tellraw " + target.getName() + " " + message );
            }
        }

        // log this chat message into console, unless it came from the spy channel or was reset
        if ( !e.getLogMessage().equals("") && !EC_API.get_channel_by_name( e.getChannelName() ).equals( EC_API.get_spy_channel() ) ) {
            consoleSender.sendMessage(e.getLogMessage());
        }
    } // end method

    /**
     * Reacts to incoming message from Bungee, so we can show
     * cross-server global messages on this server.
     *
     * @param channel Bungee channel to which a message was sent.
     * @param player Player to who is this message connected.
     * @param message The actual message itself.
     */
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }

        ByteArrayDataInput in         = ByteStreams.newDataInput(message);
        String             subchannel = in.readUTF();

        if (subchannel.equals("EasyChatMartinAmbrus")) {
            short len = in.readShort();
            byte[] msgbytes = new byte[len];
            in.readFully(msgbytes);

            DataInputStream msgin    = new DataInputStream( new ByteArrayInputStream(msgbytes) );

            try {
                String channel_name  = msgin.readUTF();
                String player_name   = msgin.readUTF();
                String player_uuid   = msgin.readUTF();
                String message_orig  = msgin.readUTF();

                /*System.out.println( channel_name );
                System.out.println( player_name );
                System.out.println( player_uuid );
                System.out.println( message_orig );*/
            } catch ( IOException e ) {
                if ( EC_API.getDebug() ) {
                    e.printStackTrace();
                }
            }
        }
    } // end method

} // end class