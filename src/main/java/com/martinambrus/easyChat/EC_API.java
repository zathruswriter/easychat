package com.martinambrus.easyChat;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.*;

/**
 * API for EasyChat used by EC's own procedures
 * but also publicly available to other plugins.
 *
 * @author Martin Ambrus
 */
@SuppressWarnings({"ClassWithTooManyMethods", "UtilityClassWithoutPrivateConstructor"})
public final class EC_API {

    /**
     * Instance of {@link EasyChat}.
     */
    private static EasyChat ec;

    /**
     * Constructor, stores an instance of {@link EasyChat},
     * so this API can work with it. Not publicly available and only ever called from the main
     * EasyChat class' constructor.
     *
     * @param ec Instance of {@link EasyChat EasyChat}.
     */
    public EC_API(final EasyChat ec) {
        EC_API.ec = ec;
    } // end method

    /**
     * Returns name of the EasyChat plugin. If it ever changes, we don't have to string-replace
     * all EasyChat names in all files where console is used to log exceptions.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * Bukkit.getLogger().warning("[" + EC_API.getEcName() + "] Something's gone wrong :-P");
     * </pre>

     *
     * @return Returns the string value from EC's configuration.
     */
    public static String getEcName() {
        return ec.getDescription().getName();
    } // end method

    /**
     * Returns a boolean value from EC's configuration.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * boolean channelsFeature = EC_API.getConfigBoolean("modules.channels.enabled");
     * </pre>
     *
     * @param key The config key we want to return a boolean value for.
     *
     * @return Returns the boolean value from EC's configuration.
     */
    public static boolean getConfigBoolean(final String key) {
        return ec.getExternalConf().getBoolean(key);
    } // end method

    /**
     * Returns a string value from EC's configuration.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * String chatNickClicksColor = EC_API.getConfigString("chatNickClickActions." + key + ".color");
     * </pre>
     *
     * @param key The config key we want to return a string value for.
     *
     * @return Returns the string value from EC's configuration.
     */
    public static String getConfigString(final String key) {
        return ec.getExternalConf().getString(key);
    } // end method

    /**
     * Returns a string value from EC's configuration.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * String listCommandsSeparator = EC_API.getConfigString("listCommandsSeparator", " ");
     * </pre>
     *
     * @param key The config key we want to return a string value for.
     * @param def Default value if string is not found in the configuration.
     *
     * @return Returns the string value from EC's configuration.
     */
    public static String getConfigString(final String key, final String def) {
        return ec.getExternalConf().getString(key, def);
    } // end method

    /**
     * Returns a string list from EC's configuration.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * String chatChannelFormat = EC_API.getConfigStringList("modules.channels.setup.format");
     * </pre>
     *
     * @param key The config key we want to return the string list for.
     *
     * @return Returns the string list from EC's configuration.
     */
    public static List<String> getConfigStringList(final String key) {
        return ec.getExternalConf().getStringList(key);
    } // end method

    /**
     * Returns keys of a configuration section from EC's configuration.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * for (final String key : EC_API.getConfigSectionKeys("chatNickClickActions")) {
     *     // do something with the configuration key name ("key" variable) here
     * }
     * }
     * </pre>
     *
     * @param key The configuration section the keys for we want to return.
     *
     * @return Returns keys for the requested configuration section.
     */
    public static Iterable<String> getConfigSectionKeys(final String key) {
        return ec.getExternalConf().getConfigurationSection(key).getKeys(false);
    } // end method

    /**
     * Replaces all valid placeholders in the given text
     * with their real data representations.
     *
     * @param txt The text in which to search for placeholders and replace them.
     * @param sender The actual person sending the message we want to replace placeholders for.
     * @param channel The channel in which this message is being show, so we can replace
     *                all of the channel-related placeholders. Can be null to use this method
     *                to replace non-channel messages.
     *
     * @return Returns txt with all placeholders replaced by their real values.
     */
    public static String replace_placeholders(String txt, CommandSender sender, Channel channel) {
        return ec.getPlaceholderUtils().replace_placeholders( txt, sender, channel );
    }

    /**
     * Returns the data folder for EasyChat.<br>
     * This is used in various warning messages in console to inform Admins where to search for
     * files they need to update (for example to turn off statistics etc.)
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * virtualPermissionsList.save(new File(EC_API.getEcDataDir(), "perms.yml"));
     * </pre>
     *
     * @return Returns the full data folder path for EasyChat.
     */
    public static String getEcDataDir() {
        return ec.getDataFolder().toString();
    } // end method

    /**
     * Gets the plugin version
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * Bukkit.getLogger().log("info", "You are running EasyChat, version " + EC_API.getEcVersion());
     * </pre>
     *
     * @return Returns version of EasyChat.
     */
    public static String getEcVersion() {
        return ec.getDescription().getVersion();
    } // end method

    /**
     * Checks whether a feature in EasyChat is enabled via config.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (EC_API.isModuleEnabled("listcommands")) {
     *     Bukkit.getLogger().log("info", "Listing of commands is enabled in EasyChat.");
     * }
     * }
     * </pre>
     *
     * @param moduleConfigKey The module name in the config file (for example "channels" to check
     *                         the "modules.channels.enabled" key).
     *
     * @return Returns true if the requested module is enabled, false otherwise.
     */
    public static boolean isModuleEnabled(final String moduleConfigKey) {
        return !ec.getConf().isDisabled(moduleConfigKey);
    } // end method

    /**
     * Gets this plugin's debug state.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (EC_API.getDebug()) {
     *     Bukkit.getLogger().log("debug", "EasyChat debug information line.");
     * }
     * }
     * </pre>
     *
     * @return Returns true if debug is turned ON, false otherwise.
     */
    public static boolean getDebug() {
        return ec.getConf().getDebug();
    } // end method

    /**
     * Retrieves a set of all commands for EasyChat.
     * This is used when registering command executors
     * and tab completers.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * for (final String cmd : EC_API.getCommandsKeySet()) {
     *   // register tab listener for the command in question
     *   // note: exception handling omitted for brevity
     *   final Class<?> cl = Class.forName("com.martinambrus.easyChat.tabcomplete." + cmd.substring(0, 1).toUpperCase() + cmd.substring(1));
     *
     *   TabCompleter tc = (TabCompleter) cl.getConstructor().newInstance();
     *   ((JavaPlugin) plugin).getCommand(cmd).setTabCompleter(tc);
     * }
     * }
     * </pre>
     *
     * @return Returns a set of names for all commands of EasyChat.
     */
    public static Iterable<String> getCommandsKeySet() {
        if (null == ec) {
            return Bukkit.getServer().getPluginManager().getPlugin("EasyChat").getDescription().getCommands().keySet();
        }

        return ec.getConf().getInternalConf().getCommands().keySet();
    } // end method

    /**
     * Checks whether the given key is present in the plugin configuration.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (EC_API.configContainsKey("channels.global")) {
     *   // config contains a default setting a global channel
     * }
     * }
     * </pre>
     *
     * @return Returns true if the key is present in configuration, false otherwise.
     */
    public static boolean configContainsKey(final String key) {
        return ec.getExternalConf().contains(key);
    } // end method

    /***
     * Registers a listener if that listener was not registered already.
     * Can be used to lazy-register listeners from various listener classes.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * // register an event listener to display join and leave click links next to nicknames in chatAliasing
     * EC_API.startRequiredListener("asyncChatListener");
     * </pre>
     *
     * @param listenerName ClassName of the listener to register. This class will then be loaded
     *                     from <i>com.martinambrus.easyChat.listeners.[[ listenerName ]]</i>
     */
    public static void startRequiredListener(final String listenerName) {
        ec.getListenerUtils().startRequiredListener(listenerName);
    } // end method

    /***
     * Registers a listener if that listener was not registered already.
     * Can be used to lazy-register listeners from various listener classes.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * EC_API.startRequiredListener("yourListenerName", yourListenerClassInstance);
     * </pre>
     *
     * @param listenerName          An arbitrary name of the listener to register. EC uses its feature names
     *                              to register event listeners this way.
     * @param listenerClassInstance The actual instance of the listener class we'd like to register.
     */
    public static void startRequiredListener(final String listenerName, final Listener listenerClassInstance) {
        ec.getListenerUtils().startRequiredListener(listenerName, listenerClassInstance);
    } //end method

    /***
     * Generates a console warning. In EasyChat, this method is used when EC is starting or reloading
     * to show any and all error messages that a server Admin should be aware of.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * EC_API.generateConsoleWarning("You shouldn't eat yellow snow!");
     * </pre>
     *
     * @param warningText The actual text to display in console.
     */
    public static void generateConsoleWarning(final String warningText) {
        Bukkit.getLogger().warning('[' + getEcName() + "] " + warningText);
    } //end method

    /***
     * Uses a query parsing algorithm to check for one or many permission nodes given
     * via the permsQuery parameter.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (ec_plugin_instance.getPluginUtils().checkPerms(sender, "my_permission", true) {
     *  // do your stuff
     *  // ... if the commandSender (sender) OR their permission groups do not have the relevant permission,
     *  //     an error message will be displayed to the sender
     * }
     * }
     * </pre>
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (ec_plugin_instance.getPluginUtils().checkPerms(sender, "my_permission", true, true) {
     * // do your stuff
     * // ... if the commandSender (sender) themselves does not have the relevant permission,
     * //     an error message will be displayed to them
     * }
     * }
     * </pre>
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (ee_plugin_instance.getPluginUtils().checkPerms(sender, "(perm1 OR perm2) AND perm3", false) {
     *  // do your stuff
     *  // ... in this case, no error message is displayed to the command sender (last parameter is false)
     *  //     and you can handle this case with your own logic
     * }
     * }
     * </pre>
     *
     * @param sender     The org.bukkit.command.CommandSender CommandSender who we're checking the permission(s) for.
     * @param permsQuery An SQL-like query containing all the parameters that we need to check for.<br>
     *                   The syntax is the same as used in SQL queries with AND / OR sections. Parenthesis are important, as is
     *                   remembering the general rule of operator priority, which says that AND has a higher priority over OR.<br>
     *                   That means that any AND statements will be evaluated first.<br><br>
     *                   <u>Example</u>: <b><i>perm1 AND perm2</i></b><br>&nbsp;&nbsp;&raquo; will check for both, perm1 and perm2 permissions<br><br>
     *                   <u>Example</u>: <b><i>perm1 OR perm2</i></b><br>&nbsp;&nbsp;&raquo; will check for either perm1 or perm2 permission<br><br>
     *                   <u>Example</u>: <b><i>perm1 AND perm2 OR perm3</i></b><br>&nbsp;&nbsp;&raquo; will check for either perm1 + perm2, or the perm3 permission<br><br>
     *                   <u>Example</u>: <b><i>perm1 AND (perm2 OR perm3)</i></b><br>&nbsp;&nbsp;&raquo; the use of parenthesis now changes the previous example,
     *                                     so perm1 + EITHER ONE of perm2 or perm3 permissions will be checked
     * @param showResultToSender If TRUE, a message will be sent out to the player / console for who we're checking these permissions.
     *
     * @return Returns true if the sender has the requested permission(s), false otherwise.
     */
    public static boolean checkPerms(final CommandSender sender, final String permsQuery, final boolean showResultToSender) {
        return ec.getPermissionUtils().checkPerms(sender, permsQuery, showResultToSender);
    } //end method

    /***
     * Checks whether the given CommandSender's primary group has the requested permission assigned.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (EC_API.checkGroupPerm(sender, "my_permission") {
     *  // do your stuff
     * }}
     * </pre>
     *
     * @param sender The org.bukkit.command.CommandSender CommandSender whose group we're checking the permission(s) for.
     * @param perm   The actual permission we're checking for.
     *
     * @return Returns true if the sender's primary group has the requested permission, false otherwise (or if Vault is not enabled).
     */
    public static boolean checkGroupPerm(final CommandSender sender, final String perm) {
        return ec.getPermissionUtils().checkGroupPermSimple(sender, perm);
    } //end method

    /***
     * Checks whether the given group in the given world has the requested permission assigned.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (EC_API.checkGroupPerm("world", "default", "my_permission") {
     *  // do your stuff
     * }}
     * </pre>
     *
     * @param worldName Name of the world for the perm group check.
     * @param groupName Name of the group we're performing the check for.
     * @param perm      The actual permission we're checking.
     *
     * @return Returns true if the sender's primary group has the requested permission, false otherwise (or if Vault is not enabled).
     */
    public static boolean checkGroupPerm(final String worldName, final String groupName, final String perm) {
        return ec.getPermissionUtils().checkGroupPermSimple(worldName, groupName, perm);
    } //end method

    /**
     * Checks whether <a href="https://dev.bukkit.org/projects/vault" target="_blank"><b>Vault</b></a> is enabled on the server.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (EC_API.isVaultEnabled()) {
     *   // Vault is enabled, we should hook in to check player permissions
     * }
     * }
     * </pre>
     *
     * @return Returns true if <a href="https://dev.bukkit.org/projects/vault" target="_blank"><b>Vault</b></a> is enabled, false otherwise.
     */
    public static boolean isVaultEnabled() {
        return ec.getPermissionUtils().isVaultEnabled();
    } // end method

    /**
     * Checks whether a player is in the given permission group.
     * This check is done via Vault's API and if <a href="https://dev.bukkit.org/projects/vault" target="_blank"><b>Vault</b></a> is not present
     * or enabled, it will return TRUE in case the group name provided
     * was either "global" or "default", FALSE otherwise.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     *  {@code
     *  if (EC_API.isPlayerInPermGroup( playerInstance, "owners" )) {
     *    // player is in the "Owners" group, let's roll...
     *  }
     *  }
     *  </pre>
     *
     * @param p Player for which we want to check presence in the permision group.
     * @param group The actual permission group name to check if the player is in.
     *
     * @return Returns TRUE if the player is present in the group, or if <a href="https://dev.bukkit.org/projects/vault" target="_blank"><b>Vault</b></a> is disabled,
     *         then returns TRUE if the player is in either group named "global" or "default".
     *         Returns FALSE otherwise.
     */
    public static boolean isPlayerInPermGroup(Player p, String group) { return ec.getPermissionUtils().isPlayerInPermGroup(p, group); } // end method

    /**
     * Returns all permission groups in which a player is present.
     * If <a href="https://dev.bukkit.org/projects/vault" target="_blank"><b>Vault</b></a> is disabled, an empty array is returned.
     *
     * @param p The player to check groups for.
     *
     * @return Returns all permission groups in which a player is present.
     *         If <a href="https://dev.bukkit.org/projects/vault" target="_blank"><b>Vault</b></a> is disabled, an empty array is returned.
     */
    public static String[] getPlayerPermGroups(Player p) { return ec.getPermissionUtils().getPlayerPermGroups(p); } // end method

    /**
     * Retrieves player's primary group name.
     *
     * @param p Player to retrieve primary group info for.
     *
     * @return Returns name of player's primary group or "" (empty string) if <a href="https://dev.bukkit.org/projects/vault" target="_blank"><b>Vault</b></a> is disabled.
     */
    public static String getPlayerPrimaryPermGroup(Player p) { return ec.getPermissionUtils().getPlayerPrimaryPermGroup(p); } // end method

    /**
     * Performs data type validation on values coming from a user-defined config file
     * and generates console warnings if it's not the type we need.
     *
     * @param class_config_option The actual configuration variable from this class.
     * @param config_path The full path to the option's value in the config.
     * @param config_section_name The name for which informative error messages will be displayed.
     *
     * @return Returns the same value if an error has occurred or a value from the configuration
     *         if we were able to retrieve the correct value type.
     */
    public static Object validateConfigOptionValue(Object class_config_option, String config_path, String config_section_name) {
        return ec.getConf().validateConfigOptionValue( class_config_option, config_path, config_section_name );
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.ChatPattern}
     * if the given chat message can be matched against one.
     *
     * @return Instance of {@link com.martinambrus.easyChat.ChatPattern} that the chat message
     *         was matched against. Null if no patterns matched the chat message.
     */
    public static ChatPattern chatMessageMatchesPattern(String world, String message) {
        return ec.getChatAliasingUtils().message_matches_pattern( world, message );
    } // end method

    /**
     * Returns the number of channels we were able to successfully
     * add to this class.
     *
     * @return Returns number of active channels.
     */
    public static int channels_count() {
        return ec.getChannnelUtils().channels_count();
    }  // end method

    /**
     * Retrieves channel in which the given player is talking.
     *
     * @param player UUID of player to return the chat channel for.
     *
     * @return Returns channel in which the given player is talking.
     */
    public static Channel get_player_channel( UUID player ) {
        return ec.getChannnelUtils().get_player_channel( player );
    } // end method

    /**
     * Checks whether chat message doesn't start with any
     * of the quick chat prefixes in any of the channels.
     * If it does,
     *
     * @param message Message to check for cross-channel chat prefix.
     *
     * @return Returns the channel which quick-chat prefix was matched in the message.
     *         If no matches were found, this method returns null.
     */
    public static Channel check_quick_chat_prefix( String message ) {
        return ec.getChannnelUtils().check_quick_chat_prefix( message );
    } // end method

    /**
     * Checks that the given channel is enabled in the given world.
     *
     * @param channel_name Name of the channel.
     * @param world_name Name of the world to check channel enabled in.
     *
     * @return Returns true if the the given channel is enabled in the given world,
     *         otherwise returns false.
     */
    public static boolean check_channel_world_enabled( String channel_name, String world_name ) {
        return ec.getChannnelUtils().check_channel_world_enabled( channel_name, world_name );
    } // end method

    /**
     * Checks whether a player is subscribed to the given channel.
     *
     * @param player Player to check subscription for.
     * @param channel Channel to check subscription in.
     *
     * @return Returns true if player is subscribed to the given channel, false otherwise.
     */
    public static boolean is_subscribed_to_channel( UUID player, Channel channel ) {
        return ec.getChannnelUtils().is_subscribed_to_channel( player, channel );
    } // end method

    /**
     * Retrieves list of all channels on the server.
     *
     * @return Returns list of all channels on the server.
     */
    public static List<Channel> get_all_channels() {
        return ec.getChannnelUtils().get_all_channels();
    } // end method

    /**
     * Retrieves a channel based on its name.
     *
     * @param channel_name Name of the channel to retrieve.
     *
     * @return Returns the channel requested by its name.
     */
    public static Channel get_channel_by_name( String channel_name ) {
        return ec.getChannnelUtils().get_channel_by_name( channel_name );
    } // end method

    /**
     * Gets the channel used to send broadcasts via /say.
     *
     * @return Returns the channel instance used to send broadcasts via /say.
     */
    public static Channel get_broadcast_channel() {
        return ec.getChannnelUtils().get_broadcast_channel();
    } // end method

    /**
     * Gets the channel used to private message via /tell or /msg.
     *
     * @return Returns the channel used to private message via /tell or /msg.
     */
    public static Channel get_private_channel() {
        return ec.getChannnelUtils().get_private_channel();
    } // end method

    /**
     * Gets the spy channel used to show all messages on the server.
     *
     * @return Returns the spy channel used to show all messages on the server.
     */
    public static Channel get_spy_channel() { return ec.getChannnelUtils().get_spy_channel(); } // end method

    /**
     * Makes the player join the given channel.
     *
     * @param player_uuid Unique ID of the player to join the channel.
     * @param ch Channel to join.
     */
    public static void join_channel( UUID player_uuid, Channel ch ) {
        ec.getChannnelUtils().join_channel( player_uuid, ch );
    } // end method

    /**
     * Subscribes given player to the channel.
     *
     * @param player_uuid Unique ID of the player to subscribe.
     * @param ch Channel to subscribe to.
     *
     * @return Returns true if player was successfully subscribed,
     *         false if they are already subscribed to the provided channel.
     */
    public static boolean subscribe_channel( UUID player_uuid, Channel ch ) {
        return ec.getChannnelUtils().subscribe_channel( player_uuid, ch );
    } // end method

    /**
     * Unsubscribes given player from the channel.
     *
     * @param player_uuid Unique ID of the player to unsubscribe.
     * @param ch Channel to unsubscribe from.
     *
     * @return Returns true if player was successfully unsubscribed,
     *         false if they are not subscribed to the provided channel.
     */
    public static boolean unsubscribe_channel( UUID player_uuid, Channel ch ) {
        return ec.getChannnelUtils().unsubscribe_channel( player_uuid, ch );
    } // end method

    /**
     * Retrieves list of channels the given player is subscribed to.
     *
     * @param player_uuid Player unique ID to check for subscribed channels.
     *
     * @return Returns list of channels the given player is subscribed to.
     */
    public static List<Channel> get_player_subscriptions_list( UUID player_uuid ) {
        return ec.getChannnelUtils().get_player_subscriptions_list( player_uuid );
    } // end method

    /**
     * Isolates player in a single channel and backs up their old
     * channel and subscriptions, so they can be restored once player
     * decides to leave the isolation (and if enabled in config).
     *
     * @param player_uuid The player who is about to be isolated.
     * @param channel Channel into which the player will be isolated.
     */
    public static void isolate_player_in_channel( UUID player_uuid, Channel channel ) {
        ec.getChannnelUtils().isolate_player_in_channel( player_uuid, channel );
    } // end method

    /**
     * Bans player from a single channel.
     *
     * @param player_name The player we are about to ban.
     * @param channel Channel in which the player should be banned.
     */
    public static boolean ban_player_in_channel( String player_name, Channel channel ) {
        return ec.getChannnelUtils().ban_player_in_channel( player_name, channel );
    } // end method

    /**
     * Un-bans player from a single channel.
     *
     * @param player_name The player we are about to un-ban.
     * @param channel Channel from which the player should be un-banned.
     */
    public static boolean unban_player_from_channel( String player_name, Channel channel ) {
        return ec.getChannnelUtils().unban_player_in_channel( player_name, channel );
    } // end method

    /**
     * Mutes player in a single channel.
     *
     * @param player_name The player we are about to mute.
     * @param channel Channel in which the player should be muted.
     */
    public static boolean mute_player_in_channel( String player_name, Channel channel ) {
        return ec.getChannnelUtils().mute_player_in_channel( player_name, channel );
    } // end method

    /**
     * Un-mute player in a single channel.
     *
     * @param player_name The player we are about to un-mute.
     * @param channel Channel from which the player should be un-muted.
     */
    public static boolean unmute_player_from_channel( String player_name, Channel channel ) {
        return ec.getChannnelUtils().unmute_player_in_channel( player_name, channel );
    } // end method

    /**
     * Returns player's old subscriptions and channel settings prior to
     * their isolation in a single channel.
     *
     * @param player_uuid Unique ID of the player we want to un-isolate.
     *
     * @return Returns true if the player was previously isolated, false otherwise.
     */
    public static boolean unisolate_player( UUID player_uuid ) {
        return ec.getChannnelUtils().unisolate_player( player_uuid );
    } // end method

    /**
     * Returns default channel for players on the server.
     *
     * @return Returns default channel for players on the server.
     */
    public static Channel get_default_channel() {
        return ec.getChannnelUtils().get_default_channel();
    } // end method

    /**
     * Checks whether the given ignored player is ignored by the first player.
     *
     * @param player The player for who to check the ignore.
     * @param ignore The ignored player to check whether they are ignored.
     *
     * @return Returns true if the second player is ignored by the first one, false otherwise.
     */
    public static boolean is_player_ignored( Player player, Player ignore ) {
        return ec.getChannnelUtils().is_player_ignored( player, ignore );
    } // end method

    /**
     * Adds a new player into the list of ignores.
     *
     * @param player Player who is adding the ignore.
     * @param ignore Player who should be ignored by the first player.
     */
    public static void add_player_ignore( Player player, Player ignore ) {
        ec.getChannnelUtils().add_player_ignore( player, ignore );
    } // end method

    /**
     * Removes ignore from the ignores list.
     *
     * @param player The player who is removing their ignore.
     * @param ignore The ignored player that should be un-ignored.
     */
    public static void remove_player_ignore( Player player, Player ignore ) {
        ec.getChannnelUtils().remove_player_ignore( player, ignore );
    } // end method

    /**
     * Retrieves all ignores for a single player.
     *
     * @param player The player to retrieve ignores for.
     *
     * @return Returns list of all ignores for the given player,
     *         so they can be stored in a DB.
     */
    public static List<String> get_player_ignores( Player player ) {
        return ec.getChannnelUtils().get_player_ignores( player );
    } // end method

    /**
     * Retrieves last used channel for the given player.
     *
     * @param player Player to retrieve the channel for.
     *
     * @return Returns last used channel for the given player.
     */
    public static String get_player_channel( Player player ) {
        return ec.getLocalStorageUtils().get_player_channel( player );
    } // end method

    /**
     * Stores last used channel for the given player.
     *
     * @param player Player to store the channel for.
     */
    public static void set_player_channel( Player player, String channel_name ) {
        ec.getLocalStorageUtils().set_player_channel( player, channel_name );
    } // end method

    /**
     * Retrieves channel subscriptions for the given player.
     *
     * @param player Player to retrieve the channel for.
     *
     * @return Returns channel subscriptions for the given player.
     */
    public static List<String> get_player_subscriptions( Player player ) {
        return ec.getLocalStorageUtils().get_player_subscriptions( player );
    } // end method

    /**
     * Stores subscriptions for the given player.
     *
     * @param player Player to store subscriptions for.
     */
    public static void set_player_subscriptions( Player player, String subscriptions ) {
        ec.getLocalStorageUtils().set_player_subscriptions( player, subscriptions );
    } // end method

    /**
     * Retrieves ignore list for the given player.
     *
     * @param player Player to retrieve the ignore list for.
     *
     * @return Returns ignore list for the given player.
     */
    public static Map<String, Boolean> get_player_ignores_list( Player player ) {
        return ec.getLocalStorageUtils().get_player_ignores_list( player );
    } // end method

    /**
     * Stores ignore list for the given player.
     *
     * @param player Player to store ignore list for.
     */
    void set_player_ignores_list( Player player, List<String> ignores ) {
        ec.getLocalStorageUtils().set_player_ignores_list( player, ignores );
    } // end method

    /**
     * Calculates display duration in ticks, so previous method can schedule next.
     *
     * @param player Player to perform the calculations for.
     * @param chat Chat message to be displayed.
     *
     * @return Returns display duration for the given chat message
     */
    public static int chatBubbleReceiveChatMessage( Player player, String chat ) {
        return ec.getChatBubblesUtils().receiveMessage( player, chat );
    } // end method

    /**
     * Shows chat message above player's head.
     *
     * @param player The player to show the message for.
     * @param msg The actual message to show.
     */
    public static void showChatBubble(Player player, String msg) {
        ec.getChatBufferUtils().receiveChat( player, msg );
    } // end method

    /**
     * Returns translation for the given identifier, and optionally
     * a set of parameters. If this identifier is not found, the same
     * identifier is used instead of the translation and returned with
     * any given parameters replaced.
     *
     * @param identifier The actual identifier from the .properties file to work with.
     * @param params     Optional parameters used to format translations with placeholders.
     *
     * @return Returns a the requested translation with all optional placeholders correctly substituted.
     */
    public static String __(String identifier, Object... params) {
        return (null != params && 0 < params.length ? ec.getLang().__(identifier, params) :
                ec.getLang().__(identifier));
    } // end method

} // end class