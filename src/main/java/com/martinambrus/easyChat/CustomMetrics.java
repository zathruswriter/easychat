package com.martinambrus.easyChat;

import org.bstats.bukkit.Metrics;
import org.bstats.bukkit.Metrics.SimplePie;
import org.bukkit.plugin.Plugin;

import java.util.concurrent.Callable;

/**
 * Plugin statistics that uses Bastian's bStats Metrics library
 * and provides extended information for various online graphs.
 *
 * @author Rojel
 * @author martinambrus
 */
final class CustomMetrics {

    /**
     * Configuration option for disabled metrics options.
     */
    private static final String disabledValue = EC_API.__("stats.not-in-use");

    /**
     * Configuration option for enabled metrics options.
     */
    private static final String enabledValue = EC_API.__("stats.in-use");

    /**
     * The plugin for which these statistics are being collected.
     */
    private final Plugin plugin;

    /**
     * Constructor. Saves references to plugin and its configuration.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * final CustomMetrics customMetrics = new CustomMetrics(yourPluginInstance, EasyChatConfigurationInstance);
     * customMetrics.initMetrics();
     * </pre>
     *
     * @param plugin - The plugin for which statistics are being collected.
     */
    CustomMetrics(final Plugin plugin) {
        this.plugin = plugin;
    } //end method

    /**
     * Adds a new Pie Graph into BStats metrics using the name and the value provided.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * customMetrics.addBcstatsPieGraph(bcStatsMetricsInstance, "PluginVersion", 1.21);
     * </pre>
     *
     * @param bmetrics BStats Metrics class instance.
     * @param graphID ID of the graph to add.
     * @param graphValue Value for the graph to send.
     */
    private void addBcstatsPieGraph( final org.bstats.bukkit.Metrics bmetrics, final String graphID, final String graphValue) {
        bmetrics.addCustomChart(new SimplePie(graphID, new Callable<String>() {
            @Override
            public String call() {
                return graphValue;
            }
        }));
    } //end method

    /**
     * Adds a new Line Graph into BStats metrics using the name and the value provided.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * customMetrics.addBcstatsLineGraph(bcStatsMetricsInstance, "PluginVersion", 1.21);
     * </pre>
     *
     * @param bmetrics BStats Metrics class instance.
     * @param graphID ID of the graph to add.
     * @param graphValue Value for the graph to send.
     */
    private void addBcstatsLineGraph( final org.bstats.bukkit.Metrics bmetrics, final String graphID, final Integer graphValue) {
        bmetrics.addCustomChart(new Metrics.SingleLineChart(graphID, new Callable<Integer>() {
            @Override
            public Integer call() {
                return graphValue;
            }
        }));
    } //end method

    /**
     * Initialization routine for BStats.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * final CustomMetrics customMetrics = new CustomMetrics(yourPluginInstance, pluginID);
     * customMetrics.initBStats();
     * </pre>
     */
    private void initBStats() {
        final org.bstats.bukkit.Metrics bmetrics = new org.bstats.bukkit.Metrics(this.plugin, 10302);

        // subscriptions module enabled/disabled
        addBcstatsPieGraph(bmetrics, "subs_module", //NON-NLS
            EC_API.isModuleEnabled("subscribe-channel-command") ? enabledValue : disabledValue); //NON-NLS

        // isolate module enabled/disabled
        addBcstatsPieGraph(bmetrics, "isolate_module", //NON-NLS
            EC_API.isModuleEnabled("isolate-channel-command") ? enabledValue : disabledValue); //NON-NLS

        // isolate command remembers subscriptions
        addBcstatsPieGraph(bmetrics, "isolate_remembers_subs", //NON-NLS
            EC_API.isModuleEnabled("channels.isolate-command-remembers-subscriptions") ? enabledValue : disabledValue); //NON-NLS

        // ignore module enabled/disabled
        addBcstatsPieGraph(bmetrics, "ignore_module", //NON-NLS
            EC_API.isModuleEnabled("ignore-channel-command") ? enabledValue : disabledValue); //NON-NLS

        // DiscordSrv module enabled/disabled
        addBcstatsPieGraph(bmetrics, "discordsrv_module", //NON-NLS
            EC_API.isModuleEnabled("discordsrv-plugin-compatibility") ? enabledValue : disabledValue); //NON-NLS

        // custom join messages module enabled/disabled
        addBcstatsPieGraph(bmetrics, "custom_join_module", //NON-NLS
            EC_API.isModuleEnabled("custom-join-message") ? enabledValue : disabledValue); //NON-NLS

        // custom leave messages module enabled/disabled
        addBcstatsPieGraph(bmetrics, "custom_leave_module", //NON-NLS
            EC_API.isModuleEnabled("custom-leave-message") ? enabledValue : disabledValue); //NON-NLS

        // global spy channel module enabled/disabled
        addBcstatsPieGraph(bmetrics, "global_spy_module", //NON-NLS
            EC_API.isModuleEnabled("global-spy-channel") ? enabledValue : disabledValue); //NON-NLS

        // MOTD module enabled/disabled
        addBcstatsPieGraph(bmetrics, "motd_module", //NON-NLS
            EC_API.isModuleEnabled("motd") ? enabledValue : disabledValue); //NON-NLS

        // rules module enabled/disabled
        addBcstatsPieGraph(bmetrics, "rules_module", //NON-NLS
            EC_API.isModuleEnabled("rules") ? enabledValue : disabledValue); //NON-NLS

        // channels module enabled/disabled
        addBcstatsPieGraph(bmetrics, "channels_module", //NON-NLS
            EC_API.isModuleEnabled("channels") ? enabledValue : disabledValue); //NON-NLS

        // default chat module enabled/disabled
        addBcstatsPieGraph(bmetrics, "default_chat_module", //NON-NLS
            (EC_API.isModuleEnabled("default-chat-format") && !EC_API.isModuleEnabled("channels")) ? enabledValue : disabledValue); //NON-NLS

        // broadcasts module enabled/disabled
        addBcstatsPieGraph(bmetrics, "broadcasts_module", //NON-NLS
            EC_API.isModuleEnabled("broadcasts") ? enabledValue : disabledValue); //NON-NLS

        // private messagging module enabled/disabled
        addBcstatsPieGraph(bmetrics, "private_chat_module", //NON-NLS
            EC_API.isModuleEnabled("private-chat") ? enabledValue : disabledValue); //NON-NLS

        // command spy module enabled/disabled
        addBcstatsPieGraph(bmetrics, "command_spy_module", //NON-NLS
            EC_API.isModuleEnabled("command-spy") ? enabledValue : disabledValue); //NON-NLS

        // chat aliasing module enabled/disabled
        addBcstatsPieGraph(bmetrics, "chat_aliasing_module", //NON-NLS
            EC_API.isModuleEnabled("chat-aliasing") ? enabledValue : disabledValue); //NON-NLS

        // chat bubbles module enabled/disabled
        addBcstatsPieGraph(bmetrics, "chat_bubbles_module", //NON-NLS
            EC_API.isModuleEnabled("chat-bubbles") ? enabledValue : disabledValue); //NON-NLS

        // strip chat logs of colors
        addBcstatsPieGraph(bmetrics, "logs_color_strip", //NON-NLS
            EC_API.getConfigBoolean("modules.console-strip-chat-colors") ? enabledValue : disabledValue); //NON-NLS

        // locale
        addBcstatsPieGraph(bmetrics, "locale", EC_API.getConfigString( "lang" ) ); //NON-NLS

        // total number of channels
        addBcstatsLineGraph(bmetrics, "channels_count", EC_API.channels_count() ); //NON-NLS

        // number of channels for this server
        addBcstatsPieGraph(bmetrics, "channels_count_exact", EC_API.channels_count() + "" ); //NON-NLS

        boolean prefix_msg_enabled_channels = false;
        boolean chat_bubbles_modules_enabled = EC_API.isModuleEnabled("chat-bubbles");
        boolean chat_bubbles_enabled_channels = false;
        boolean chat_bubbles_only_channels = false;
        boolean local_channels = false;
        boolean hear_over_walls_channels = false;
        boolean pw_protected_channels = false;
        boolean channel_subs_enabled = false;
        int mutes_number = 0;
        int bans_number = 0;

        // make cross-channel stats
        for ( Channel channel : EC_API.get_all_channels() ) {
            // do we have any one-time-msg-char channels?
            if ( !channel.getOne_time_msg_pattern().equals( "" ) ) {
                prefix_msg_enabled_channels = true;
            }

            // do we have any channels with enabled chat bubbles?
            if ( chat_bubbles_modules_enabled && !channel.getFormat_chat_bubble().equals( "" ) ) {
                chat_bubbles_enabled_channels = true;
            }

            // do we have any channels with chat bubbles solely enabled?
            if ( chat_bubbles_modules_enabled && channel.show_only_chat_bubbles() ) {
                chat_bubbles_only_channels = true;
            }

            // do we have any local channels?
            if ( channel.getDistance() > 0 ) {
                local_channels = true;
            }

            // do we have any local channels with hear-over-walls feature enabled?
            if ( channel.getDistance() > 0 && channel.getHear_over_solid_blocks_distance() > -1 ) {
                hear_over_walls_channels = true;
            }

            // do we have any password-protected channels?
            if ( !channel.getPassword().equals( "" ) ) {
                pw_protected_channels = true;
            }

            // do we have subscriptions enabled for any of the channels?
            if ( channel.getSubscriptions_allowed() ) {
                channel_subs_enabled = true;
            }

            mutes_number += channel.getMutelist().size();
            bans_number += channel.getBanlist().size();
        }

        // one-time cross-channel messaging active on this server
        addBcstatsPieGraph(bmetrics, "one_time_msgs_active", //NON-NLS
            prefix_msg_enabled_channels ? enabledValue : disabledValue); //NON-NLS

        // chat bubbles active on this server
        addBcstatsPieGraph(bmetrics, "chat_bubbles_active", //NON-NLS
            chat_bubbles_enabled_channels ? enabledValue : disabledValue); //NON-NLS

        // chat bubbles only active on this server
        addBcstatsPieGraph(bmetrics, "chat_bubbles_only_active", //NON-NLS
            chat_bubbles_only_channels ? enabledValue : disabledValue); //NON-NLS

        // local channels active on this server
        addBcstatsPieGraph(bmetrics, "local_channels_active", //NON-NLS
            local_channels ? enabledValue : disabledValue); //NON-NLS

        // hear-over-wall channels active on this server
        addBcstatsPieGraph(bmetrics, "hear_over_walls_active", //NON-NLS
            hear_over_walls_channels ? enabledValue : disabledValue); //NON-NLS

        // password-protected channels active on this server
        addBcstatsPieGraph(bmetrics, "pw_protected_channels_active", //NON-NLS
            pw_protected_channels ? enabledValue : disabledValue); //NON-NLS

        // subscription-enabled channels active on this server
        addBcstatsPieGraph(bmetrics, "sub_channels_active", //NON-NLS
            channel_subs_enabled ? enabledValue : disabledValue); //NON-NLS

        // number of bans in all channels
        addBcstatsLineGraph(bmetrics, "bans_count", bans_number ); //NON-NLS

        // number of mutes in all channels
        addBcstatsLineGraph(bmetrics, "mutes_count", mutes_number ); //NON-NLS
    } //end method

    /**
     * Initialization method for all supported metrics.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * final CustomMetrics customMetrics = new CustomMetrics(yourPluginInstance, EasyChatConfigurationInstance);
     * customMetrics.initMetrics();
     * </pre>
     */
    void initMetrics() {
        initBStats();
    } //end method

} // end class