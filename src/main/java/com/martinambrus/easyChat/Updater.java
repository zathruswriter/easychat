package com.martinambrus.easyChat;

import com.martinambrus.easyChat.events.ECReloadEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitTask;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Spigot plugin version checker. Compares latest version on Spigot
 * with the current one and warns in console and chatAliasing as needed.
 *
 * @author Martin Ambrus
 */
final class Updater implements Runnable, Listener {

    /**
     * Is set to FALSE after first display of the "getting update info" text.
     * This is to prevent console spam when update interval is too short.
     */
    private boolean firstRun = true;

    /**
     * Determines whether a newer version is available, in which case
     * we'll notify people with the appropriate permissions in-game.
     */
    private String newVersionAvailable;

    /**
     * URL of this resource on Spigot.
     */
    @SuppressWarnings("HardCodedStringLiteral")
    private final String spigotURL = "https://www.spigotmc.org/resources/easychat-go-crazy-with-your-chat.88859/";

    /**
     * API URL for this resource on Spigot to check for new versions.
     */
    private final String spigotAPICheckURL = "https://api.spigotmc.org/legacy/update.php?resource=88859";

    /**
     * {@link EasyChat EasyChat} instance.
     */
    private final EasyChat plugin;

    /**
     * Scheduled task ID, used for periodic update checks.
     */
    private BukkitTask scheduledTaskID = null;

    /**
     * True if we registered this class to listen for player joins,
     * false otherwise.
     */
    private boolean eventListenerRegistered = false;

    /**
     * Constructor.
     * Starts listening to player join events to possibly
     * inform them about a new version availability.
     *
     * @param ec {@link EasyChat EasyChat} instance.
     */
    Updater(final EasyChat ec) {
        plugin = ec;

        // do all the things that are usually done when ec_reload is performed
        onReload();
    } // end method

    /**
     * Sends an in-game player chatAliasing information about new version of the plugin.
     *
     * @param player The player we want to inform about new EC version.
     */
    private void tellNewVersionInChat(final CommandSender player) {
        player.sendMessage(ChatColor.AQUA +
            EC_API.__(
                "chat.updater-new-version-available",
                ChatColor.YELLOW + newVersionAvailable + ChatColor.AQUA,
                ChatColor.YELLOW + EC_API.getEcName() + ChatColor.AQUA,
                ChatColor.WHITE + spigotURL)
        );
    } // end method

    /**
     * Asks the Spigot HTTP API for newest version of this plugin
     * and notifies console if any are found.
     */
    @Override
    public void run() {
        if (firstRun) {
            Bukkit.getLogger().info('[' + EC_API.getEcName() + "] " + EC_API.__("updater.checking-for-updates"));
        }

        try {
            final HttpURLConnection con = (HttpURLConnection) new URL(this.spigotAPICheckURL).openConnection();

            con.setDoOutput(true);
            con.setRequestMethod("GET"); //NON-NLS

            final String version = new BufferedReader(new InputStreamReader(con.getInputStream())).readLine();
            // only version number would be the output of this, so it'll always be less than 10 characters
            if (10 >= version.length()) {
                final String ecVersion = plugin.getDescription().getVersion();
                final VersionComparator cmp = new VersionComparator();

                if (cmp.compare(version, ecVersion) == 1) {
                    Bukkit.getLogger().warning(
                        '[' + plugin.getName() + "] " +
                            EC_API.__("updater.new-version-available", version, spigotURL, ecVersion)
                    );
                    newVersionAvailable = version;
                } else {
                    if (firstRun) {
                        Bukkit.getLogger().info('[' + plugin.getName() + "] " + EC_API.__("updater.no-new-version"));
                    }
                }
            } else {
                Bukkit.getLogger().warning('[' + plugin.getName()
                    + "] " + EC_API.__("updater.update-check-failed"));
            }
        } catch (final Exception ex) {
            Bukkit.getLogger().warning('[' + plugin.getName()
                + "] " + EC_API.__("updater.update-check-failed"));
        }

        firstRun = false;
    } // end method

    /**
     * Registers event listener for the join event and starts
     * a scheduled task that will check for this plugin's updated
     * version online every hour.
     *
     * @param forceShowVersionInfo If set, an one-time update check will be triggered.
     */
    void onReload(final boolean... forceShowVersionInfo) {
        if (!eventListenerRegistered) {
            // start listening to player joins
            plugin.getServer().getPluginManager().registerEvents(this, plugin);
            eventListenerRegistered = true;
        }

        if (!plugin.getConf().isDisabled("check-for-updates")) { //NON-NLS
            int period;
            try {
                period = Integer.parseInt(EC_API.getConfigString("modules.check-for-updates.check-for-updates-every-seconds"));
            } catch (Exception ex) {
                EC_API.generateConsoleWarning( EC_API.__("updated.invalid-time-period") );
                period = 7200;
            }

            // check if we want a single run of the update check or a periodic one
            if ( period > 0 ) {
                // run periodically
                if (null == scheduledTaskID) {
                    scheduledTaskID = Bukkit.getScheduler().runTaskTimerAsynchronously(plugin, this, 20, period * 20);
                } else if (0 < forceShowVersionInfo.length) {
                    Bukkit.getScheduler().runTaskAsynchronously( this.plugin, new Runnable() {
                        @Override
                        public void run() {
                            Updater.this.run();
                        }
                    });
                }
            } else if ( firstRun ) {
                // single-check run
                run();
            }
        } else {
            if (null != scheduledTaskID) {
                scheduledTaskID.cancel();
                scheduledTaskID = null;
            }
        }
    } // end method

    /***
     * Disables update checking. Used when disabling the plugin.
     */
    void unregister() {
        if (null != scheduledTaskID) {
            scheduledTaskID.cancel();
            scheduledTaskID = null;
        }
    } // end method

    /**
     * Returns the newVersionAvailable value, be it null
     * or a real new version string.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * String newVersion = this.getAvailableNewVersion();
     * if (newVersion != null) {
     *     Bukkit.getLogger().log("info", "New EasyChat version (" + newVersion + ") was released. Download your copy today!");
     * }
     * </pre>
     *
     * @return Returns the value of newVersionAvailable. It will be null if no new version
     *         is available, a real version number string otherwise.
     */
    String getAvailableNewVersion() {
        return newVersionAvailable;
    } // end method

    /***
     * Notifies everyone with the right permission about new version availability.
     *
     * @param e A player join event holding the player information, from which name is used
     *          to determine whether to notify the player about a new version or not.
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void notifyNewVersion(final PlayerJoinEvent e) {
        if ((null != scheduledTaskID) && (null != newVersionAvailable)
            && EC_API.checkPerms(e.getPlayer(), "ec.notifynewversion", false)) { //NON-NLS
            tellNewVersionInChat(e.getPlayer());
        }
    } // end method

    /***
     * React to the a custom "reload" event for which we'll reload our version information.
     *
     * @param e The actual new ECReloadEvent event.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void reload(final ECReloadEvent e) {
        run();
    } // end method

} // end class