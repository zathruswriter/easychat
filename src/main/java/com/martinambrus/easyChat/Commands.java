package com.martinambrus.easyChat;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandMap;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;

/**
 * Commands-related utilities and methods. Used to seek and find commands from all plugins,
 * search for their duplicates, overwrites and other inconsistencies.
 *
 * @author Martin Ambrus
 */
@SuppressWarnings({"OverlyComplexClass", "VariableNotUsedInsideIf"})
final class Commands {

    /**
     * Instance of {@link com.martinambrus.easyChat.EasyChat}.
     */
    private final Plugin plugin;

    /**
     * Constructor, creates a new instance of the Commands class
     * and registers command executors for all EC commands found.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * // usage in EasyChat main class
     * this.commands = new Commands(this);
     *
     * if (!this.commands.registerCommandExecutors(this.config)) {
     *     // bail out if we're disabling EC due to a missing command
     *     return;
     * }
     * }
     * </pre>
     *
     * @param ec Instance of {@link com.martinambrus.easyChat.EasyChat EasyChat}.
     */
    Commands(final Plugin ec) {
        plugin = ec;
    } //end method

    /**
     * Used on EasyChat startup (i.e. in onEnable).
     * Parses all commands that are in plugin.yml file for this plugin
     * and registers their executors one by one to the correct classes.
     * This is possible, as classes follow the same naming convention
     * as commands, i.e. Ec_channel.class = /ec_channel.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * // usage in EasyChat main class
     * this.commands = new Commands(this);
     *
     * if (!this.commands.registerCommandExecutors(this.config)) {
     *     // bail out if we're disabling EC due to a missing command
     *     return;
     * }
     * </pre>
     *
     * @param config Plugin configuration class instance.
     * @return Returns true if registration was successful, false in case of any errors.
     */
    boolean registerCommandExecutors(final ConfigAbstractAdapter config) {
        // we can only register commands from plugin.yml file
        if (null != config.getInternalConf().getCommands()) {
            // iterate all commands and register them one by one
            for (final String cmd : EC_API.getCommandsKeySet()) {
                // don't register disabled commands
                if (!config.isDisabled(cmd.replaceAll("ec_", ""))) { //NON-NLS
                    try {
                        final Class<?> cl = Class.forName("com.martinambrus.easyChat.commands." + Utils.capitalize(cmd));
                        final Constructor<?> cons = cl.getConstructor(Plugin.class);

                        CommandExecutor co = (CommandExecutor) cons.newInstance(plugin);
                        ((JavaPlugin) plugin).getCommand(cmd).setExecutor(co);

                        // if this command executor also implements a listener,
                        // activate it
                        if (co instanceof Listener) {
                            ((EasyChat) plugin).getListenerUtils()
                                                    .startRequiredListener(Utils.capitalize(cmd), (Listener) co);
                        }
                    } catch (final NoSuchMethodException e) {
                        // not every command has a constructor
                        try {
                            final Class<?> cl = Class.forName("com.martinambrus.easyChat.commands." + Utils.capitalize(cmd));

                            CommandExecutor co = (CommandExecutor) cl.getConstructor().newInstance();
                            ((JavaPlugin) plugin).getCommand(cmd).setExecutor(co);

                            // if this command executor also implements a listener,
                            // activate it
                            if (co instanceof Listener) {
                                ((EasyChat) plugin).getListenerUtils()
                                                        .startRequiredListener(Utils.capitalize(cmd), (Listener) co);
                            }
                        } catch (final Throwable e1) {
                            Bukkit.getLogger().severe('[' + config.getPluginName()
                                + "] " + EC_API.__("error.command-not-found", cmd));
                            Bukkit.getServer().getPluginManager().disablePlugin(plugin);
                            e1.printStackTrace();

                            return true;
                        }
                    } catch (final Throwable e) {
                        Bukkit.getLogger().severe('[' + config.getPluginName()
                            + "] " + EC_API.__("error.command-not-found", cmd));
                        Bukkit.getServer().getPluginManager().disablePlugin(plugin);
                        e.printStackTrace();

                        return true;
                    }
                }
            }
        }

        return false;
    } //end method

    /**
     * Unregisters all commands initially bound to this plugin.
     * Used during plugin disable.
     *
     *  <br><br><strong>Example:</strong>
     * <pre>
     * // initialize Config & Commands (onEnable() method)
     * this.config = new Config(this);
     * this.commands = new Commands(this);
     *
     * // ... some more code, other methods etc.
     *
     * // onDisable() method
     * this.commands.unregisterCommandExecutors(this.config);
     * </pre>
     *
     * @param config Plugin configuration class instance.
     */
    void unregisterCommandExecutors(final ConfigAbstractAdapter config) {
        try {
            Field commandMap = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            commandMap.setAccessible(true);

            Field knownCommands = SimpleCommandMap.class.getDeclaredField("knownCommands");
            knownCommands.setAccessible(true);

            if (null != config.getInternalConf().getCommands()) {
                // iterate all commands and unregister them one by one
                for (final String cmd : EC_API.getCommandsKeySet()) {
                    // no need to unregister a disabled command
                    if (!config.isDisabled(cmd.replaceAll("ec_", ""))) { //NON-NLS
                        try {
                            ((JavaPlugin) plugin).getCommand(cmd)
                                                 .unregister((CommandMap) commandMap.get(Bukkit.getServer()));
                        } catch (IllegalArgumentException | IllegalAccessException e) {
                            Bukkit.getLogger()
                                  .severe('[' + config.getPluginName() + "] " + EC_API
                                      .__("error.command-cannot-unregister", cmd));
                        }
                    }
                }
            }
        } catch (final Exception e) {
            // if we can't get the command map, then don't unregister our commands
            // because there is nothing to unregister
        }
    } //end method

} // end class