package com.martinambrus.easyChat;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.List;

/**
 * Configuration class helper, contains utilities based around plugin configuration.
 *
 * @author Martin Ambrus
 */
@SuppressWarnings("HardCodedStringLiteral")
final class Config {

    /**
     * Instance of {@link EasyChat}.
     */
    private final EasyChat plugin;

    /**
     * Instance of {@link com.martinambrus.easyChat.ConfigAbstractAdapter}.
     * Will contain the correct configuration adapter (DB, file-based), based on a setting in the main config file.
     */
    private ConfigAbstractAdapter config = null;

    /**
     * Constructor, checks plugin configuration and updates it as necessary.
     * This will check for upgraded plugin with old configuration as well as
     * fill-in any missing config file values.
     *
     * @param ec The singleton instance of EasyChat.
     */
    Config(final EasyChat ec) {
        this.plugin = ec;

        // initialize and load user configuration
        FileConfiguration initialConfig = YamlConfiguration.loadConfiguration(new File(EC_API.getEcDataDir(), "config.yml"));
        String backend = initialConfig.getString("configBackend");

        // copy over config.yml and config-db.yml, as we did not find a backend,
        // so we will use the file backend and also copy over the DB template, if not there
        if (null == backend) {
            this.plugin.saveResource("config.yml", true);
            this.plugin.saveResource("config-db.yml", false);
            backend = "file";
        }

        switch (backend) {
            case "file": this.config = new ConfigFileAdapter(this.plugin);
                break;

            // if we're using a database, let's first create a DB adapter which updates the actual local file config
            // files and then return the actual config file adapter with the latest configuration
            case "db":
                // save the DB config file, if it doesn't exist
                if (!new File(EC_API.getEcDataDir(), "config-db.yml").exists()) {
                    this.plugin.saveResource("config-db.yml", true);
                }

                // check which DB adapter should we use
                FileConfiguration dbConfig = YamlConfiguration.loadConfiguration(new File(EC_API.getEcDataDir(), "config-db.yml"));

                switch (dbConfig.getString("db_type")) {
                    case "mysql":
                        ConfigMySQLAdapter mysql = new ConfigMySQLAdapter(this.plugin);
                        this.config = new ConfigFileAdapter(this.plugin);
                        // once the config file adapter is created, the config file would be updated
                        // with any newly added keys... so we'll make sure to transfer them to the DB
                        mysql.ignoreDBChangedOnce = true;
                        mysql.writeConfigFileIntoDB("main");
                        // this will set ignoreDBChangedOnce back to false, while updating last change dates as well
                        // if the main configuration has changed
                        mysql.run();
                        // ... but we'll still unset the single ignore flag here in case config didn't change,
                        //     in which case the DB check would fail to update local configs the next time
                        //     a DB change occured
                        mysql.ignoreDBChangedOnce = false;
                        break;

                    default: Bukkit.getLogger().severe("[EasyChat] " + EC_API.__("error.sql-no-suitable-adapter"));
                        this.config = new ConfigFileAdapter(this.plugin);
                        return;
                }

                break;

            default: Bukkit.getLogger().severe("[EasyChat] " + EC_API.__("error.config-no-suitable-configuration-found"));
                Bukkit.getPluginManager().disablePlugin(this.plugin);
        }
    } //end method

    /**
     * Returns the actual used configuration adapter, so it can be set in the main EasyChat class.
     *
     * @return Returns instance of ConfigAbstractAdapter.
     */
    ConfigAbstractAdapter getConfigAdapter() {
        return this.config;
    }

} // end class