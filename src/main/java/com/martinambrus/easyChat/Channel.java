package com.martinambrus.easyChat;

import com.martinambrus.easyChat.events.ECToggleDebugEvent;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A single channel object representation.
 *
 * @author Martin Ambrus
 */
public final class Channel {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * Full name (title) of the channel.
     */
    private String name;

    /**
     * A 1-letter shortcut for the channel.
     */
    private String shortcut = "";

    /**
     * Optional color code to use in informative messages about this channel.
     */
    private String color = "";

    /**
     * Pattern used to send a single message
     * into this channel regardless in which channel
     * the player is actively talking.
     */
    private String one_time_msg_pattern = "";

    /**
     * Whether or not to show channel join/leave messages
     * to all players in this channel (or subscribed to it)
     * when a player joins or leaves it.
     */
    private Boolean show_join_leave_messages = false;

    /**
     * Whether to auto-subscribe new players to this channel
     * when they first join the server. Auto-subscription
     * means that these players will see messages from this channel
     * even if they are actively talking in another one.
     */
    private Boolean auto_subscribe_newcomers = false;

    /**
     * Parsed format for this channel which is used in /tellraw
     * command to send JSON-formatted chatAliasing messages.
     * Stored in a Map, as we need to check each player's permissions
     * since all of the chatAliasing parts can have various permission
     * settings for various player groups.
     * Therefore, we'll be actually assembling the final format string
     * from these Map values.
     */
    private String format = "";

    /**
     * This format will be used for sending the chatAliasing to console
     * for logging purposes, removing all color codes from output,
     * so those weird characters don't pollute console logs.
     */
    private String format_clear = "";

    /**
     * Format for the chat bubble - a simple format with {MESSAGE} placeholder
     * for the actual message and optional color & formatting codes.
     */
    private String format_chat_bubble = "";

    /**
     * If this is true, chat will not be sent out and only chat bubbles
     * will be shown above player heads as they talk.
     */
    private boolean only_show_chat_bubbles = false;

    /**
     * Distance up to which players should hear other players in this channel.
     * When set to 0, messages are heard in the whole world.
     */
    private Integer distance = 0;

    /**
     * Distance to hear chatAliasing messages in this channel over solid blocks.
     * Ignored when set to -1 or when "distance" is set to 0.
     * Will be set to value of "distance" if this value is greater.
     */
    private Integer hear_over_solid_blocks_distance = -1;

    /**
     * List of worlds in which this chatAliasing should not be heard.
     */
    private List<String> excluded_worlds = new ArrayList<>();

    /**
     * If true, the "excluded_worlds" variable works as a whitelist
     * and this channel will be heard only in worlds listed there.
     */
    private Boolean invert_excluded_worlds = false;

    /**
     * Permissions from which a player needs to have at least one
     * in order to be allowed to join this channel.
     */
    private List<String> join_permissions = new ArrayList<>();

    /**
     * Permissions from which a player needs to have at least one
     * in order to be allowed to sunscribe to this channel.
     */
    private List<String> subscribe_permissions = new ArrayList<>();

    /**
     * Permissions from which a player needs to have at least one
     * in order to be allowed to use the quick-talk character to
     * send a single message to this channel.
     */
    private List<String> one_time_msg_permissions = new ArrayList<>();

    /**
     * Password that should be used when joining or subscribing
     * to this channel.
     */
    private String password = "";

    /**
     * How long does the player need to wait before sending
     * next message in this channel. Value is in seconds.
     */
    private Integer cooldown = 0;

    /**
     * Whether this channel is muted.
     */
    private Boolean muted = false;

    /**
     * Whether players are allowed to subscribe to this channel.
     */
    private Boolean subscriptions_allowed = true;

    /**
     * List of player names that are banned from using this channel.
     */
    private List<String> banlist = new ArrayList<>();

    /**
     * List of player names that are muted in this channel.
     */
    private List<String> mutelist = new ArrayList<>();

    /**
     * Contains timestamp of each player's last message in this channel.
     * Used in cooldown checks.
     */
    private Map<UUID, Integer> last_msg = new HashMap<>();

    /**
     * Constructor, creates a new channel object with settings from config.
     *
     * @param name A unique name for the channel from the config file.
     * @param alt_channel_name This is used to create pseudo-channels, such as one used
     *                         for default chat where everyone will be writing instead
     *                         of handling multiple channels, or for broadcast and private
     *                         messaging.
     */
    Channel(Plugin ec, String name, String... alt_channel_name) {
        this.plugin = ec;
        this.name = name;

        // get the config route with which we'll work to get our settings
        // depending on whether this is an ordinary channel or we're creating
        // a pseudo-channel for default chatAliasing
        String config_route = ( null != alt_channel_name && 0 < alt_channel_name.length ? alt_channel_name[0] : "modules.channels.setup." + this.name);

        // retrieve the rest of the configuration
        this.shortcut = (String) EC_API.validateConfigOptionValue( this.shortcut, config_route + ".shortcut", this.name );
        this.color = (String) EC_API.validateConfigOptionValue( this.color, config_route + ".color", this.name );
        this.format_chat_bubble = (String) EC_API.validateConfigOptionValue( this.format_chat_bubble, config_route + ".chat-bubble-format", this.name );
        this.only_show_chat_bubbles = (Boolean) EC_API.validateConfigOptionValue( this.only_show_chat_bubbles, config_route + ".hide-chat-only-show-chat-bubbles", this.name );
        this.auto_subscribe_newcomers = (Boolean) EC_API.validateConfigOptionValue( this.auto_subscribe_newcomers, config_route + ".auto-subscribe-newcomers", this.name );
        this.show_join_leave_messages = (Boolean) EC_API.validateConfigOptionValue( this.show_join_leave_messages, config_route + ".channel-join-leave-message", this.name );
        this.one_time_msg_pattern = (String) EC_API.validateConfigOptionValue( this.one_time_msg_pattern, config_route + ".one-time-channel-msg-character", this.name );
        this.distance = (Integer) EC_API.validateConfigOptionValue( this.distance, config_route + ".distance", this.name );
        this.hear_over_solid_blocks_distance = (Integer) EC_API.validateConfigOptionValue( this.hear_over_solid_blocks_distance, config_route + ".hear-over-solid-blocks-distance", this.name );
        this.excluded_worlds = (List) EC_API.validateConfigOptionValue( this.excluded_worlds, config_route + ".do-not-use-in-these-worlds", this.name );
        this.invert_excluded_worlds = (Boolean) EC_API.validateConfigOptionValue( this.invert_excluded_worlds, config_route + ".do-not-use-works-as-whitelist", this.name );

        // override global Spy channel permissions
        if ( null != alt_channel_name && 0 < alt_channel_name.length && alt_channel_name[0].equals( "modules.global-spy-channel" ) ) {
            this.join_permissions = Arrays.asList( "ec.global-spy-channel-cannot-be-joined" );
            this.subscribe_permissions = Arrays.asList( "ec.channels.spy.subscribe" );
            this.one_time_msg_permissions = Arrays.asList( "ec.global-spy-channel-cannot-be-quick-messaged" );
        } else {
            // normal channel
            this.join_permissions = (List) EC_API.validateConfigOptionValue( this.join_permissions, config_route + ".join-permissions", this.name );
            this.subscribe_permissions = (List) EC_API.validateConfigOptionValue( this.subscribe_permissions, config_route + ".subscribe-permissions", this.name );
            this.one_time_msg_permissions = (List) EC_API.validateConfigOptionValue( this.one_time_msg_permissions, config_route + ".one-time-msg-permissions", this.name );
        }

        this.password = (String) EC_API.validateConfigOptionValue( this.password, config_route + ".password", this.name );
        this.cooldown = (Integer) EC_API.validateConfigOptionValue( this.cooldown, config_route + ".cooldown", this.name );
        this.muted = (Boolean) EC_API.validateConfigOptionValue( this.muted, config_route + ".muted", this.name );
        this.subscriptions_allowed = (Boolean) EC_API.validateConfigOptionValue( this.subscriptions_allowed, config_route + ".subscriptions", this.name );
        this.banlist = (List) EC_API.validateConfigOptionValue( this.banlist, config_route + ".bans", this.name );
        this.mutelist = (List) EC_API.validateConfigOptionValue( this.mutelist, config_route + ".mutes", this.name );

        // if still set to false at the end of the first for loop,
        // we only have a simple text format without any JSON parameters
        // and we will use it to feed our format_colored_only
        boolean had_json_properties = false;

        // holds text-only representations of the JSON texts to send
        // used to fill format_colored_only and format_clear variables
        List<String> message_part_texts = new ArrayList<>();

        // set format for this channel from channel's configuration section
        try {
            // iterate over all message parts to create the resulting JSON output
            for (String message_part : EC_API.getConfigSectionKeys( config_route + ".format" )) {
                // add this into message_part_texts, so we can build clear log format from them
                message_part_texts.add( message_part );

                // this array will hold all parts of the final formatting
                // for this message part, since we might need to split
                // the message part into multiple texts if there is a HEX color in it
                String[] message_part_sections;
                final String hex_placeholder_text = "###HEX_COLOR###";
                final Pattern hex_with_text_patt  = Pattern.compile( Utils.hex_colors_pattern + ".+", Pattern.MULTILINE );
                Matcher matcher                   = hex_with_text_patt.matcher( message_part );

                if ( matcher.find() ) {
                    final Pattern pattern       = Pattern.compile( Utils.hex_colors_pattern, Pattern.MULTILINE );
                    matcher                     = pattern.matcher( message_part );
                    final String  placeholdered = matcher.replaceAll(  hex_placeholder_text + Utils.hex_colors_pattern_prefix + "$1");
                    message_part_sections       = placeholdered.split( hex_placeholder_text );
                } else {
                    message_part_sections = new String[]{ message_part };
                }

                // start building the final JSON format
                List<String> final_format = new ArrayList<>();

                for ( String message_part_section : message_part_sections ) {
                    if ( "".equals( message_part_section ) ) {
                        continue;
                    }

                    // if we have a hex color present for this message part,
                    // extract it and add it as a separate color JSON value
                    if ( message_part_section.matches( Utils.hex_colors_pattern + ".+" ) ) {
                        String color = message_part_section.substring( Utils.hex_colors_pattern_prefix.length(), 8 );
                        message_part_section = message_part_section.substring( message_part_section.indexOf( Utils.hex_colors_pattern_prefix ) + 8 );
                        final_format.add("{\"text\":\"" + message_part_section + "\",\"color\":\"#" + color + "\"");
                    } else {
                        // add the actual message part text
                        final_format.add("{\"text\":\"" + message_part_section + "\"");
                    }

                    // iterate over all JSON properties and set them up
                    for (String json_option : EC_API.getConfigSectionKeys(config_route + ".format." + message_part)) {
                        // determine which JSON property we're setting yp
                        switch ( json_option ) {
                            // text to insert into chatAliasing as the player is typing when they click on this message part
                            case "insertion":
                                final_format.add( "\"insertion\":\"" + EC_API.getConfigString(config_route + ".format." + message_part + "." + json_option) + "\"" );
                                had_json_properties = true;
                                break;

                            // hover text to display on player's mouse movement over this message part
                            case "hover":
                                String hover_component_text = EC_API.getConfigString(config_route + ".format." + message_part + "." + json_option);

                                // check whether we don't have HEX colors for this hover text,
                                // in which case we need to build a complex text component for this hover value
                                // in order to show them correctly
                                matcher = hex_with_text_patt.matcher( hover_component_text );
                                if ( matcher.find() ) {
                                    final Pattern pattern                  = Pattern.compile( Utils.hex_colors_pattern, Pattern.MULTILINE );
                                    matcher                                = pattern.matcher( hover_component_text );
                                    final String  placeholdered            = matcher.replaceAll( hex_placeholder_text + Utils.hex_colors_pattern_prefix + "$1" );
                                    String[]      hover_component_sections = placeholdered.split( hex_placeholder_text );

                                    // reset the text, so we can build it up again as a complex JSON component
                                    // with correct HEX coloring
                                    hover_component_text = "[";

                                    for ( String hover_component_section : hover_component_sections ) {
                                        // if we have a hex color present for this hover section,
                                        // extract it and add it as a separate color JSON value
                                        if ( hover_component_section.matches( Utils.hex_colors_pattern + ".+" ) ) {
                                            String color = hover_component_section.substring( Utils.hex_colors_pattern_prefix.length(), 8 );
                                            hover_component_section = hover_component_section.substring( hover_component_section.indexOf( Utils.hex_colors_pattern_prefix ) + 8 );
                                            hover_component_text += "{\"text\":\"" + hover_component_section + "\",\"color\":\"#" + color + "\"},";
                                        } else {
                                            // add the actual message part text
                                            hover_component_text += "{\"text\":\"" + hover_component_section + "\"},";
                                        }
                                    }

                                    // remove the final comma and add a closing bracket
                                    hover_component_text = hover_component_text.substring( 0, hover_component_text.length() - 1 ) + "]";
                                    final_format.add( "\"hoverEvent\":{\"action\":\"show_text\",\"value\":" + hover_component_text + "}" );
                                } else {
                                    final_format.add( "\"hoverEvent\":{\"action\":\"show_text\",\"value\":\"" + hover_component_text + "\"}" );
                                }

                                had_json_properties = true;
                                break;

                            // click on this message part event
                            case "click":
                                had_json_properties = true;
                                switch ( EC_API.getConfigSectionKeys(config_route + ".format." + message_part + ".click").iterator().next() ) {
                                    case "suggest":
                                        final_format.add( "\"clickEvent\":{\"action\":\"suggest_command\",\"value\":\"" + EC_API.getConfigString(config_route + ".format." + message_part + ".click.suggest" ) + "\"}" );
                                        break;

                                    case "command":
                                        // add the leading slash, if not present in the command
                                        String cmd = EC_API.getConfigString(config_route + ".format." + message_part + ".click.command" );
                                        if ( !cmd.startsWith("/") ) {
                                            cmd = "/" + json_option;
                                        }

                                        final_format.add( "\"clickEvent\":{\"action\":\"run_command\",\"value\":\"" + cmd + "\"}" );
                                        break;

                                    case "url":
                                        final_format.add( "\"clickEvent\":{\"action\":\"open_url\",\"value\":\"" + EC_API.getConfigString(config_route + ".format." + message_part + ".click.url" ) + "\"}" );
                                        break;

                                    case "clipboard":
                                        final_format.add( "\"clickEvent\":{\"action\":\"copy_to_clipboard\",\"value\":\"" + Utils.remove_ampersand_chat_color_values( message_part ) + "\"}" );
                                        break;
                                }
                        }
                    }

                    final_format.add( "}" );
                }

                this.format += ("".equals(this.format) ? "" : ",") + Utils.implode(final_format, ",");
            }

            // if we didn't have JSON properties, assemble the final format from the message_part_texts list
            if ( !had_json_properties ) {
                // assemble the final format from the message_part_texts list
                String imploded = Utils.implode(message_part_texts, "");
                this.format = "{\"text\":\"" + Utils.translate_chat_colors( imploded ) + "\"}";
                this.format_clear = imploded;
            } else {
                // finalize the JSON format and create the clear format for logging purposes
                this.format = Utils.translate_chat_colors( "[\"\"," + this.format + "]" );
                this.format_clear = Utils.implode(message_part_texts, "");
            }

            // remove extra commas that came in from multiple HEX colors formatting adjustment
            this.format = this.format.replaceAll( "\",}", "\"}" );
            this.format = this.format.replaceAll( "},}", "}}" );
        } catch (NullPointerException ex) {
            // no message parts found, treat this as simple text format representation
            this.format = "{\"text\":\"" + Utils.translate_chat_colors( EC_API.getConfigString(config_route + ".format")) + "\"}";
            this.format_clear = EC_API.getConfigString(config_route + ".format");
        }
    } //end method

    /**
     * Adds a player to the list of banned players in this channel.
     *
     * @param player_name Name of the player to ban.
     */
    void addBannedPlayer( String player_name ) {
        this.banlist.add( player_name );
        ((EasyChat) this.plugin).getConf().getConf().set("modules.channels.setup." + this.name + ".bans", this.banlist);
        ((EasyChat) this.plugin).getConf().getConf().saveConfig();
        Bukkit.getPluginManager().callEvent(new ECToggleDebugEvent());
    } // end method

    /**
     * Removes a banned player from the list of banned players in this channel.
     *
     * @param player_name Name of the player to unban.
     */
    void removeBannedPlayer( String player_name ) {
        this.banlist.remove( player_name );
        ((EasyChat) this.plugin).getConf().getConf().set("modules.channels.setup." + this.name + ".bans", this.banlist);
        ((EasyChat) this.plugin).getConf().getConf().saveConfig();
        Bukkit.getPluginManager().callEvent(new ECToggleDebugEvent());
    } // end method

    /**
     * Adds a player to the list of muted players in this channel.
     *
     * @param player_name Name of the player to mute.
     */
    void addMutedPlayer( String player_name ) {
        this.mutelist.add( player_name );
        ((EasyChat) this.plugin).getConf().getConf().set("modules.channels.setup." + this.name + ".mutes", this.mutelist);
        ((EasyChat) this.plugin).getConf().getConf().saveConfig();
        Bukkit.getPluginManager().callEvent(new ECToggleDebugEvent());
    } // end method

    /**
     * Removes a muted player from the list of muted players in this channel.
     *
     * @param player_name Name of the player to unmute.
     */
    void removeMutedPlayer( String player_name ) {
        this.mutelist.remove( player_name );
        ((EasyChat) this.plugin).getConf().getConf().set("modules.channels.setup." + this.name + ".mutes", this.mutelist);
        ((EasyChat) this.plugin).getConf().getConf().saveConfig();
        Bukkit.getPluginManager().callEvent(new ECToggleDebugEvent());
    } // end method

    /**
     * Mutes this channel.
     */
    public void muteChannel() {
        this.muted = true;
        ((EasyChat) this.plugin).getConf().getConf().set("modules.channels.setup." + this.name + ".muted", this.muted);
        ((EasyChat) this.plugin).getConf().getConf().saveConfig();
        Bukkit.getPluginManager().callEvent(new ECToggleDebugEvent());
    } // end method

    /**
     * Un-mutes this channel.
     */
    public void unmuteChannel() {
        this.muted = false;
        ((EasyChat) this.plugin).getConf().getConf().set("modules.channels.setup." + this.name + ".muted", this.muted);
        ((EasyChat) this.plugin).getConf().getConf().saveConfig();
        Bukkit.getPluginManager().callEvent(new ECToggleDebugEvent());
    } // end method

    /**
     * Checks whether the given player reached cooldown timestamp
     * needed to send next chat message in this channel.
     *
     * @param player Player for who to check the cooldown.
     *
     * @return Returns number of seconds a player needs to wait
     *         before they can send next message in this channel.
     */
    public int check_cooldown( UUID player ) {
        if ( this.cooldown == 0 ) {
            // no cooldown for this channel
            return 0;
        }

        // check cooldown for the player
        if ( this.last_msg.containsKey( player ) ) {
            // check if we've left cooldown period
            if ( Utils.getUnixTimestamp() >= this.last_msg.get( player ) + this.cooldown ) {
                // we're good with this player's cooldown
                this.last_msg.put( player, Utils.getUnixTimestamp() );
                return 0;
            } else {
                // return how much longer player needs to wait
                return (this.last_msg.get( player ) + this.cooldown) - Utils.getUnixTimestamp();
            }
        } else {
            // add new record with last message timestamp
            this.last_msg.put( player, Utils.getUnixTimestamp() );
            return 0;
        }
    } // end method

    public String getName() {
        return name;
    } //end method

    public String getShortcut() {
        return shortcut;
    } //end method

    public String getColor() {
        return color;
    } //end method

    public List<String> getSubscribe_permissions() {
        return subscribe_permissions;
    } //end method

    public List<String> getOne_time_msg_permissions() {
        return one_time_msg_permissions;
    } //end method

    public String getOne_time_msg_pattern() {
        return one_time_msg_pattern;
    } //end method

    public boolean getShow_join_leave_messages() {
        return show_join_leave_messages;
    } //end method

    public boolean getAuto_subscribe_newcomers() {
        return auto_subscribe_newcomers;
    } //end method

    public String getFormat() {
        return format;
    } //end method

    public String getFormat_clear() {
        return format_clear;
    } //end method

    public String getFormat_chat_bubble() { return format_chat_bubble; } // end method

    public int getDistance() {
        return distance;
    } //end method

    public int getHear_over_solid_blocks_distance() {
        return hear_over_solid_blocks_distance;
    } //end method

    public List<String> getExcluded_worlds() {
        return excluded_worlds;
    } //end method

    public boolean getInvert_excluded_worlds() {
        return invert_excluded_worlds;
    } //end method

    public List<String> getJoin_permissions() {
        return join_permissions;
    } //end method

    public String getPassword() {
        return password;
    } //end method

    public boolean isMuted() {
        return muted;
    } //end method

    public boolean getSubscriptions_allowed() {
        return subscriptions_allowed;
    } //end method

    public List<String> getBanlist() {
        return banlist;
    } //end method

    public List<String> getMutelist() {
        return mutelist;
    } //end method

    public boolean show_only_chat_bubbles() { return only_show_chat_bubbles; } // end method
} // end class