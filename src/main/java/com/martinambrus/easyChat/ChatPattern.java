package com.martinambrus.easyChat;

import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * A chatAliasing pattern used to replace various chatAliasing text portions
 * with other text or to execute commands when those portions are found.
 *
 * @author Martin Ambrus
 */
public final class ChatPattern {

    /**
     * The actual Java Regex Pattern to look for in a chatAliasing message.
     */
    private Pattern pattern;

    /**
     * Type of pattern replacement.
     * Can be one of: text, command
     */
    private String replacement_type;

    /**
     * Value with which to replace the pattern if found.
     */
    private String replacement_value;

    /**
     * An optional response to be sent to the player sending the message.
     */
    private String response;

    /**
     * Whether to cancel original chatAliasing message or leave it alone.
     */
    private boolean cancels_chat_message;

    /**
     * Constructor.
     *
     * @param pattern The actual Java Pattern to look for in chatAliasing.
     * @param replacement_type Type of pattern replacement. Can be one of: text, command
     * @param replacement_value Value with which to replace the pattern if found.
     * @param cancels_chat_message If set to true, original message will be cancelled.
     *                             If set to false, original message will be left alone.
     */
    ChatPattern( Pattern pattern, String replacement_type, String replacement_value, boolean cancels_chat_message ) {
        this.pattern = pattern;
        this.replacement_type = replacement_type;
        this.replacement_value = replacement_value;
        this.cancels_chat_message = cancels_chat_message;
    } //end method

    public Pattern getPattern() {
        return pattern;
    } //end method

    public String getReplacement_type() {
        return replacement_type;
    } //end method

    public String getReplacement_value() {
        return replacement_value;
    } //end method

    public boolean should_cancel_chat_message() {
        return cancels_chat_message;
    } //end method

    public String getResponse() {
        return response;
    } //end method

    public void setResponse(String response) {
        this.response = response;
    } //end method

} // end class