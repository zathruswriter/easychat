package com.martinambrus.easyChat;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.Plugin;

import java.util.*;

/**
 * ChatAliasing channels-related utilities and methods. Used to enable players
 * to join or listen to various chatAliasing channels or isolate themselves in a single one.
 *
 * @author Martin Ambrus
 */
@SuppressWarnings({"OverlyComplexClass", "VariableNotUsedInsideIf"})
final class Channels implements Listener {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * Name of this feature, used when reloading EC.
     */
    @SuppressWarnings("HardCodedStringLiteral")
    private final String featureName = "channels";

    /**
     * Default channel instance, if channels are enabled.
     */
    private Channel default_channel;

    /**
     * A fast-lookup map with all channels on the server
     * mapped to worlds in which they are enabled.
     * The key is a String value of "channel-name_world-name",
     * so we can easily check whether current channel is enabled
     * for current player's world.
     * The value is then the channel itself, so we can correctly
     * format player's message.
     */
    private Map<String, Channel> channel_to_world_map = new HashMap<>();

    /**
     * Map of channel prefixes used for quick-cross-channel-messaging.
     */
    private Map<String, Channel> prefix_to_channel_map = new HashMap<>();

    /**
     * Contains all players on the server and channels in which
     * they are currently talking.
     */
    private Map<UUID, Channel> player_to_channel_map = new HashMap<>();

    /**
     * Contains all players on the server and channels to which they are currently subscribed.
     */
    private Map<UUID, List<Channel>> player_to_subscription_map = new HashMap<>();

    /**
     * Contains channel in which a player was active before they isolated themselves
     * in a single (potentially other) channel. Used for restoration purposes.
     */
    private Map<UUID, Channel> player_to_pre_isolation_channel_map = new HashMap<>();

    /**
     * Holds all channels that this player was subscribed to before they decided
     * to isolate their communication to a single channel. Used for restoration purposes.
     */
    private Map<UUID, List<Channel>> player_to_pre_isolation_subscriptions_map = new HashMap<>();

    /**
     * Channel used to format broadcast messages sent via /say - if enabled.
     */
    private Channel broadcast_channel;

    /**
     * Channel used to format broadcast messages sent via /tell or /msg - if enabled.
     */
    private Channel private_channel;

    /**
     * Channel used to spy on all messages globally.
     */
    private Channel spy_channel;

    /**
     * List of players ignored by other players.
     * This is a quick-lookup map where keys are structured like this:
     * ignoring_player_uuid-ignored_player_uuid
     * Value is not used and is always set to true.
     */
    private Map<String, Boolean> ignores_list = new HashMap<>();

    /**
     * Constructor, creates a new instance of the Channels class
     * and creates all channels in the configuration.
     *
     * @param ec Instance of {@link EasyChat EasyChat}.
     */
    Channels(final Plugin ec) {
        plugin = ec;

        // check if we have channels enabled, and if so,
        // load them into all the maps where they should go
        if ( EC_API.isModuleEnabled( this.featureName ) ) {
            for (String channel_name : EC_API.getConfigSectionKeys("modules.channels.setup")) {
                Channel ch = new Channel( this.plugin, channel_name );
                this.prefix_to_channel_map.put( ch.getOne_time_msg_pattern(), ch );

                // if this is a default channel, store it as such
                if ( ch.getName().equals( EC_API.getConfigString("modules.channels.default-channel-name") ) ) {
                    this.default_channel = ch;
                }

                // check if this is a global world
                if ( ch.getExcluded_worlds().isEmpty() ) {
                    this.channel_to_world_map.put( ch.getName() + "_*", ch );
                } else {
                    // only add worlds where this channel is enabled
                    for ( World world : Bukkit.getWorlds() ) {
                        if (
                            // if we're not using the excluded worlds as an include list,
                            // check that we don't have this world in it
                            ( !ch.getInvert_excluded_worlds() && !ch.getExcluded_worlds().contains( world.getName()) )
                            ||
                            // if we're using the excluded worlds as an include list,
                            // add this world into the worlds list enabled for this channel
                            ( ch.getInvert_excluded_worlds() && ch.getExcluded_worlds().contains( world.getName()) )
                        ) {
                            this.channel_to_world_map.put(ch.getName() + "_" + world.getName(), ch );
                        }
                    }
                }
            }
        } else if ( EC_API.isModuleEnabled("default-chat-format") ) {
            // only default chat is enabled, create a pseudo-channel for it
            Channel ch = new Channel( this.plugin, "default-chat-format", "modules.default-chat-format" );
            this.prefix_to_channel_map.put( ch.getOne_time_msg_pattern(), ch );
            this.channel_to_world_map.put( "*", ch );
        }

        if ( EC_API.isModuleEnabled("broadcasts") ) {
            this.broadcast_channel = new Channel( this.plugin, "@broadcasts", "modules.broadcasts" );
        }

        if ( EC_API.isModuleEnabled("private-chat") ) {
            this.private_channel = new Channel( this.plugin, "@private", "modules.private-chat" );
        }

        if ( EC_API.isModuleEnabled("global-spy-channel") ) {
            this.spy_channel = new Channel( this.plugin, "@global_spy", "modules.global-spy-channel" );
            EC_API.startRequiredListener("spyChannelListener");
        }

        // start the chat listener
        EC_API.startRequiredListener("asyncChatListener");

        // add ourselves as a listener
        EC_API.startRequiredListener( this.featureName, this );

        // if we've reloaded, we'll have players on the server, so let's re-chat-join them
        if ( Bukkit.getOnlinePlayers().size() > 0 ) {
            for ( Player player : Bukkit.getOnlinePlayers() ) {
                this.channelize_joining_player( player );

                // load this player's ignore list
                this.ignores_list.putAll( EC_API.get_player_ignores_list( player ) );
            }
        }
    } //end method

    /**
     * Returns the number of channels we were able to successfully
     * add to this class.
     *
     * @return Returns number of active channels.
     */
    int channels_count() {
       return channel_to_world_map.size();
    } // end method

    /**
     * Retrieves channel in which the given player is talking.
     *
     * @param player UUID of player to return the chat channel for.
     *
     * @return Returns channel in which the given player is talking.
     */
    Channel get_player_channel( UUID player ) {
        return this.player_to_channel_map.get( player );
    } // end method

    /**
     * Checks whether a player is subscribed to the given channel.
     *
     * @param player Player to check subscription for.
     * @param channel Channel to check subscription in.
     *
     * @return Returns true if player is subscribed to the given channel, false otherwise.
     */
    boolean is_subscribed_to_channel( UUID player, Channel channel ) {
        return ( this.player_to_subscription_map.containsKey( player ) && this.player_to_subscription_map.get( player ).contains( channel ) );
    } // end method

    /**
     * Checks whether chat message doesn't start with any
     * of the quick chat prefixes in any of the channels.
     * If it does,
     *
     * @param message Message to check for cross-channel chat prefix.
     *
     * @return Returns the channel which quick-chat prefix was matched in the message.
     *         If no matches were found, this method returns null.
     */
    Channel check_quick_chat_prefix( String message ) {
        if ( !EC_API.isModuleEnabled( "channels" )) {
            return null;
        }

        for ( Channel channel : this.prefix_to_channel_map.values() ) {
            if ( !message.equals( channel.getOne_time_msg_pattern() ) && message.startsWith( channel.getOne_time_msg_pattern() ) ) {
                return channel;
            }
        }

        return null;
    } // end method

    /**
     * Checks that the given channel is enabled in the given world.
     *
     * @param channel_name Name of the channel.
     * @param world_name Name of the world to check channel enabled in.
     *
     * @return Returns true if the the given channel is enabled in the given world,
     *         otherwise returns false.
     */
    boolean check_channel_world_enabled( String channel_name, String world_name ) {
        return (
            this.channel_to_world_map.containsKey( channel_name + "_" + world_name )
            ||
            this.channel_to_world_map.containsKey( channel_name + "_*" )
        );
    } // end method

    /**
     * Retrieves list of all channels on the server.
     *
     * @return Returns list of all channels on the server.
     */
    List<Channel> get_all_channels() {
        List<Channel> channels = new ArrayList<>();

        for ( Channel channel : this.prefix_to_channel_map.values() ) {
            channels.add( channel );
        }

        return channels;
    } // end method

    /**
     * Retrieves a channel based on its name.
     *
     * @param channel_name Name of the channel to retrieve.
     *
     * @return Returns the channel requested by its name.
     */
    Channel get_channel_by_name( String channel_name ) {
        for ( Channel channel : this.prefix_to_channel_map.values() ) {
            if ( channel.getName().equals( channel_name ) ) {
                return channel;
            }
        }

        if ( channel_name.equals( this.broadcast_channel.getName() ) ) {
            return this.broadcast_channel;
        }

        if ( channel_name.equals( this.private_channel.getName() ) ) {
            return this.private_channel;
        }

        if ( channel_name.equals( this.spy_channel.getName() ) ) {
            return this.spy_channel;
        }

        return null;
    } // end method

    /**
     * Gets the channel used to send broadcasts via /say.
     *
     * @return Returns the channel instance used to send broadcasts via /say.
     */
    Channel get_broadcast_channel() {
        return this.broadcast_channel;
    } // end method

    /**
     * Gets the channel used to private message via /tell or /msg.
     *
     * @return Returns the channel used to private message via /tell or /msg.
     */
    Channel get_private_channel() {
        return this.private_channel;
    } // end method

    /**
     * Gets the spy channel used to show all messages on the server.
     *
     * @return Returns the spy channel used to show all messages on the server.
     */
    Channel get_spy_channel() {
        return this.spy_channel;
    } // end method

    /**
     * Makes the player join the given channel.
     *
     * @param player_uuid Unique ID of the player to join the channel.
     * @param ch Channel to join.
     */
    void join_channel( UUID player_uuid, Channel ch ) {
        // handle channel join/leave notifications
        Channel old_channel = this.player_to_channel_map.get( player_uuid );
        Player player = Bukkit.getPlayer( player_uuid );
        if (
            !EC_API.checkPerms( player, "ec.bypass.channel-join-leave-notification", false ) &&
            (old_channel.getShow_join_leave_messages() || ch.getShow_join_leave_messages() )
        ) {
            String player_name = player.getDisplayName();

            // handle old channel leave notification
            if ( old_channel.getShow_join_leave_messages() ) {
                for ( Map.Entry<UUID, Channel> entry : this.player_to_channel_map.entrySet() ) {
                    if ( !entry.getKey().equals( player_uuid ) && entry.getValue().equals( old_channel ) ) {
                        Bukkit.getPlayer( entry.getKey() ).sendMessage( EC_API.__( "channel.player-left", player_name, old_channel.getColor() + old_channel.getName() ) );
                    }
                }

                for ( Map.Entry<UUID, List<Channel>> entry : this.player_to_subscription_map.entrySet() ) {
                    if ( !entry.getKey().equals( player_uuid ) && entry.getValue().contains( old_channel ) ) {
                        Bukkit.getPlayer( entry.getKey() ).sendMessage( EC_API.__( "channel.player-left", player_name, old_channel.getColor() + old_channel.getName() ) );
                    }
                }
            }

            // handle new channel join notification
            if ( ch.getShow_join_leave_messages() ) {
                for ( Map.Entry<UUID, Channel> entry : this.player_to_channel_map.entrySet() ) {
                    if ( !entry.getKey().equals( player_uuid ) && entry.getValue().equals( ch ) ) {
                        Bukkit.getPlayer( entry.getKey() ).sendMessage( EC_API.__( "channel.player-joined", player_name, ch.getColor() + ch.getName() ) );
                    }
                }

                for ( Map.Entry<UUID, List<Channel>> entry : this.player_to_subscription_map.entrySet() ) {
                    if ( !entry.getKey().equals( player_uuid ) && entry.getValue().contains( ch ) ) {
                        Bukkit.getPlayer( entry.getKey() ).sendMessage( EC_API.__( "channel.player-joined", player_name, ch.getColor() + ch.getName() ) );
                    }
                }
            }
        }

        this.player_to_channel_map.replace( player_uuid, ch );
    } // end method

    /**
     * Subscribes given player to the channel.
     *
     * @param player_uuid Unique ID of the player to subscribe.
     * @param ch Channel to subscribe to.
     *
     * @return Returns true if player was successfully subscribed,
     *         false if they are already subscribed to the provided channel.
     */
    boolean subscribe_channel( UUID player_uuid, Channel ch ) {
        if ( !this.player_to_subscription_map.containsKey( player_uuid ) ) {
            this.player_to_subscription_map.put( player_uuid, new ArrayList<>() );
        }

        if ( this.player_to_subscription_map.get( player_uuid ).contains( ch ) ) {
            return false;
        } else {
            this.player_to_subscription_map.get( player_uuid ).add( ch );
            return true;
        }
    } // end method

    /**
     * Unsubscribes given player from the channel.
     *
     * @param player_uuid Unique ID of the player to unsubscribe.
     * @param ch Channel to unsubscribe from.
     *
     * @return Returns true if player was successfully unsubscribed,
     *         false if they are not subscribed to the provided channel.
     */
    boolean unsubscribe_channel( UUID player_uuid, Channel ch ) {
        if ( !this.player_to_subscription_map.containsKey( player_uuid ) || !this.player_to_subscription_map.get( player_uuid ).contains( ch ) ) {
            return false;
        }

        this.player_to_subscription_map.get( player_uuid ).remove( ch );
        return true;
    } // end method

    /**
     * Retrieves list of channels the given player is subscribed to.
     *
     * @param player_uuid Player unique ID to check for subscribed channels.
     *
     * @return Returns list of channels the given player is subscribed to.
     */
    List<Channel> get_player_subscriptions_list( UUID player_uuid ) {
        if ( !this.player_to_subscription_map.containsKey( player_uuid ) ) {
            return new ArrayList<>();
        } else {
            return this.player_to_subscription_map.get( player_uuid );
        }
    } // end method

    /**
     * Bans player from a single channel.
     *
     * @param player_name The player we are about to ban.
     * @param channel Channel in which the player should be banned.
     */
    boolean ban_player_in_channel( String player_name, Channel channel ) {
        if ( !channel.getBanlist().contains( player_name ) ) {
            channel.addBannedPlayer( player_name );
            return true;
        } else {
            return false;
        }
    } // end method

    /**
     * Un-bans player from a single channel.
     *
     * @param player_name The player we are about to un-ban.
     * @param channel Channel from which the player should be un-banned.
     */
    boolean unban_player_in_channel( String player_name, Channel channel ) {
        if ( channel.getBanlist().contains( player_name ) ) {
            channel.removeBannedPlayer( player_name );
            return true;
        } else {
            return false;
        }
    } // end method

    /**
     * Mutes player in a single channel.
     *
     * @param player_name The player we are about to mute.
     * @param channel Channel in which the player should be muted.
     */
    boolean mute_player_in_channel( String player_name, Channel channel ) {
        if ( !channel.getMutelist().contains( player_name ) ) {
            channel.addMutedPlayer( player_name );
            return true;
        } else {
            return false;
        }
    } // end method

    /**
     * Un-mutes player in a single channel.
     *
     * @param player_name The player we are about to un-mute.
     * @param channel Channel from which the player should be un-muted.
     */
    boolean unmute_player_in_channel( String player_name, Channel channel ) {
        if ( channel.getMutelist().contains( player_name ) ) {
            channel.removeMutedPlayer( player_name );
            return true;
        } else {
            return false;
        }
    } // end method

    /**
     * Isolates player in a single channel and backs up their old
     * channel and subscriptions, so they can be restored once player
     * decides to leave the isolation (and if enabled in config).
     *
     * @param player_uuid The player who is about to be isolated.
     * @param channel Channel into which the player will be isolated.
     */
    void isolate_player_in_channel( UUID player_uuid, Channel channel ) {
        // backup current player's channel and subscriptions, if enabled
        if ( EC_API.isModuleEnabled( "channels.isolate-command-remembers-subscriptions" ) ) {
            // if already isolated, un-isolate first, then isolate again
            if ( this.player_to_pre_isolation_channel_map.containsKey( player_uuid ) ) {
                this.unisolate_player( player_uuid );
            }

            // isolate
            if ( this.player_to_channel_map.containsKey( player_uuid ) ) {
                this.player_to_pre_isolation_channel_map.put( player_uuid, this.player_to_channel_map.get( player_uuid ) );
                this.player_to_channel_map.remove( player_uuid );
            }

            if ( this.player_to_subscription_map.containsKey( player_uuid ) ) {
                this.player_to_pre_isolation_subscriptions_map.put( player_uuid, this.player_to_subscription_map.get( player_uuid ) );
                this.player_to_subscription_map.remove( player_uuid );
            }
        } else {
            // no remembering, just remove everything
            this.player_to_channel_map.remove( player_uuid );
            this.player_to_subscription_map.remove( player_uuid );
        }

        // change player's channel to the requested one
        this.player_to_channel_map.put( player_uuid, channel );
    } // end method

    /**
     * Returns player's old subscriptions and channel settings prior to
     * their isolation in a single channel.
     *
     * @param player_uuid Unique ID of the player we want to un-isolate.
     *
     * @return Returns true if the player was previously isolated, false otherwise.
     */
    boolean unisolate_player( UUID player_uuid ) {
        if (
            EC_API.isModuleEnabled( "channels.isolate-command-remembers-subscriptions" )
            &&
            this.player_to_pre_isolation_channel_map.containsKey( player_uuid )
        ) {
            if ( this.player_to_pre_isolation_channel_map.containsKey( player_uuid ) ) {
                this.player_to_channel_map.replace( player_uuid, this.player_to_pre_isolation_channel_map.get( player_uuid ) );
                this.player_to_pre_isolation_channel_map.remove( player_uuid );
            }

            if ( this.player_to_pre_isolation_subscriptions_map.containsKey( player_uuid ) ) {
                this.player_to_subscription_map.put( player_uuid, this.player_to_pre_isolation_subscriptions_map.get( player_uuid ) );
                this.player_to_pre_isolation_subscriptions_map.remove( player_uuid );
            }

            return true;
        } else {
            return false;
        }
    } // end method

    /**
     * Adds a new player into the list of ignores.
     *
     * @param player Player who is adding the ignore.
     * @param ignore Player who should be ignored by the first player.
     */
    void add_player_ignore( Player player, Player ignore ) {
        this.ignores_list.put( player.getUniqueId() + "_" + ignore.getUniqueId(), true );
        ((EasyChat) this.plugin).getLocalStorageUtils().set_player_ignores_list( player, this.get_player_ignores( player ) );
    } // end method

    /**
     * Removes ignore from the ignores list.
     *
     * @param player The player who is removing their ignore.
     * @param ignore The ignored player that should be un-ignored.
     */
    void remove_player_ignore( Player player, Player ignore ) {
        this.ignores_list.remove( player.getUniqueId() + "_" + ignore.getUniqueId() );
        ((EasyChat) this.plugin).getLocalStorageUtils().set_player_ignores_list( player, this.get_player_ignores( player ) );
    } // end method

    /**
     * Checks whether the given ignored player is ignored by the first player.
     *
     * @param player The player for who to check the ignore.
     * @param ignore The ignored player to check whether they are ignored.
     *
     * @return Returns true if the second player is ignored by the first one, false otherwise.
     */
    boolean is_player_ignored(Player player, Player ignore ) {
        return this.ignores_list.containsKey( player.getUniqueId() + "_" + ignore.getUniqueId() );
    } // end method

    /**
     * Retrieves all ignores for a single player.
     *
     * @param player The player to retrieve ignores for.
     *
     * @return Returns list of all ignores for the given player,
     *         so they can be stored in a DB.
     */
    List<String> get_player_ignores( Player player ) {
        String start_check   = player.getUniqueId() + "_";
        List<String> ignores = new ArrayList<>();

        for ( String ignore : this.ignores_list.keySet() ) {
            if ( ignore.startsWith( start_check ) ) {
                ignores.add( ignore.replace( start_check, "" ) );
            }
        }

        return ignores;
    } // end method

    /**
     * Returns default channel for players on the server.
     *
     * @return Returns default channel for players on the server.
     */
    Channel get_default_channel() {
        return this.default_channel;
    } // end method

    /**
     * When player joins the server or EasyChat is reloaded,
     * this procedure will set their correct chat parameters.
     *
     * @param player
     */
    void channelize_joining_player( Player player ) {
        // check that we have channels enabled
        if (
            EC_API.isModuleEnabled( "channels" )
                &&
                this.channels_count() > 0
        ) {
            String player_channel = "";
            Channel real_player_channel = null;

            // check that the player doesn't actually have a channel joined already
            if ( player.hasPlayedBefore() ) {
                player_channel = EC_API.get_player_channel( player );

                // check that player's channel still exists
                if ( null != player_channel ) {
                    for (Channel channel : this.prefix_to_channel_map.values()) {
                        if (channel.getName().equals(player_channel)) {
                            real_player_channel = channel;
                            break;
                        }
                    }
                }
            }

            // player doesn't have a channel joined, let's auto-join them
            if ( null == real_player_channel ) {
                // check that we have a default channel set
                String  default_channel = EC_API.getConfigString("modules.channels.default-channel-name");

                // check that the default channel is set
                if (null != default_channel) {
                    // check if the default channel actually exists
                    boolean default_found_ok = false;
                    for (Channel channel : this.prefix_to_channel_map.values()) {
                        if (channel.getName().equals(default_channel)) {
                            default_found_ok = true;
                            real_player_channel = channel;
                            break;
                        }
                    }

                    // invalid default channel, make the first default and warn in console
                    if (!default_found_ok) {
                        String bad_default = default_channel;
                        real_player_channel = this.prefix_to_channel_map.values().iterator().next();
                        default_channel = real_player_channel.getName();
                        this.default_channel = real_player_channel;
                        EC_API.generateConsoleWarning(EC_API.__("error.channel-invalid-default-channel", bad_default, default_channel));
                    }
                } else {
                    // no default channel, make the first one available default
                    // and log warning to console
                    real_player_channel = this.prefix_to_channel_map.values().iterator().next();
                    default_channel = real_player_channel.getName();
                    this.default_channel = real_player_channel;
                    EC_API.generateConsoleWarning(EC_API.__("error.channel-no-default-channel", default_channel));
                }

                // save player's channel
                EC_API.set_player_channel( player, real_player_channel.getName() );
            }

            // set player's channel
            this.player_to_channel_map.put( player.getUniqueId(), real_player_channel );

            // check subscriptions of this player
            if ( player.hasPlayedBefore() ) {
                // make sure we have this player's key in the subscriptions map
                if ( !this.player_to_subscription_map.containsKey( player.getUniqueId() ) ) {
                    this.player_to_subscription_map.put( player.getUniqueId(), new ArrayList<>() );
                }

                // split subscriptions and check them 1 by 1
                List<String> valid_subscriptions = new ArrayList<>();
                List<String> stored_subscriptions = EC_API.get_player_subscriptions( player );
                String spy_channel_name = ( null != this.spy_channel ? this.spy_channel.getName() : "" );

                for ( String subscription : stored_subscriptions ) {
                    // check if this isn't a global Spy subscription
                    if ( !spy_channel_name.equals( "" ) && subscription.equals( spy_channel_name ) ) {
                        valid_subscriptions.add( subscription );
                        this.player_to_subscription_map.get( player.getUniqueId() ).add( this.spy_channel );
                    } else {
                        for ( Channel channel : this.prefix_to_channel_map.values() ) {
                            // subscription found, add it to the map
                            if ( subscription.equals( channel.getName() ) && channel.getSubscriptions_allowed() ) {
                                valid_subscriptions.add( subscription );
                                this.player_to_subscription_map.get( player.getUniqueId() ).add( channel );
                            }
                        }
                    }
                }

                // if we couldn't find any 1 of the old subscription channels,
                // update player's meta
                if ( !Utils.implode( stored_subscriptions, "," ).equals( Utils.implode(valid_subscriptions, ",") ) ) {
                    EC_API.set_player_subscriptions( player, Utils.implode(valid_subscriptions, ",") );
                }
            } else {
                List<String> auto_subscriptions = new ArrayList<>();
                UUID player_uuid = player.getUniqueId();
                this.player_to_subscription_map.put( player_uuid, new ArrayList<>() );

                // auto-subscribe first time players to all channels that have auto-subscription active
                for ( Channel channel : this.prefix_to_channel_map.values() ) {
                    if ( channel.getAuto_subscribe_newcomers() && channel.getSubscriptions_allowed() ) {
                        this.player_to_subscription_map.get( player_uuid ).add(channel);
                        auto_subscriptions.add( channel.getName() );
                    }
                }

                // store subscriptions into player's meta data
                if ( !this.player_to_subscription_map.get( player_uuid ).isEmpty() ) {
                    EC_API.set_player_subscriptions( player, Utils.implode(auto_subscriptions, ",") );
                }
            }
        } else {
            // default chat enabled and no channels active,
            // put player into the global chat channel
            this.player_to_channel_map.put( player.getUniqueId(), this.channel_to_world_map.get("*") );
        }
    } // end method

    /***
     * When a player joins the server for the first time,
     * this listener will add meta data to them with the default
     * channel which they would auto-join.
     *
     * If channels are disabled, no meta data are added.
     *
     * @param e The actual player join event to work with.
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void addFirstJoinMeta(final PlayerJoinEvent e) {
        this.channelize_joining_player( e.getPlayer() );

        // load this player's ignore list
        this.ignores_list.putAll( EC_API.get_player_ignores_list( e.getPlayer() ) );
    } // end method

    /***
     * When a player leaves the server, clean up their data in memory.
     *
     * @param e The actual player quit event to work with.
     */
    @EventHandler(priority = EventPriority.MONITOR)
    public void cleanUpPlayerData(final PlayerQuitEvent e) {
        UUID player_uuid = e.getPlayer().getUniqueId();

        this.player_to_subscription_map.remove( player_uuid );
        this.player_to_channel_map.remove( player_uuid );
        this.player_to_subscription_map.remove( player_uuid );
        this.player_to_pre_isolation_channel_map.remove( player_uuid );
        this.player_to_pre_isolation_subscriptions_map.remove( player_uuid );

    } // end method

} // end class