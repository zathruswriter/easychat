package com.martinambrus.easyChat;

import com.martinambrus.easyChat.events.ECChatEvent;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import java.util.ArrayList;

/**
 * Handles chat bubbles displaying for the player.
 *
 * @author atesin
 */
public class ChatBubbles implements Listener {
    /**
     * Number of characters to add to the display time
     * calculations.
     */
    private int handicapChars;

    /**
     * Reading speed, in characters per minute.
     */
    private int readSpeed;

    /**
     * Constructor.
     */
    public ChatBubbles() {
        String read_speed_config_route = "modules.chat-bubbles.read-speed";
        try {
            this.readSpeed = Integer.parseInt( EC_API.getConfigString( read_speed_config_route ) );
        } catch (Exception ex) {
            this.readSpeed = 800;
            EC_API.generateConsoleWarning( EC_API.__("chatbubbles.invalid-int-value", read_speed_config_route, 800 ) );
        }

        String read_speed_adjust_config_route = "modules.chat-bubbles.read-speed-adjustment";
        try {
            this.handicapChars = Integer.parseInt( EC_API.getConfigString( read_speed_adjust_config_route ) );
        } catch (Exception ex) {
            this.handicapChars = 10;
            EC_API.generateConsoleWarning( EC_API.__("chatbubbles.invalid-int-value", read_speed_adjust_config_route, 10 ) );
        }

        EC_API.startRequiredListener( "chat-bubbles", this );
    } // end method

    /**
     * Calculates display duration in ticks, so previous method can schedule next.
     *
     * @param player Player to perform the calculations for.
     * @param chat Chat message to be displayed.
     *
     * @return Returns display duration for the given chat message
     */
    int receiveMessage( Player player, String chat ) {
        // prepare chat message and empty bubble
        String[] chatLines = chat.split( "\n" );
        new ArrayList<LivingEntity>();

        // calculate bubble duration, 1200 = ticks per minute, to convert readSpeed to ticks
        int      duration   = (chat.length() + (this.handicapChars * chatLines.length)) * 1200 / this.readSpeed;
        Location spawnPoint = player.getLocation();
        spawnPoint.setY( -1 );

        // spawn name tags from bottom to top
        Entity vehicle = player;
        for ( int i = chatLines.length - 1; i >= 0; --i ) {
            vehicle = spawnNameTag( vehicle, chatLines[ i ], spawnPoint, duration, player );
        }
        return duration;
    } // end method

    /**
     * Spawns a name tag and returns it to caller,
     * so multiple tags can stack together.
     *
     * @param vehicle A vehicle this name tag will be tied to.
     * @param text Text to display on this name tag.
     * @param spawnPoint Location for the name tag.
     * @param duration How long should the tag remain visible.
     * @param player Player for which we're creating the name tag.
     *               Needed to get the message format through their current channel.
     *
     * @return Returns a name tag which can be positioned above player's head.
     */
    private AreaEffectCloud spawnNameTag( Entity vehicle, String text, Location spawnPoint, int duration, Player player ) {
        // spawn name tag away from player in same chunk, then set invisible
        AreaEffectCloud nameTag = (AreaEffectCloud) spawnPoint.getWorld().spawnEntity( spawnPoint, EntityType.AREA_EFFECT_CLOUD );
        nameTag.setParticle( Particle.TOWN_AURA ); // ITEM_TAKE was deprecated so i found mycelium (TOWN_AURA) has the tiniest particle
        nameTag.setRadius( 0 );

        // mount over vehicle and set name
        vehicle.addPassenger( nameTag );
        nameTag.setCustomName( Utils.translate_chat_colors( EC_API.get_player_channel( player.getUniqueId() ).getFormat_chat_bubble().replace( "{MESSAGE}", text ) ) );
        nameTag.setCustomNameVisible( true );

        // set duration and return
        nameTag.setWaitTime( 0 );
        nameTag.setDuration( duration );
        return nameTag;
    } // end method

    /***
     * Displays a chat bubble on top of player's head upon
     * sending a chat message.
     *
     * @param e The actual event to work with.
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void showChatBubble(final ECChatEvent e) {
        if ( e.isCancelled() ) {
            return;
        }

        if ( e.getPlayer() instanceof Player ) {
            // check that current channel has chat bubbles enabled
            if ( !EC_API.get_channel_by_name( e.getChannelName() ).getFormat_chat_bubble().equals( "" ) ) {
                EC_API.showChatBubble( (Player) e.getPlayer(), e.getOriginalMessage() );
            }
        }
    } // end method

} // end class