package com.martinambrus.easyChat;

import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MultiverseWorld;
import me.clip.placeholderapi.PlaceholderAPI;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import static org.bukkit.Bukkit.getServer;

/**
 * Placeholders replacement and handling class.
 *
 * @author Martin Ambrus
 */
final class Placeholders {

    /**
     * Will be set to true if we can find PlaceholderAPI plugin
     * on the server. PlaceholderAPI is used to replace various
     * predefined placeholders into valid data.
     */
    boolean placeholderapi_found = false;

    /**
     * Constructor, checks for PAPI plugin being found on the server.
     */
    Placeholders() {
        if (null != Bukkit.getPluginManager().getPlugin("PlaceholderAPI")) {
            this.placeholderapi_found = true;
        }
    }

    /**
     * Replaces all valid placeholders in the given text
     * with their real data representations.
     *
     * @param txt The text in which to search for placeholders and replace them.
     * @param sender The actual person sending the message we want to replace placeholders for.
     * @param channel The channel in which this message is being show, so we can replace
     *                all of the channel-related placeholders. Can be null to use this method
     *                to replace non-channel messages.
     *
     * @return Returns txt with all placeholders replaced by their real values.
     */
    String replace_placeholders(String txt, CommandSender sender, Channel channel) {
        // channel name and prefix
        if ( null != channel ) {
            txt = txt.replace( "{CHANNEL_NAME}", channel.getName() );
            txt = txt.replace( "{CHANNEL_SHORTCUT}", channel.getShortcut() );
        }

        // replace keys from our language file
        txt = Utils.replace_translation_placeholders( txt );

        // real player name (mostly used for running commands)
        txt = txt.replace( "{PLAYERNAME}", sender.getName() );

        // displayed player name (usually color-formatted from perm plugins)
        if ( sender instanceof Player ) {
            Player player = (Player) sender;
            boolean can_use_vaultChat = false;
            Chat vaultChat = null;

            // check that we can use vaultChat
            if ( EC_API.isVaultEnabled() ) {
                vaultChat = getServer().getServicesManager().load( Chat.class );

                // if we cannot use vaultChat, it's probable that there is no perms plugin on the server
                if ( null != vaultChat ) {
                    can_use_vaultChat = true;
                }
            }

            if ( can_use_vaultChat) {
                String prefix = vaultChat.getPlayerPrefix( player );
                if ( null == prefix || prefix.equals("") ) {
                    prefix = vaultChat.getGroupPrefix( player.getWorld(), EC_API.getPlayerPrimaryPermGroup( player ) );
                }

                if ( null != prefix && !prefix.equals("") ) {
                    prefix = Utils.translate_chat_colors( prefix );
                }

                String suffix = vaultChat.getPlayerSuffix( player );
                if ( null == suffix || suffix.equals("") ) {
                    suffix = vaultChat.getGroupSuffix( player.getWorld(), EC_API.getPlayerPrimaryPermGroup( player ) );
                }

                if ( null != suffix && !suffix.equals("") ) {
                    suffix = Utils.translate_chat_colors( suffix );
                }

                if ( prefix == null ) {
                    prefix = "";
                }

                if ( suffix == null ) {
                    suffix = "";
                }

                txt = txt.replace("{DISPLAYNAME}", prefix + player.getDisplayName() + suffix);
            } else {
                txt = txt.replace("{DISPLAYNAME}", player.getDisplayName());
            }

            // player's current world name
            // try MultiVerse Core Nickname first
            Plugin mv_core = Bukkit.getPluginManager().getPlugin("Multiverse-Core");
            if ( null != mv_core ) {
                MultiverseCore mv_plugin = (MultiverseCore) mv_core;
                MultiverseWorld mv_world = mv_plugin.getMVWorldManager().getMVWorld( ((Player) sender).getWorld().getName() );
                txt = txt.replace( "{WORLDNAME}", ( !mv_world.getAlias().equals("") ? mv_world.getAlias() : mv_world.getName() ) );
            } else {
                txt = txt.replace("{WORLDNAME}", ((Player) sender).getWorld().getName());
            }
        } else {
            txt = txt.replace("{DISPLAYNAME}", sender.getName() );

            // no world for console
            txt = txt.replace("{WORLDNAME}", "" );
        }

        if ( sender instanceof Player && EC_API.isVaultEnabled() && null != EC_API.getPlayerPrimaryPermGroup( (Player) sender ) ) {
            // Vault-backed player's group name
            txt = txt.replace("{GROUP}", EC_API.getPlayerPrimaryPermGroup( (Player) sender ) );
        } else if ( !(sender instanceof Player) ) {
            // no group for console or no group found
            txt = txt.replace("{GROUP}", "" );
        }

        // if we have PlaceholderAPI on the server, replace its own placeholders
        if ( this.placeholderapi_found && sender instanceof Player ) {
            txt = PlaceholderAPI.setPlaceholders((Player) sender, txt);
        }

        return txt;
    } // end method

} // end class