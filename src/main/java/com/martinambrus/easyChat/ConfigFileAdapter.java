package com.martinambrus.easyChat;

import com.google.common.base.Charsets;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * File configuration class, used to read and write
 * configuration of EasyChat from/into a YAML config file.
 *
 * @author Martin Ambrus
 */
@SuppressWarnings("HardCodedStringLiteral")
final class ConfigFileAdapter extends ConfigAbstractAdapter {

    // configuration adapter to provide YAML methods used by EasyChat
    private ConfigSectionFileAdapter conf;

    // file name of the YAML configuration file for EasyChat
    private final String configFileName = "config-file.yml";

    // file name of the sample YAML configuration file for EasyChat
    private final String configFileNameSample = "config-file-sample.yml";

    /**
     * Constructor, loads plugin configuration.
     *
     * @param ec The singleton instance of EasyChat.
     */
    ConfigFileAdapter(final EasyChat ec) {
        plugin = ec;

        // copy over sample config file with comments in all instances
        this.plugin.saveResource(this.configFileNameSample, true);

        // check if we're running EC for the first time,
        // in which case copy over the sample file-based config file
        File newConfigFile = new File( EC_API.getEcDataDir(), this.configFileName );
        if ( !newConfigFile.exists() ) {
            // no file-based config file found, copy over the sample one
            Path copied       = Paths.get(EC_API.getEcDataDir() + "/" + this.configFileName);
            Path original     = Paths.get(EC_API.getEcDataDir() + "/" + this.configFileNameSample);
            try {
                Files.copy(original, copied, StandardCopyOption.REPLACE_EXISTING);
            } catch (Exception ex) {
                Bukkit.getConsoleSender().sendMessage( EC_API.__("config.error-cannot-save-config") );
                ex.printStackTrace();
                Bukkit.getPluginManager().disablePlugin( plugin );
                return;
            }

            if ( !newConfigFile.exists() ) {
                Bukkit.getConsoleSender().sendMessage( EC_API.__("config.error-cannot-save-config", EC_API.getEcName()) );
                Bukkit.getConsoleSender().sendMessage( "Sample file could not be copied over to the file configuration YAML file." );
                Bukkit.getPluginManager().disablePlugin( plugin );
                return;
            }
        } else {
            // copy current config into a backup file in case it contains errors
            // in which case it would be reset to the default config with no channels
            // and default values (and so we can restore and fix it)
            Path copied       = Paths.get(EC_API.getEcDataDir() + "/" + this.configFileName + ".backup");
            Path original     = Paths.get(EC_API.getEcDataDir() + "/" + this.configFileName);
            try {
                Files.copy(original, copied, StandardCopyOption.REPLACE_EXISTING);
            } catch (Exception ex) {
                EC_API.generateConsoleWarning( EC_API.__("config.error-cannot-save-backup-config", EC_API.getEcName()) );
            }

            // update default values for the main config file
            this.updateDefaults();
        }

        // load config from file
        this.reloadConfig();

        yml = plugin.getDescription();

        // set debug from config
        debug = conf.getBoolean(CONFIG_VALUES.DEBUGENABLED.toString());

        // load the plugin name, as it's being used everywhere for logging purposes
        pluginName = yml.getName();

        // build a list of disabled modules
        if (null != conf.getConfigurationSection("modules")) {
            for (final String feature : conf.getConfigurationSection("modules").getKeys(false)) {
                if (!conf.getBoolean("modules." + feature + ".enabled")) {
                    // found a disabled feature
                    disabledModules.add(feature);
                    disabledModules.addAll(conf.getStringList("modules." + feature + ".linked"));
                }
            }
        }
    } //end method

    /**
     * Updates default keys in a potentially old config file.
     */
    void updateDefaults() {
        // copy any defaults into the renamed config that might be missing due to plugin upgrades
        File newConfigFile                = new File( EC_API.getEcDataDir(), this.configFileName );
        FileConfiguration newConfig       = YamlConfiguration.loadConfiguration( newConfigFile );
        InputStream       defConfigStream = this.plugin.getResource( this.configFileName );
        if (defConfigStream != null) {
            newConfig.setDefaults( YamlConfiguration.loadConfiguration( new InputStreamReader(defConfigStream, Charsets.UTF_8) ) );
        }
        newConfig.options().copyDefaults(true);

        try {
            newConfig.save(newConfigFile);
        } catch (Throwable ex) {
            Bukkit.getLogger().severe('[' + EC_API.getEcName()
                + "] Could not save configuration file. Please report the following to the plugin's author..."); //NON-NLS
            ex.printStackTrace();
        }
    }

    /**
     * Sets this plugin's debug state.
     *
     * @param value New value for plugin's debug state - true to turn ON, false to turn OFF.
     */
    void setDebug(final boolean value) {
        debug = value;
        conf.set(CONFIG_VALUES.DEBUGENABLED.toString(), debug);
    } //end method

    /**
     * Gets user YAML plugin configuration for EasyChat.
     *
     * @return Returns user YAML plugin configuration for EasyChat
     * from the config.yml file located in [pluginsFolder]/EasyChat.
     */
    ConfigSectionAbstractAdapter getConf() {
        return conf;
    } //end method

    /***
     * Closes any DB or file connections that the config class
     * have open upon disabling the plugin.
     */
    void onClose() {
        // nothing needed for file configuration
    } // end mehod

    /**
     * Reloads configuration from the source, overwriting any old values we might have cached in memory.
     * Used when EasyChat is being disabled to prevent storing old values into the DB.
     */
    void reloadConfig() {
        File configFile = new File(EC_API.getEcDataDir(), configFileName);
        conf = new ConfigSectionFileAdapter( YamlConfiguration.loadConfiguration( configFile ) );
        conf.setConfigFile( configFile );
    } // end method

} // end class