    package com.martinambrus.easyChat;

    import org.bukkit.Bukkit;
    import org.bukkit.command.TabCompleter;
    import org.bukkit.event.Listener;
    import org.bukkit.plugin.Plugin;
    import org.bukkit.plugin.java.JavaPlugin;

    /**
     * Creates and registers tab completer for commands
     * on servers where this is supported and for commands
     * that do have this feature enabled.
     *
     * @author Martin Ambrus
     */
    final class TabComplete implements Listener {

        /**
         * Instance of {@link EasyChat}.
         */
        private final Plugin plugin;

        /**
         * Constructor, creates a new instance of the TabComplete class
         * and registers itself as a TabCompleter for all EasyChat commands.
         *
         * <br><br><strong>Example:</strong>
         * <pre>
         * // usage in EasyChat main class
         * this.tabcomplete = new TabComplete(this);
         * this.tabcomplete.registerTabCompleters();
         * </pre>
         *
         * @param ec Instance of {@link EasyChat EasyChat}.
         */
        TabComplete(final Plugin ec) {
            plugin = ec;
        } //end method

        /**
         * Used on EasyChat startup (i.e. in onEnable).
         * Parses all commands that are in plugin.yml file for this plugin
         * and registers their tab completers one by one to the correct classes,
         * should these commands have tab completers present.
         * This is possible, as classes follow the same naming convention
         * as commands, i.e. Ec_version.class = /ec_version.
         *
         * <br><br><strong>Example:</strong>
         * <pre>
         * // usage in EasyChat main class
         * tabcomplete = new TabComplete(this);
         * tabcomplete.registerTabCompleters();
         * </pre>
         */
        void registerTabCompleters() {
            ConfigAbstractAdapter config = ((EasyChat) plugin).getConf();

            if (null != config.getInternalConf().getCommands()) {
                // iterate all EC commands and register their tab completers, one by one
                for (final String cmd : EC_API.getCommandsKeySet()) {
                    // don't register tab completers for disabled commands
                    if (!config.isDisabled(cmd.replaceAll("ec_", ""))) { //NON-NLS
                        try {
                            final Class<?> cl = Class.forName("com.martinambrus.easyChat.tabcomplete." + Utils.capitalize(cmd));

                            TabCompleter tc = (TabCompleter) cl.getConstructor().newInstance();
                            ((JavaPlugin) plugin).getCommand(cmd).setTabCompleter(tc);
                        } catch (final NoClassDefFoundError | ClassNotFoundException e1) {
                            // older versions (1.7-) do not support tab completion
                            // and neither have all commands tab completers
                        } catch (final Throwable e2) {
                            Bukkit.getLogger().severe('[' + config.getPluginName()
                                + "] " + EC_API.__("error.failed-to-register-tab-completer", cmd));
                            e2.printStackTrace();
                        }
                    }
                }
            }
        } // end method

    } // end class