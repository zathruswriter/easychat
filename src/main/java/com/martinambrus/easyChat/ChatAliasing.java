package com.martinambrus.easyChat;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * ChatAliasing-related utilities and methods.
 * Used to work with chatAliasing aliasing and custom commands executions.
 *
 * @author Martin Ambrus
 */
@SuppressWarnings({"OverlyComplexClass", "VariableNotUsedInsideIf"})
final class ChatAliasing {

    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * A fast-lookup map for patterns within worlds where they are enabled.
     * The key is world name for which the patterns in the List value are enabled.
     */
    private Map<String, List<ChatPattern>> regexes = new HashMap<>();

    /**
     * Constructor.
     * @param ec Instance of {@link EasyChat EasyChat}.
     */
    ChatAliasing(final Plugin ec) {
        plugin = ec;

        if ( EC_API.isModuleEnabled( "chat-aliasing" ) ) {
            // load all patterns, compile them and cache them
            for ( String pattern_name : EC_API.getConfigSectionKeys("modules.chat-aliasing.patterns") ) {
                try {
                    String pattern = EC_API.getConfigString( "modules.chat-aliasing.patterns." + pattern_name + ".match" );

                    // make sure we have a valid pattern
                    if ( null == pattern ) {
                        EC_API.generateConsoleWarning( EC_API.__( "error.pattern-invalid", pattern_name ) );
                        continue;
                    }

                    // create the ChatPattern object
                    String replacement_type;
                    String replacement_value;
                    boolean cancels_chat_message = true;

                    if ( null != EC_API.getConfigString( "modules.chat-aliasing.patterns." + pattern_name + ".command" ) ) {
                        replacement_type = "command";
                        replacement_value = EC_API.getConfigString( "modules.chat-aliasing.patterns." + pattern_name + ".command" );
                    } else if ( null != EC_API.getConfigString( "modules.chat-aliasing.patterns." + pattern_name + ".replace" ) ) {
                        replacement_type = "text";
                        replacement_value = EC_API.getConfigString( "modules.chat-aliasing.patterns." + pattern_name + ".replace" );
                    } else {
                        EC_API.generateConsoleWarning( EC_API.__( "error.pattern-invalid-type", pattern_name ) );
                        continue;
                    }

                    if ( null != EC_API.getConfigString( "modules.chat-aliasing.patterns." + pattern_name + ".cancel-original-message" ) ) {
                        cancels_chat_message = EC_API.getConfigBoolean( "modules.chat-aliasing.patterns." + pattern_name + ".cancel-original-message" );
                    }

                    ChatPattern patt = new ChatPattern( Pattern.compile( pattern ), replacement_type, replacement_value, cancels_chat_message );

                    // if we have a response to be sent back to player, set it here
                    if ( null != EC_API.getConfigString( "modules.chat-aliasing.patterns." + pattern_name + ".respond" ) ) {
                        patt.setResponse( EC_API.getConfigString( "modules.chat-aliasing.patterns." + pattern_name + ".respond" ) );
                    }

                    // add global pattern
                    List<String> excluded_worlds = EC_API.getConfigStringList("modules.chat-aliasing.patterns." + pattern_name + ".excluded-worlds");
                    if ( excluded_worlds.isEmpty() ) {
                        if ( null == this.regexes.get("*") ) {
                            this.regexes.put( "*", new ArrayList<>() );
                        }

                        this.regexes.get("*").add( patt );
                    } else {
                        // add pattern to each of the worlds where it's enabled
                        boolean invert_excluded_worlds = EC_API.getConfigBoolean("modules.chat-aliasing.patterns." + pattern_name + ".excluded-worlds-works-as-whitelist");
                        for ( World world : Bukkit.getWorlds() ) {
                            if (
                                // if we're not using the excluded worlds as an include list,
                                // check that we don't have this world in it
                                ( !invert_excluded_worlds && !excluded_worlds.contains( world.getName()) )
                                ||
                                // if we're using the excluded worlds as an include list,
                                // add this world into the worlds enabled for this pattern
                                ( invert_excluded_worlds && excluded_worlds.contains( world.getName()) )
                            ) {
                                if ( null == this.regexes.get( world.getName() ) ) {
                                    this.regexes.put( world.getName(), new ArrayList<>() );
                                }

                                this.regexes.get( world.getName() ).add( patt );
                            }
                        }
                    }
                } catch (PatternSyntaxException ex) {
                    EC_API.generateConsoleWarning( EC_API.__( "error.pattern-invalid", pattern_name ) );
                }
            }
        }
    } //end method

    /**
     * Checks whether the chatAliasing message matches any patterns
     * in the given world and returns it, or null if no matches were found.
     *
     * @param world_name Name of the world to check message for the patterns in.
     * @param message The actual message to check for patterns.
     *
     * @return Returns the pattern found or null if no patterns match the given message.
     */
    public ChatPattern message_matches_pattern(String world_name, String message) {
        // if we don't have anything to check, it's easy
        if ( this.regexes.isEmpty() ) {
            return null;
        }

        // load patterns in the world we're requesting
        List<ChatPattern> chat_patterns = this.regexes.get( world_name );
        if ( null == chat_patterns ) {
            chat_patterns = new ArrayList<>();
        }

        // load global patterns
        if ( null != this.regexes.get("*") ) {
            chat_patterns.addAll(this.regexes.get("*"));
        }

        // check for matches and return it when found
        for ( ChatPattern pattern : chat_patterns ) {
            final Matcher m = pattern.getPattern().matcher( message );
            if (m.find()) {
                // found a match, return the whole pattern and bail out
                return pattern;
            }
        }

        return null;
    }

} // end class