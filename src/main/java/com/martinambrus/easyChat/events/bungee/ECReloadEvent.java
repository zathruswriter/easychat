package com.martinambrus.easyChat.events.bungee;

import net.md_5.bungee.api.plugin.Event;

/**
 * Event which is fired up when a reload of data
 * is required, for example when we want to check
 * for a new version of EC manually via /ec_version
 *
 * @author Martin Ambrus
 */
public class ECReloadEvent extends Event {

    /**
     * The actual identifier which will be used
     * to determine who should react to this event.
     *
     * In EasyChat, this is usually name of the feature
     * that should react and reload its configuration.
     */
    private final String message;

    /**
     * Constructor, stores internal variables.
     *
     * @param triggerTarget The actual identifier which will be used
     *                      to determine who should react to this event.
     */
    public ECReloadEvent( final String triggerTarget) {
        this.message = triggerTarget;
    } // end method

    /**
     * Getter for message.
     *
     * @return Returns the actual identifier which will be used
     *         to determine who should react to this event.
     */
    public String getMessage() {
        return this.message;
    } // end method

} // end method