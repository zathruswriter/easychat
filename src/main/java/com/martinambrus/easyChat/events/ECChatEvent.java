package com.martinambrus.easyChat.events;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.Utils;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Event which is fired up when an adjusted chat line
 * is about to be sent out to players' chat.
 *
 * @author Martin Ambrus
 */
public class ECChatEvent extends Event {

    /**
     * Message that is being sent out to players' chat.
     */
    private final String message;

    /**
     * Clear final chat message, used for logging purposes.
     * Contains no JSON parts. Contains colors (which are removed
     * if so configured).
     */
    private String log_message;

    /**
     * Stores original message from the player without any formatting
     * from EasyChat.
     */
    private String original_message;

    /**
     * Player (or console) who is sending this message.
     */
    private final CommandSender player;

    /**
     * List of players who will receive this message.
     */
    Set<Player> recipients;

    /**
     * If we need to prefix the original message only for certain players,
     * we'll store them here.
     */
    private final Map<Player, List<String>> per_player_message_prefixes = new HashMap<>();

    /**
     * Name of the chat channel to which this event connects.
     */
    private final String channel_name;

    /**
     * List of all event handlers activated for this event.
     */
    private static final HandlerList handlers = new HandlerList();

    /**
     * Whether or not this event was cancelled by any plugin.
     */
    private boolean is_cancelled = false;

    /**
     * Constructor, stores internal variables.
     *
     * @param message The message that is being sent out to players' chat.
     * @param sender Player (or console) who is sending this message.
     * @param log_message Clear message for console logging purposes.
     * @param original_message The original message that player actually typed into chat
     *                         before all our formatting occured.
     * @param channel_name Name of the channel to which this chat message is connected.
     * @param recipients Players who will receive this message.
     */
    public ECChatEvent(final String message, final String log_message, final String original_message, final CommandSender sender, final Set<Player> recipients, final String channel_name) {
        this.message = message;
        this.log_message = log_message;
        this.original_message = original_message;
        this.channel_name = channel_name;

        if ( EC_API.getConfigBoolean( "modules.console-strip-chat-colors" ) ) {
            this.log_message = Utils.remove_chat_color_values( this.log_message );
        }

        this.player = sender;
        this.recipients = recipients;
    } // end method

    /**
     * Adds simple text to the beginning of the formatted message.
     *
     * @param text Simple text to add to the beginning of the message.
     */
    public void prefix_message_with_text( String text ) {
        this.message.replace( "[\"\",", "[\"\",{\"text\":\"" + Utils.translate_chat_colors( text ) + "\"}," );

        if ( EC_API.getConfigBoolean( "modules.console-strip-chat-colors" ) ) {
            this.log_message = Utils.remove_chat_color_values( text ) + this.log_message;
        } else {
            this.log_message = Utils.translate_chat_colors( text ) + this.log_message;
        }
    } // end method

    /**
     * Adds JSON-formatted text component to the beginning of the formatted message.
     *
     * @param json JSON-formatted text component to add to the beginning of the message.
     */
    public void prefix_message_with_json( String json ) {
        this.message.replace( "[\"\",", "[\"\"," + json + "," );

        // extract text information from the JSON text and put it into the clear log message
        String clear_text = json
            .replace( "\": ", "\":" )
            .replace( " :\"", ":\"" )
            .replace( "\" : \"", "\":\"" )
            .replace( "{ \"", "{\"" )
            .replace( "\" }", "\"}" );

        if ( clear_text.indexOf( "\"text\":\"" ) > -1 ) {
            // first, start with the text portion of the JSON text
            clear_text = clear_text.substring( clear_text.indexOf( "\"text\":\"" ) );

            // then remove the "text" pattern from the beginning
            clear_text = clear_text.replace("\"text\":\"", "");

            // replace escaped quotes in the text that's left by a temporary placeholder
            clear_text = clear_text.replace("\\\"", "$[TMP_QUOTE]$");

            // copy all that's left from position 0 until the first unescaped quote
            if ( clear_text.indexOf( "\"" ) > -1 ) {
                clear_text = clear_text.substring( 0, clear_text.indexOf( "\"" ) );

                // return escaped quotes
                clear_text = clear_text.replace("$[TMP_QUOTE]$", "\\\"");
            } else {
                // malformed JSON with no ending quotes
                clear_text = "";
            }
        }

        if ( EC_API.getConfigBoolean( "modules.console-strip-chat-colors" ) ) {
            this.log_message = Utils.remove_chat_color_values( clear_text ) + this.log_message;
        } else {
            this.log_message = Utils.translate_chat_colors( clear_text ) + this.log_message;
        }
    } // end method

    /**
     * Adds a new prefix to be used in the given player's message
     *
     * @param player Player to add the given simple text prefix to.
     */
    public void prefix_player_message_with_text( Player player, String text ) {
        if ( !this.per_player_message_prefixes.containsKey( player ) ) {
            this.per_player_message_prefixes.put( player, new ArrayList<>());
        }

        this.per_player_message_prefixes.get( player ).add("{\"text\":\"" + Utils.translate_chat_colors( text) + "\"}");
    } // end method

    /**
     * Adds a new JSON-formatted prefix to be used in the given player's message
     *
     * @param player Player to add the given json-formatted prefix to.
     */
    public void prefix_player_message_with_json( Player player, String json ) {
        if ( !this.per_player_message_prefixes.containsKey( player ) ) {
            this.per_player_message_prefixes.put( player, new ArrayList<>());
        }

        this.per_player_message_prefixes.get( player ).add( json );
    } // end method

    /**
     * Getter for message.
     *
     * @return Returns the message that is being sent out to players' chat.
     */
    public String getMessage() {
        return this.message;
    } // end method

    /**
     * Getter for log_message.
     *
     * @return Returns the log message that is being sent to console output
     *         for logging purposes.
     */
    public String getLogMessage() {
        return this.log_message;
    } // end method

    /**
     * Getter for original_message.
     *
     * @return Returns the original message from the player without any formatting.
     */
    public String getOriginalMessage() {
        return this.original_message;
    } // end method

    /**
     * Getter for channel_name.
     *
     * @return Returns the channel name to which this event is connected.
     */
    public String getChannelName() {
        return this.channel_name;
    } // end method

    /**
     * Returns JSON message part to prefix to the given player's message.
     *
     * @param player Player to get a JSON prefix value for.
     * @return
     */
    public String getPlayerMessagePrefix( Player player ) {
        if ( this.per_player_message_prefixes.containsKey( player ) ) {
            return Utils.implode( this.per_player_message_prefixes.get( player ), "," );
        } else {
            return null;
        }
    } // end method

    /**
     * Getter for player.
     *
     * @return Returns the player who is sending this message.
     */
    public CommandSender getPlayer() {
        return this.player;
    } // end method

    /**
     * Getter for recipients.
     *
     * @return Returns a list of players who will receive this message.
     */
    public Set<Player> getRecipients() {
        return this.recipients;
    } // end method

    /**
     * Getter for is_cancelled.
     *
     * @return Returns true if this event was cancelled by any plugin, false otherwise.
     */
    public boolean isCancelled() { return this.is_cancelled; } // end method

    /**
     * Setter for is_cancelled.
     */
    public void setCancelled( boolean cancelled ) { this.is_cancelled = cancelled; } // end method

    /**
     * Getter for list of all handlers for this event.
     */
    @Override
    public HandlerList getHandlers() {
        return handlers;
    } // end method

    /**
     * Getter for list of all handlers for this event.
     *
     * @return Returns list of handlers for this event.
     */
    public static HandlerList getHandlerList() {
        return handlers;
    } // end method

} // end method