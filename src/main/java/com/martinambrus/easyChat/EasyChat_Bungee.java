package com.martinambrus.easyChat;

import com.martinambrus.easyChat.commands.bungee.Ec_version;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;

/**
 * EasyChat for BungeeCord
 *
 * @author Martin Ambrus
 */
@SuppressWarnings("ClassWithTooManyDependencies")
public final class EasyChat_Bungee extends Plugin {

    /**
     * Instance of {@link Updater}
     */
    private UpdaterBungee updater = null;

    /***
     * Called by the server when a plugin is loaded
     * and ready for some action.
     */
    @Override
    public void onEnable() {
        // enable update checker
        updater = new UpdaterBungee(this);

        // register commands
        ProxyServer.getInstance().getPluginManager().registerCommand(this, new Ec_version( this ) );

        // we're enabled!
        //noinspection HardCodedStringLiteral
        getLogger().info(this.getDescription().getName() + " v" + this.getDescription().getVersion() + " enabled!");
    } // end method

    /***
     * Called by the server when a plugin needs to be unloaded.
     * This is usually called on a server stop or reload, the latter
     * of which is EVIL and should never be used! :-P
     */
    @Override
    public void onDisable() {
        // stop update checking
        if (null != updater) {
            updater.unregister();
        }

        // inform (via console) that we're disabled now
        getLogger().info(this.getDescription().getName() + " v" + this.getDescription().getVersion() + " disabled.");
    } // end method

} // end class