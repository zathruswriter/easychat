package com.martinambrus.easyChat;

import java.util.*;

import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * Buffers chat in order to enabled chat bubbles above player
 * to display 1 by 1 in timed intervals in parts.
 *
 * @author atesin
 */
public class ChatBuffer {
    /**
     * Instance of {@link EasyChat}.
     */
    private final Plugin plugin;

    /**
     * Chat bubble height (rows).
     */
    private int maxBubbleHeight;

    /**
     * Chat bubble width (characters).
     */
    private int maxBubbleWidth;

    /**
     * Time in which to change chat bubbles.
     */
    private int bubblesInterval;

    /**
     * Map of queued chat messages for a player.
     */
    private Map<UUID, Queue<String>> chatQueue = new HashMap<>();

    /**
     * Constructor.
     * @param ec Instance of {@link EasyChat EasyChat}.
     */
    ChatBuffer(Plugin ec)
    {
        this.plugin = ec;

        String max_height_config_route = "modules.chat-bubbles.max-bubble-height";
        try {
            this.maxBubbleHeight = Integer.parseInt( EC_API.getConfigString( max_height_config_route ) );
        } catch (Exception ex) {
            this.maxBubbleHeight = 3;
            EC_API.generateConsoleWarning( EC_API.__("chatbubbles.invalid-int-value", max_height_config_route, 3 ) );
        }

        String max_width_config_route = "modules.chat-bubbles.max-bubble-characters";
        try {
            this.maxBubbleWidth = Integer.parseInt( EC_API.getConfigString( max_width_config_route ) );
        } catch (Exception ex) {
            this.maxBubbleWidth = 15;
            EC_API.generateConsoleWarning( EC_API.__("chatbubbles.invalid-int-value", max_width_config_route, 15 ) );
        }

        String bubbles_interval_config_route = "modules.chat-bubbles.pause-between-bubbles";
        try {
            this.bubblesInterval = Integer.parseInt( EC_API.getConfigString( bubbles_interval_config_route ) );
        } catch (Exception ex) {
            this.bubblesInterval = 5;
            EC_API.generateConsoleWarning( EC_API.__("chatbubbles.invalid-int-value", bubbles_interval_config_route, 5 ) );
        }
    }

    /**
     * Wraps pre-trimmed chat and puts it in a player buffer.
     *
     * @param player The player to queue the chat line for.
     * @param msg The actual message to buffer.
     */
    void receiveChat(Player player, String msg) {
        // most probable case, 1 line chat
        if ( msg.length() <= this.maxBubbleWidth ) {
            this.queueChat( player, msg + "\n" );
            return;
        }

        // longer chat, prepare word wrap
        msg = msg + " ";
        String chat = "";
        int delimPos;
        int lineCount = 0;

        // word wrap chat
        while (msg.length() > 0) {
            // search delimiter (space) before or after bubble width, or finishing
            delimPos = msg.lastIndexOf(' ', this.maxBubbleWidth);

            if (delimPos < 0) {
                delimPos = msg.indexOf(' ', this.maxBubbleWidth);
            }

            if (delimPos < 0) {
                delimPos = msg.length();
            }

            // pull sized text from chat message
            chat += msg.substring( 0, delimPos );
            msg = msg.substring( delimPos + 1 );

            // line wrap chat in multiple messages if exceeds max lines
            ++lineCount;
            if ( lineCount % this.maxBubbleHeight == 0 || msg.length() == 0 ) {
                queueChat( player, chat + (msg.length() == 0 ? "\n" : "...\n") );
                chat = "";
            } else {
                chat += "\n";
            }
        }
    } // end method

    /**
     * Gets word-wrapped chat and queues in a player buffer,
     * creates it if it doesn't exist.
     *
     * @param player Player to wrap chat for.
     * @param chat The word-wrapped chat of the player.
     */
    private void queueChat( Player player, String chat ) {
        // if we have no player buffer yet, create it and schedule this message
        UUID playerId = player.getUniqueId();
        if ( !this.chatQueue.containsKey( playerId ) ) {
            this.chatQueue.put( playerId, new LinkedList<String>() );
            scheduleMessageUpdate( player, playerId, 0 );
        }

        // queue this message
        this.chatQueue.get( playerId ).add( chat );
    } // end method

    /**
     * Takes previously queued chat messages and schedules them
     * one by one for displaying.
     *
     * @param player Player to schedule chat messages for.
     * @param playerId ID of the player, used for lookup purposes.
     * @param timer The actual timer time to schedule next bubble after.
     */
    private void scheduleMessageUpdate( Player player, UUID playerId, int timer ) {
        new BukkitRunnable() {
            @Override
            public void run() {
                // player could be not chatting or can be offline,
                // check this and remove their data if that's so
                if ( ChatBuffer.this.chatQueue.get( playerId ).size() < 1 || !player.isOnline() ) {
                    ChatBuffer.this.chatQueue.remove( playerId );
                    return;
                }

                // pull queued message, send it to be displayed and get the duration, schedule the next message
                String chat           = ChatBuffer.this.chatQueue.get( playerId ).poll();
                int    bubbleDuration = EC_API.chatBubbleReceiveChatMessage( player, chat );
                scheduleMessageUpdate( player, playerId, bubbleDuration + ChatBuffer.this.bubblesInterval );
            }
        }.runTaskLater( plugin, timer );
    } // end method

} // end class