package com.martinambrus.easyChat;

import com.martinambrus.easyChat.events.ECToggleDebugEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredListener;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

/**
 * Everything listeners-related that's not tied to commands
 * and concrete functionality is in this (helper) class.
 *
 * @author Martin Ambrus
 */
final class Listeners implements Listener {

    /**
     * The EasyChat plugin instance pointer.
     */
    private final Plugin plugin;

    /**
     * Multiple commands can use the same listener, so this variable will record which
     * listeners are already used and prevent double or triple registering of the same
     * listener functionality.
     */
    private final Map<String, Listener> registeredListeners = new HashMap<String, Listener>();

    /**
     * Constructor, stores an instance of {@link EasyChat}
     * and enables all relevant listeners.
     *
     * @param ec The singleton instance of {@link EasyChat}.
     */
    Listeners(final Plugin ec) {
        plugin = ec;
    } //end method

    /**
     * Initializes and activates all EasyChat listeners.
     */
    void init() {
        // activate core listening functionality
        startRequiredListener("coreListeners", this); //NON-NLS
        startRequiredListener("EasyChat", (Listener) this.plugin); //NON-NLS
        // this listener is used for DB-based configs which store changed configs into a DB directly after
        // changing them locally in a file
        startRequiredListener("EasyChatConfig", ((EasyChat) this.plugin).getConf()); //NON-NLS
    } // end method

    /***
     * Registers a listener if that listener was not registered already.
     * Used to lazy-register listeners from commands and methods that expect them to be registered.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * // register an event listener to display join and leave click links next to nicknames in chatAliasing
     * this.startRequiredListener("chatJoinLeaveClicks");
     * </pre>
     *
     * @param listenerName ClassName of the listener to register. This class will then be loaded
     *                     from com.martinambrus.easyChat.listeners.[[ listenerName ]]
     */
    void startRequiredListener(final String listenerName) {
        if (!isListenerRegistered(listenerName)) {
            try {
                final Class<?>       cl               = Class.forName("com.martinambrus.easyChat.listeners." + listenerName);
                final Constructor<?> cons             = cl.getConstructor(Plugin.class);
                final Listener       listenerInstance = (Listener) cons.newInstance(plugin);

                // only register this listener if it did not provide call to startRequiredListener() in its own constructor,
                // in which case it'd already be registered
                if (!isListenerRegistered(listenerName)) {
                    registeredListeners.put(listenerName, listenerInstance);
                    plugin.getServer().getPluginManager().registerEvents(listenerInstance, plugin);
                }
            } catch (final Throwable e) {
                Bukkit.getLogger()
                      .severe('[' + ((EasyChat) plugin).getConf().getPluginName()
                          + "] " + EC_API.__("error.listener-not-found", listenerName));
                e.printStackTrace();
                Bukkit.getServer().getPluginManager().disablePlugin(plugin);
            }
        }
    } //end method

    /***
     * Registers a listener if that listener was not registered already.
     * Used to lazy-register listeners from commands and methods that expect them to be registered.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * this.startRequiredListener("yourListenerName", yourListenerClassInstance);
     * </pre>
     *
     * @param listenerName          ClassName of the listener to register. This class will then be loaded
     *                              from com.martinambrus.easyChat.listeners.[[ listenerName ]]
     * @param listenerClassInstance The actual instance of the listener class we'd like to register.
     *                              This method signature is used by event listener classes themselves
     *                              to self-register as needed (e.g. lazy-self-registering).
     */
    void startRequiredListener(final String listenerName, final Listener listenerClassInstance) {
        if (!isListenerRegistered(listenerName)) {
            registeredListeners.put(listenerName, listenerClassInstance);
            plugin.getServer().getPluginManager().registerEvents(listenerClassInstance, plugin);
        }
    } //end method

    /***
     * Unregisters all event listeners registered by this plugin.
     * Used when disabling EC.
     */
    void unregisterListeners() {
        if (!registeredListeners.isEmpty()) {
            final Iterable<HandlerList> hl = HandlerList.getHandlerLists();
            for (final HandlerList l : hl) {
                for (final RegisteredListener r : l.getRegisteredListeners()) {
                    for (final Map.Entry<String, Listener> pair : registeredListeners.entrySet()) {
                        if (r.getListener().equals(pair.getValue())) {
                            l.unregister(r);

                            if (((EasyChat) plugin).getDebug()) {
                                //noinspection UseOfSystemOutOrSystemErr,ObjectToString
                                Utils.logDebug("EC listener unregistered: " + r, (EasyChat) plugin); //NON-NLS
                            }
                        }
                    }
                }
            }

            registeredListeners.clear();
        }
    } //end method

    /**
     * Checks whether a listener is already registered.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * if (!this.isListenerRegistered("chatJoinLeaveClicks")) {
     *   // register an event listener to display join and leave click links next to nicknames
     *   // in chatAliasing
     * }
     * </pre>
     *
     * @param listenerName Name of the listener to check for.
     *
     * @return Returns true if the given listener name is already registered, false otherwise.
     */
    boolean isListenerRegistered(final String listenerName) {
        return registeredListeners.containsKey(listenerName);
    } // end method

    /***
     * Reacts to the custom ECToggleDebugEvent which is fired whenever we need
     * to toggle EasyChat's debugging facilities on or off.
     *
     * @param e The actual debug event.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void toggleDebug(final ECToggleDebugEvent e) {
        ((EasyChat) plugin).getConf().setDebug(!((EasyChat) plugin).getConf().getDebug());
        ((EasyChat) plugin).getConf().getConf().saveConfig();
    } // end method

} // end class