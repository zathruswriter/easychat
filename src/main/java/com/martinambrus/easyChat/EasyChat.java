package com.martinambrus.easyChat;

import com.martinambrus.easyChat.events.ECReloadEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.AreaEffectCloud;
import org.bukkit.entity.Entity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * EasyChat - a working chatAliasing solution for servers small and large
 *
 * @author Martin Ambrus
 */
@SuppressWarnings("ClassWithTooManyDependencies")
public final class EasyChat extends JavaPlugin implements Listener {

    /**
     * Instance of {@link com.martinambrus.easyChat.ConfigAbstractAdapter}.
     */
    private ConfigAbstractAdapter config = null;

    /**
     * Instance of {@link com.martinambrus.easyChat.Language}.
     */
    private Language lang = null;

    /**
     * Instance of {@link com.martinambrus.easyChat.Commands}.
     */
    Commands commands = null;

    /**
     * Instance of {@link com.martinambrus.easyChat.Channels}.
     */
    Channels channels = null;

    /**
     * Instance of {@link ChatAliasing}.
     */
    ChatAliasing chatAliasing = null;

    /**
     * Instance of {@link Placeholders}.
     */
    Placeholders placeholders = null;

    /**
     * Instance of {@link LocalStorage}.
     */
    LocalStorage localStorage = null;

    /**
     * Instance of {@link ChatBubbles}.
     */
    ChatBubbles chatBubbles = null;

    /**
     * Instance of {@link ChatBuffer}.
     */
    ChatBuffer chatBuffer = null;

    /**
     * Instance of {@link com.martinambrus.easyChat.Permissions}
     */
    private Permissions perms = null;

    /**
     * Instance of {@link com.martinambrus.easyChat.Listeners}
     */
    private Listeners listeners = null;

    /**
     * Instance of {@link com.martinambrus.easyChat.DiscordSrv}
     */
    private DiscordSrv discordSrv = null;

    /**
     * Instance of {@link com.martinambrus.easyChat.TabComplete}
     */
    private TabComplete tabComplete = null;

    /**
     * Instance of {@link com.martinambrus.easyChat.Updater}
     */
    private Updater updater = null;

    /***
     * Determines whether custom metrics have already been started.
     * This is present, so even if EC is reloaded (disabled and re-enabled
     * on the server itself), we won't be duplicating metrics.
     */
    private boolean metricsStarted = false;

    /***
     * Called by the server when a plugin is loaded
     * and ready for some action.
     */
    @Override
    public void onEnable() {
        // initialize the API
        new EC_API(this);

        // check, update and set current valid plugin configuration
        Config configBootstrap = new Config(this);
        this.config = configBootstrap.getConfigAdapter();

        // initialize local storage utilities
        localStorage = new LocalStorage(this);

        // load translations
        lang = new Language(this);
        if (!lang.init()) {
            // bail out if we couldn't load the language file (EC will be disabled automatically)
            return;
        }

        // initialize the listeners registrator and utils class
        listeners = new Listeners(this);
        listeners.init();

        // initialize channels handler
        channels = new Channels(this);

        // initialize chat aliasing related utilities
        chatAliasing = new ChatAliasing(this);

        // initialize chat placeholders related utilities
        placeholders = new Placeholders();

        // initialize chat bubbles related utilities
        if ( EC_API.isModuleEnabled("chat-bubbles") ) {
            try {
                Class.forName( "org.bukkit.entity.AreaEffectCloud" );
                AreaEffectCloud.class.getMethod("addPassenger", new Class[] { Entity.class });
                chatBubbles = new ChatBubbles();
                chatBuffer = new ChatBuffer( this );
            } catch (ClassNotFoundException | NoSuchMethodException | SecurityException ex) {
                EC_API.generateConsoleWarning(
                    "\n\n"
                        + "**************************************************\n"
                        + "** " + EC_API.__("error.chat-bubbles-incorrect-mc-version.1") + "\n"
                        + "** " + EC_API.__("error.chat-bubbles-incorrect-mc-version.2") + '\n'
                        + "**************************************************\n");
            }
        }

        // initialize permissions handler (Vault or native)
        perms = new Permissions(this);

        // register executors for commands
        commands = new Commands(this);
        if (commands.registerCommandExecutors(config)) {
            // bail out if we're disabling EC due to a missing command (EC will be disabled automatically)
            return;
        }

        // enable command spy, if enabled in config
        if ( EC_API.isModuleEnabled("command-spy") ) {
            EC_API.startRequiredListener("commandSpy");
        }

        // enable custom join/leave messages, if enabled in config
        if ( EC_API.isModuleEnabled( "custom-join-message" ) || EC_API.isModuleEnabled( "custom-leave-message" ) ) {
            EC_API.startRequiredListener( "customJoinLeaveMessage" );
        }

        // register tab completer
        tabComplete = new TabComplete(this);
        tabComplete.registerTabCompleters();

        // enable DiscordSrv support, if allowed in config
        if ( EC_API.isModuleEnabled("discordsrv-plugin-compatibility") && Bukkit.getPluginManager().getPlugin( "DiscordSrv" ) != null ) {
            discordSrv = new DiscordSrv( this );
        }

        // enable move to chat module, if allowed in config
        if ( EC_API.isModuleEnabled("move-to-chat") ) {
            EC_API.startRequiredListener( "moveToChatListener" );
        }

        // enable chat notifications module, if allowed in config
        if ( EC_API.isModuleEnabled("mention-sound-notification") ) {
            EC_API.startRequiredListener( "mentionsListener" );
        }

        // enable update checker
        updater = new Updater(this);

        // start collecting some metrics
        if (!metricsStarted) {
            new CustomMetrics(this).initMetrics();
            metricsStarted = true;
        }

        // we're enabled!
        //noinspection HardCodedStringLiteral
        Bukkit.getLogger()
              .info(config.getPluginName() + " v" + config.getInternalConf().getVersion() + ' ' + EC_API
                  .__("general.enabled"));
    } // end method

    /***
     * Called by the server when a plugin needs to be unloaded.
     * This is usually called on a server stop or reload, the latter
     * of which is EVIL and should never be used! :-P
     */
    @Override
    public void onDisable() {
        // unregister Bungee channels messaging
        this.getServer().getMessenger().unregisterOutgoingPluginChannel( this, "BungeeCord" );
        this.getServer().getMessenger().unregisterIncomingPluginChannel( this, "BungeeCord" );

        // load config from the config file, since we don't want to save old values
        config.reloadConfig();
        reloadConfig();

        // unregister all registered EC commands and clear command caches
        if (null != commands && null != config) {
            commands.unregisterCommandExecutors(config);
        }

        // unregister the Vault connection
        if (null != perms) {
            perms.unregisterVaultServiceProvider();
        }

        // unregister event listeners for EC
        if (null != listeners) {
            listeners.unregisterListeners();
        }

        // stop update checking
        if (null != updater) {
            updater.unregister();
        }

        // terminate configuration DB connection, if any
        config.onClose();

        // reset internal variables, should we re-enable this plugin again,
        // so they get re-loaded correctly
        config = null;
        commands = null;
        channels = null;
        chatAliasing = null;
        placeholders = null;
        perms = null;
        listeners = null;
        updater = null;
        tabComplete = null;
        localStorage.onDisable();
        localStorage = null;
        chatBubbles = null;
        chatBuffer = null;
        if ( null != discordSrv ) {
            discordSrv.onDisable();
        }
        discordSrv = null;

        // inform (via console) that we're disabled now
        Bukkit.getLogger()
              .info((null != config ? config.getPluginName() + " v" + config.getInternalConf().getVersion() : //NON-NLS
                     "EasyChat") + ' ' + EC_API.__("general.disabled")); //NON-NLS

        lang = null;
    } // end method

    /***
     * Wrapper method to {@link com.martinambrus.easyChat.Config#getConfigAdapter()}.
     * Gets user YAML plugin configuration (config.yml) for EasyChat.
     *
     * @return Returns plugin configuration for EasyChat, loaded either from
     *         the config.yml file located in [pluginsFolder]/EasyChat
     *         or from a DB backend.
     */
    ConfigSectionAbstractAdapter getExternalConf() {
        return config.getConf();
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.ConfigAbstractAdapter}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.ConfigAbstractAdapter}.
     */
    ConfigAbstractAdapter getConf() {
        return config;
    } // end method

    /**
     * Gets the actual instance of {@link com.martinambrus.easyChat.Language}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.Language}.
     */
    Language getLang() {
        return lang;
    } // end method

    /***
     * Gets the state of current debug mode state.
     *
     * @return True if debug is enabled, false otherwise.
     */
    boolean getDebug() {
        return config.getDebug();
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.Listeners}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.Listeners}.
     */
    Listeners getListenerUtils() {
        return listeners;
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.Permissions}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.Permissions}.
     */
    Permissions getPermissionUtils() {
        return perms;
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.ChatBubbles}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.ChatBubbles}.
     */
    ChatBubbles getChatBubblesUtils() {
        return chatBubbles;
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.ChatBuffer}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.ChatBuffer}.
     */
    ChatBuffer getChatBufferUtils() {
        return chatBuffer;
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.LocalStorage}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.LocalStorage}.
     */
    LocalStorage getLocalStorageUtils() {
        return localStorage;
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.Placeholders}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.Placeholders}.
     */
    Placeholders getPlaceholderUtils() {
        return placeholders;
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.Channels}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.Channels}.
     */
    Channels getChannnelUtils() {
        return channels;
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.ChatAliasing}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.ChatAliasing}.
     */
    ChatAliasing getChatAliasingUtils() {
        return chatAliasing;
    } // end method

    /***
     * Gets the actual instance of {@link com.martinambrus.easyChat.Updater}.
     *
     * @return Instance of {@link com.martinambrus.easyChat.Updater}.
     */
    Updater getUpdater() {
        return updater;
    } // end method

    /**
     * Reacts to the reload event and clears all available caches.
     */
    void onReload() {
        // simply call disable and enable procedures
        Bukkit.getPluginManager().disablePlugin(this);
        Bukkit.getPluginManager().enablePlugin(this);
    } // end method

    /***
     * React to the custom ReloadEvent which is fired when <b><i>/ec_reload</i></b> gets executed
     * or when we enable EC for the first time.
     *
     * @param e The actual reload event with message that says who is this reload for.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void reload(final ECReloadEvent e) {
        final String msg = e.getMessage();
        if (null != msg && msg.isEmpty()) {
            onReload();
        }
    } // end method

} // end class