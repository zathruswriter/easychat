package com.martinambrus.easyChat.tabcomplete;

import com.martinambrus.easyChat.Channel;
import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.Utils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import java.util.ArrayList;
import java.util.List;

/**
 * Tab completion for the /ec_subscribe command.
 *
 * @author Martin Ambrus
 */
public class Ec_subscribe implements TabCompleter {

    @Override
    public List<String> onTabComplete(CommandSender commandSender, Command command, String alias, String[] args) {
        if (1 < args.length) {
            //noinspection ReturnOfNull
            return null;
        }

        if ( 1 == args.length ) {
            List<String> channel_names = new ArrayList<>();
            for ( Channel channel : EC_API.get_all_channels() ) {
                channel_names.add(channel.getName());
            }

            Channel spy_channel = EC_API.get_spy_channel();
            if ( null != spy_channel && EC_API.checkPerms( commandSender, Utils.implode( spy_channel.getSubscribe_permissions(), " OR " ), false ) ) {
                channel_names.add( spy_channel.getName() );
            }

            return Utils.get_filtered_command_completions( args[0], channel_names );
        }

        return null;
    } // end method

} // end class