package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.events.ECToggleDebugEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Toggles the debug mode of EasyChat.
 *
 * @author Martin Ambrus
 */
public class Ec_debug implements CommandExecutor {

    /***
     * /ec_debug - enables or disables the debug mode of EasyChat
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if (!EC_API.isModuleEnabled("debug-mode-command")) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        //noinspection HardCodedStringLiteral
        Bukkit.getPluginManager().callEvent(new ECToggleDebugEvent());
        sender.sendMessage(
            ChatColor.GREEN +
                EC_API.__(
                    "commands.debug-on-off-message",
                    (EC_API.getDebug() ? EC_API.__("general.enabled") : EC_API.__("general.disabled")) +
                        '.'
                )
        );

        return true;
    } // end method

} // end class