package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.Channel;
import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.Utils;
import com.martinambrus.easyChat.events.ECChatEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashSet;

/**
 * Replacement for the minecraft:say command to use our channels
 * and formatting, if enabled. Calls the original chat command
 * in case we've disabled this functionality.
 *
 * @author Martin Ambrus
 */
public class Ec_say implements CommandExecutor {

    /**
     * Instance of EasyChat.
     */
    private Plugin plugin;

    /**
     * Constructor, stores the plugin singleton instance.
     * @param ec EasyChat plugin instance.
     */
    public Ec_say(final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * /ec_say - broadcasts a message to all players on the server
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if (!EC_API.isModuleEnabled("broadcasts") ) {
            // execute the original Minecraft /say command if custom broadcasts are disabled
            Bukkit.dispatchCommand( sender, "minecraft:say " + Utils.implode( args, " " ) );
            return true;
        }

        Channel channel = EC_API.get_broadcast_channel();
        String cmd_params = Utils.implode( args, " " );

        // update format of the message to send
        String message_final;
        String message_log;

        message_final = Utils.translate_chat_colors( channel.getFormat() );
        message_log = Utils.translate_chat_colors( channel.getFormat_clear() );

        message_final = EC_API.replace_placeholders( message_final, sender, channel );
        message_log = EC_API.replace_placeholders( message_log, sender, channel );

        message_final = message_final.replace( "{MESSAGE}", cmd_params );
        message_log = message_log.replace( "{MESSAGE}", cmd_params );

        // send formatted message to both, the sender and the recipient
        String finalMessage_final = message_final;
        String finalMessage_log   = message_log;
        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
            @Override
            public void run() {
                Bukkit.getPluginManager().callEvent(new ECChatEvent(finalMessage_final, finalMessage_log, cmd_params, sender, new HashSet<>( Bukkit.getOnlinePlayers() ), channel.getName() ));
            }
        });

        return true;
    } // end method

} // end class