package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.Utils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.File;

/**
 * Displays server rules from the rules.txt file.
 *
 * @author Martin Ambrus
 */
public class Ec_rules implements CommandExecutor {

    /**
     * Instance of EasyChat.
     */
    private Plugin plugin;

    /**
     * File name to read server rules from.
     */
    private String rules_file_name = "rules.txt";

    /**
     * Constructor, stores the plugin singleton instance.
     * @param ec EasyChat plugin instance.
     */
    public Ec_rules( final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * /ec_rules - displays server rules from the rules.txt file
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if ( !EC_API.isModuleEnabled("rules") ) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        // if the rules file does not exist yet, save it from our plugin's resources
        File rules_file = new File( EC_API.getEcDataDir(), this.rules_file_name );
        if ( !rules_file.exists() ) {
            this.plugin.saveResource(this.rules_file_name, true);
            EC_API.generateConsoleWarning( EC_API.__( "rules.creating-new-file" ) );
        }

        // read rules from the file
        String rules = Utils.read_file( EC_API.getEcDataDir() + File.separatorChar + this.rules_file_name, true );
        if ( !"".equals( rules ) ) {
            // replace translation placeholders
            rules = Utils.replace_translation_placeholders( rules );

            // replace our own + PAPI placeholders
            rules = EC_API.replace_placeholders( rules, sender, null );

            if ( !(sender instanceof Player) && EC_API.getConfigBoolean( "modules.console-strip-chat-colors" ) ) {
                rules = Utils.remove_chat_color_values( rules );
            }

            sender.sendMessage( rules );
        }

        return true;
    } // end method

} // end class