package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.Channel;
import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.Utils;
import com.martinambrus.easyChat.events.ECChatEvent;
import net.milkbowl.vault.chat.Chat;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.HashSet;
import java.util.Set;

import static org.bukkit.Bukkit.getServer;

/**
 * Replacement for the minecraft:say command to use our channels
 * and formatting, if enabled. Calls the original chat command
 * in case we've disabled this functionality.
 *
 * @author Martin Ambrus
 */
public class Ec_tell implements CommandExecutor {

    /**
     * Instance of EasyChat.
     */
    private Plugin plugin;

    /**
     * Sound to play when a player is mentioned in a message.
     */
    private Sound mention_sound;

    /**
     * Constructor, stores the plugin singleton instance.
     * @param ec EasyChat plugin instance.
     */
    public Ec_tell(final Plugin ec ) {
        this.plugin = ec;

        // load the notification sound
        String sound_name = EC_API.getConfigString( "modules.mention-sound-notification.sound-private-message" );
        boolean sound_found = false;

        // try private message sound
        try {
            if ( null != sound_name && !"".equals( sound_name ) && null != Sound.valueOf( sound_name ) ) {
                sound_found = true;
                this.mention_sound = Sound.valueOf( sound_name );
            }
        } catch ( IllegalArgumentException ex ) {
            // sound not found, let's try the global one
            EC_API.generateConsoleWarning( EC_API.__("error.chat-sound-notification-not-found", sound_name) );
        }

        if ( !sound_found ) {
            // try global sound
            sound_name = EC_API.getConfigString( "modules.mention-sound-notification.sound" );
            try {
                if ( null != sound_name && !"".equals( sound_name ) && null != Sound.valueOf( sound_name ) ) {
                    this.mention_sound = Sound.valueOf( sound_name );
                }
            } catch ( IllegalArgumentException ex ) {
                // sound not found, play a default one
                EC_API.generateConsoleWarning( EC_API.__( "error.chat-sound-notification-not-found", sound_name ) );
                // MC 1.9+
                try {
                    this.mention_sound = Sound.valueOf( "BLOCK_NOTE_BLOCK_PLING" );
                } catch ( IllegalArgumentException ex2 ) {
                    // MC 1.8
                    this.mention_sound = Sound.valueOf( "NOTE_PLING" );
                }
            }
        }
    } // end method

    /***
     * /ec_say - broadcasts a message to all players on the server
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if (!EC_API.isModuleEnabled("private-chat") || 2 > args.length ) {
            // execute the original Minecraft /tell command if custom private messaging is disabled
            // or if we don't have enough parameters
            Bukkit.dispatchCommand( sender, "minecraft:tell " + Utils.implode( args, " " ) );
            return true;
        }

        String username  = args[0];
        final Player recipient = Bukkit.getPlayer( username );

        final String[] args_without_username = new String[ args.length - 1 ];
        System.arraycopy(args, 1, args_without_username, 0, args.length - 1);

        String message  = Utils.implode( args_without_username, " " );
        Channel channel = EC_API.get_private_channel();

        // player not found
        if ( null == recipient || !recipient.isOnline() ) {
            sender.sendMessage( ChatColor.RED + EC_API.__( "general.player-not-found", username ) );
            return true;
        }

        // don't send messages to ourselves
        if ( recipient.getName().equals( sender.getName() ) ) {
            sender.sendMessage( ChatColor.RED + EC_API.__( "chat.trying-to-send-message-to-self", username ) );
            return true;
        }

        if ( sender instanceof Player ) {
            // make sure we're not ignoring this player
            if ( EC_API.get_player_ignores( (Player) sender ).contains( recipient.getUniqueId().toString() ) ) {
                sender.sendMessage( ChatColor.RED + EC_API.__( "chat.trying-to-msg-ignored-player", recipient.getName() ) );
                return true;
            }

            // if an ignored player is trying to send us a message, cancel it
            if ( EC_API.is_player_ignored( recipient, (Player) sender ) ) {
                sender.sendMessage( ChatColor.RED + EC_API.__( "chat.player-is-ignoring-you" ) );
                return true;
            }

            // check cooldown
            int cooldown = channel.check_cooldown( ((Player) sender).getUniqueId());
            if ( cooldown > 0 && !EC_API.checkPerms(sender, "ec.bypass.cooldown", false) ) {
                sender.sendMessage(EC_API.__("chat.wait-before-sending-next-message", cooldown));
                return true;
            }
        }

        // update format of the message to send
        String message_to_recipient;

        // sender will receive different message to receiver,
        // as the private chat format is "Player > me" for recipient
        // and "me > Player" for the sender
        String message_to_sender;

        // this is the clear message we'll log into console
        String message_log;

        message_to_recipient = Utils.translate_chat_colors( channel.getFormat() );
        message_to_sender = Utils.translate_chat_colors( channel.getFormat() );
        message_log = Utils.translate_chat_colors( channel.getFormat_clear() );

        // replace name and display name manually for sender, since we're replacing it by the word "me"
        message_to_sender = message_to_sender.replace( "{PLAYERNAME}", EC_API.__( "general.me" ) );
        message_to_sender = message_to_sender.replace( "{DISPLAYNAME}", EC_API.__( "general.me" ) );

        // replace placeholders normally for recipient
        message_to_recipient = EC_API.replace_placeholders( message_to_recipient, sender, channel );
        message_log = EC_API.replace_placeholders( message_log, sender, channel );

        // replace message placeholder
        message_to_sender = message_to_sender.replace( "{MESSAGE}", message );
        message_to_recipient = message_to_recipient.replace( "{MESSAGE}", message );
        message_log = message_log.replace( "{MESSAGE}", message );

        // replace recipient name and display name manually for recipient, since we're replacing it by the word "me"
        message_to_recipient = message_to_recipient.replace( "{RECIPIENT}", EC_API.__( "general.me" ) );
        message_to_recipient = message_to_recipient.replace( "{RECIPIENT_NAME}", EC_API.__( "general.me" ) );

        // replace recipient name and display name normally for sender
        if ( EC_API.isVaultEnabled() ) {
            Chat vaultChat = getServer().getServicesManager().load( Chat.class );

            String prefix = vaultChat.getPlayerPrefix( recipient );
            if ( prefix.equals("") ) {
                prefix = vaultChat.getGroupPrefix( recipient.getWorld(), EC_API.getPlayerPrimaryPermGroup( recipient ) );
            }
            prefix = Utils.translate_chat_colors( prefix );

            String suffix = vaultChat.getPlayerSuffix( recipient );
            if ( suffix.equals("") ) {
                suffix = vaultChat.getGroupSuffix( recipient.getWorld(), EC_API.getPlayerPrimaryPermGroup( recipient ) );
            }
            suffix = Utils.translate_chat_colors( suffix );

            message_to_sender = message_to_sender.replace( "{RECIPIENT}", prefix + recipient.getDisplayName() + suffix );
            message_log = message_log.replace( "{RECIPIENT}", prefix + recipient.getDisplayName() + suffix );
        } else {
            message_to_sender = message_to_sender.replace( "{RECIPIENT}", recipient.getDisplayName() );
            message_log = message_log.replace( "{RECIPIENT}", recipient.getDisplayName() );
        }

        message_to_sender = message_to_sender.replace( "{RECIPIENT_NAME}", username );
        message_log = message_log.replace( "{RECIPIENT_NAME}", username );

        // send formatted message to the sender first
        if ( sender instanceof Player ) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "tellraw " + sender.getName() + " " + message_to_sender ) ;
        }

        // add recipient to players receiving this message
        Set<Player> recipients = new HashSet<>();
        recipients.add( recipient );

        // send formatted message to recipient
        String finalMessage_final = message_to_recipient;
        String finalMessage_log   = message_log;
        Bukkit.getScheduler().runTask(this.plugin, new Runnable() {
            @Override
            public void run() {
                Bukkit.getPluginManager().callEvent(new ECChatEvent(finalMessage_final, finalMessage_log, message, sender, recipients, channel.getName()));

                // play a custom sound effect, if we have this feature enabled
                if (
                    EC_API.isModuleEnabled( "mention-sound-notification" ) &&
                    EC_API.getConfigBoolean( "modules.mention-sound-notification.use-for-private-messages" ) &&
                    EC_API.checkPerms( recipient, "ec.mention-sound-notification", false )
                ) {
                    recipient.playSound( recipient.getEyeLocation(), Ec_tell.this.mention_sound, 1, 1 );
                }
            }
        });

        return true;
    } // end method

} // end class