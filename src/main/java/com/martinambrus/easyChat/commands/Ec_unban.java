package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.Channel;
import com.martinambrus.easyChat.EC_API;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.Plugin;

/**
 * Un-bans player from a channel.
 *
 * @author Martin Ambrus
 */
public class Ec_unban implements CommandExecutor {

    /**
     * Instance of EasyChat.
     */
    private Plugin plugin;

    /**
     * Constructor, stores the plugin singleton instance.
     * @param ec EasyChat plugin instance.
     */
    public Ec_unban(final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * /ec_ban - un-bans player from a channel
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if (!EC_API.isModuleEnabled("channels") || !EC_API.isModuleEnabled("unban-channel-command")) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        // check parameters
        if ( 2 > args.length) {
            return false;
        }

        // check if we have a valid channel name provided as parameter
        Channel validated_channel = null;
        String requested_channel  = args[0].toLowerCase();

        for ( Channel channel : EC_API.get_all_channels() ) {
            if ( requested_channel.equals( channel.getName().toLowerCase() ) || requested_channel.equals( (channel.getShortcut() + "").toLowerCase() ) ) {
                validated_channel = channel;
                break;
            }
        }

        // if we've not found a valid channel, let the player know
        if ( null == validated_channel ) {
            sender.sendMessage( EC_API.__("channel.not-found") );
            return true;
        }

        // unban player from that channel
        if ( EC_API.unban_player_from_channel( args[1], validated_channel ) ) {
            sender.sendMessage( ChatColor.GREEN + EC_API.__( "channel.player-unbanned", args[1], validated_channel.getColor() + validated_channel.getName() ) );
        } else {
            sender.sendMessage( ChatColor.YELLOW + EC_API.__( "channel.player-not-banned", args[1], validated_channel.getColor() + validated_channel.getName() ) );
        }

        return true;
    } // end method

} // end class