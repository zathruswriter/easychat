package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.events.ECReloadEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Command which will show current EC version either
 * to the player or console running this command.
 *
 * @author Martin Ambrus
 */
public class Ec_version implements CommandExecutor {

    /***
     * /ec_version - shows current EasyChat version
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true.
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        if (!EC_API.isModuleEnabled("get-easychat-version")) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        //noinspection HardCodedStringLiteral
        sender.sendMessage(ChatColor.AQUA + EC_API.getEcName() + ' ' + EC_API.__("general.version") + ' ' + EC_API
            .getEcVersion());

        // fire up the reload event for the updater to refresh current version and potentially inform us about a new one
        Bukkit.getPluginManager().callEvent(new ECReloadEvent("Updater")); //NON-NLS

        return true;
    } // end method

} // end class