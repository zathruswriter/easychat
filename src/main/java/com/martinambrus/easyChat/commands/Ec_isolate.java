package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.Channel;
import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.Utils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Isolates player in a single channel and backs up their old
 * channel and subscriptions, so they can be restored once player
 * decides to leave the isolation (and if enabled in config).
 *
 * @author Martin Ambrus
 */
public class Ec_isolate implements CommandExecutor {

    /**
     * Instance of EasyChat.
     */
    private Plugin plugin;

    /**
     * Constructor, stores the plugin singleton instance.
     * @param ec EasyChat plugin instance.
     */
    public Ec_isolate(final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * /ec_isolate - isolates a player into a single channel, cancelling or pausing all of their subscriptions
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if (!EC_API.isModuleEnabled("channels") || !EC_API.isModuleEnabled("isolate-channel-command")) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        // only players can be isolated into channels
        if ( !( sender instanceof Player ) ) {
            sender.sendMessage( ChatColor.YELLOW + EC_API.__( "error.command-player-only" ) );
            return true;
        }

        // if we don't have any parameters, restore previous isolation
        if ( args.length == 0 ) {
            // only restore if this feature is enabled
            if ( EC_API.isModuleEnabled("channels.isolate-command-remembers-subscriptions") ) {
                if ( EC_API.unisolate_player( ((Player) sender).getUniqueId() ) ) {
                    sender.sendMessage( ChatColor.GREEN + EC_API.__("channel.unisolated") );
                    return true;
                } else {
                    sender.sendMessage( ChatColor.YELLOW + EC_API.__("channel.nothing-to-unisolate") );
                    return false;
                }
            } else {
                return false;
            }
        }

        // check if we have a valid channel name provided as parameter
        Channel validated_channel = null;
        String requested_channel  = args[0].toLowerCase();

        for ( Channel channel : EC_API.get_all_channels() ) {
            if ( requested_channel.equals( channel.getName().toLowerCase() ) || requested_channel.equals( (channel.getShortcut() + "").toLowerCase() ) ) {
                // check permissions
                if ( channel.getJoin_permissions().size() == 0 || EC_API.checkPerms( sender, Utils.implode( channel.getJoin_permissions(), " OR " ), false ) ) {
                    // check if this channel is enabled in player's current world
                    if (
                        channel.getExcluded_worlds().isEmpty()
                        ||
                        !(sender instanceof Player)
                        ||
                        (
                            channel.getInvert_excluded_worlds() && channel.getExcluded_worlds().contains( ((Player) sender).getWorld().getName() )
                            ||
                            !channel.getInvert_excluded_worlds() && !channel.getExcluded_worlds().contains( ((Player) sender).getWorld().getName() )
                        )
                    ) {
                        validated_channel = channel;
                        break;
                    } else {
                        // channel disabled in player's current world
                        sender.sendMessage( EC_API.__( "channel.disabled-in-world", channel.getColor() + channel.getName() ) );
                        return true;
                    }
                } else {
                    // found channel but player has no permission to join it
                    sender.sendMessage( ChatColor.RED + EC_API.__("channel.no-permission-join") );
                    return true;
                }
            }
        }

        // if we've not found a valid channel, let the player know
        if ( null == validated_channel ) {
            sender.sendMessage( EC_API.__("channel.not-found") );
            return true;
        }

        // check if this channel is not password-protected
        if ( !validated_channel.getPassword().equals("") ) {
            // channel is password-protected, did we provide one?
            if ( 2 > args.length || (!args[1].equals( validated_channel.getPassword() ) && !EC_API.checkPerms( sender, "ec.bypass.password", false ) ) ) {
                sender.sendMessage( ChatColor.RED + EC_API.__( "channel.incorrect-password", validated_channel.getName() ) );
                return true;
            }
        }

        // isolate player into that channel
        EC_API.isolate_player_in_channel( ((Player) sender).getUniqueId(), validated_channel );

        // let the player know
        sender.sendMessage( ChatColor.YELLOW + EC_API.__( "channel.isolated", validated_channel.getColor() + validated_channel.getName() ) );

        return true;
    } // end method

} // end class