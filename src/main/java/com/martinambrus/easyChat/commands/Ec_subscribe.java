package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.Channel;
import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.Utils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;

/**
 * Subscribes to a channel or lists available ones.
 *
 * @author Martin Ambrus
 */
public class Ec_subscribe implements CommandExecutor {

    /**
     * Instance of EasyChat.
     */
    private Plugin plugin;

    /**
     * Constructor, stores the plugin singleton instance.
     * @param ec EasyChat plugin instance.
     */
    public Ec_subscribe(final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * /ec_subscribe - subscribes to a channel or lists available ones
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if (!EC_API.isModuleEnabled("channels") || !EC_API.isModuleEnabled("subscribe-channel-command")) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        // if we don't have any parameters, list all channels available to player
        if ( args.length == 0 ) {
            // only list channels if we have that feature enabled
            if ( EC_API.isModuleEnabled("join-channel-command.available-channels-listing") ) {
                sender.sendMessage( EC_API.__("commands.channels-list") + ":" );

                int channels_size = EC_API.get_all_channels().size();

                if ( null != EC_API.get_spy_channel() ) {
                    channels_size++;
                }

                Channel[] channels = EC_API.get_all_channels().toArray( new Channel[ channels_size ] );

                if ( null != EC_API.get_spy_channel() ) {
                    channels[ channels.length - 1 ] = EC_API.get_spy_channel();
                }

                for ( Channel channel : channels ) {
                    if ( channel.getJoin_permissions().size() == 0 || EC_API.checkPerms( sender, Utils.implode( channel.getJoin_permissions(), " OR " ), false ) ) {
                        // check if this channel is enabled in player's current world
                        if (
                            channel.getExcluded_worlds().isEmpty()
                            ||
                            !(sender instanceof Player)
                            ||
                            (
                                channel.getInvert_excluded_worlds() && channel.getExcluded_worlds().contains( ((Player) sender).getWorld().getName() )
                                ||
                                !channel.getInvert_excluded_worlds() && !channel.getExcluded_worlds().contains( ((Player) sender).getWorld().getName() )
                            )
                        ) {
                            String channel_color_real = Utils.translate_chat_colors( channel.getColor() );
                            String one_time_prefix = ( !channel.getOne_time_msg_pattern().equals("") ? " " + channel.getOne_time_msg_pattern() : "" );

                            if ( channel.getOne_time_msg_permissions().size() > 0 && !EC_API.checkPerms( sender, Utils.implode( channel.getOne_time_msg_permissions(), " OR " ), false ) ) {
                                one_time_prefix = "";
                            }

                            String channel_msg = "- " +
                                (!channel.getPassword().equals("") ? ChatColor.ITALIC + "$" : "") +
                                channel_color_real + channel.getName() + ChatColor.RESET +
                                " (" + channel_color_real + channel.getShortcut() + ChatColor.RESET + ")" +
                                one_time_prefix;

                            if ( !(sender instanceof Player) && ( EC_API.getConfigBoolean("modules.console-strip-chat-colors") ) ) {
                                channel_msg = Utils.remove_chat_color_values( channel_msg );
                            }

                            sender.sendMessage( channel_msg );
                        }
                    }
                }

                return true;
            } else {
                return false;
            }
        }

        // only players can subscribe to channels
        if ( !( sender instanceof Player ) ) {
            sender.sendMessage( ChatColor.YELLOW + EC_API.__( "error.command-player-only" ) );
            return true;
        }

        // check if we have a valid channel name provided as parameter
        Channel validated_channel = null;
        String requested_channel  = args[0].toLowerCase();

        int channels_size = EC_API.get_all_channels().size();

        if ( null != EC_API.get_spy_channel() ) {
            channels_size++;
        }

        Channel[] channels = EC_API.get_all_channels().toArray( new Channel[ channels_size ] );

        if ( null != EC_API.get_spy_channel() ) {
            channels[ channels.length - 1 ] = EC_API.get_spy_channel();
        }

        for ( Channel channel : channels ) {
            if ( requested_channel.equals( channel.getName().toLowerCase() ) || requested_channel.equals( (channel.getShortcut() + "").toLowerCase() ) ) {
                // check permissions
                if ( channel.getSubscribe_permissions().size() == 0 || EC_API.checkPerms( sender, Utils.implode( channel.getSubscribe_permissions(), " OR " ), false ) ) {
                    // check if this channel is enabled in player's current world
                    if (
                        channel.getExcluded_worlds().isEmpty()
                        ||
                        !(sender instanceof Player)
                        ||
                        (
                            channel.getInvert_excluded_worlds() && channel.getExcluded_worlds().contains( ((Player) sender).getWorld().getName() )
                            ||
                            !channel.getInvert_excluded_worlds() && !channel.getExcluded_worlds().contains( ((Player) sender).getWorld().getName() )
                        )
                    ) {
                        validated_channel = channel;
                        break;
                    } else {
                        // channel disabled in player's current world
                        sender.sendMessage( EC_API.__( "channel.disabled-in-world", channel.getColor() + channel.getName() ) );
                        return true;
                    }
                } else {
                    // found channel but player has no permission to join it
                    sender.sendMessage( ChatColor.RED + EC_API.__("channel.no-permission-subscribe") );
                    return true;
                }
            }
        }

        // if we've not found a valid channel, let the player know
        if ( null == validated_channel ) {
            sender.sendMessage( EC_API.__("channel.not-found") );
            return true;
        }

        // check if this channel is not password-protected
        if ( !validated_channel.getPassword().equals("") ) {
            // channel is password-protected, did we provide one?
            if ( 2 > args.length || (!args[1].equals( validated_channel.getPassword() ) && !EC_API.checkPerms( sender, "ec.bypass.password", false ) ) ) {
                sender.sendMessage( ChatColor.RED + EC_API.__( "channel.incorrect-password", validated_channel.getName() ) );
                return true;
            }
        }

        // subscribe to the channel
        if ( EC_API.subscribe_channel( ((Player) sender).getUniqueId(), validated_channel ) ) {
            // store subscriptions
            List<String> channel_names = new ArrayList<>();

            for ( Channel channel : EC_API.get_player_subscriptions_list( ((Player) sender).getUniqueId() ) ) {
                channel_names.add( channel.getName() );
            }

            EC_API.set_player_subscriptions( (Player) sender, Utils.implode(channel_names, ",") );
        } else {
            // player already subscribed to this channel
            sender.sendMessage( ChatColor.YELLOW + EC_API.__( "channel.already-subscribed" ) );
            return true;
        }

        // let the player know
        sender.sendMessage( ChatColor.YELLOW + EC_API.__( "channel.successfully-subscribed", validated_channel.getColor() + validated_channel.getName() ) );

        return true;
    } // end method

} // end class