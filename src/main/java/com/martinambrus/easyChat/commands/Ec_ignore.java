package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.EC_API;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * Ignores a player in chat.
 *
 * @author Martin Ambrus
 */
public class Ec_ignore implements CommandExecutor {

    /**
     * Instance of EasyChat.
     */
    private Plugin plugin;

    /**
     * Constructor, stores the plugin singleton instance.
     * @param ec EasyChat plugin instance.
     */
    public Ec_ignore(final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * /ec_ignore - ignores a player in chat
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        // only players can ignore other players
        if ( !( sender instanceof Player ) ) {
            sender.sendMessage( ChatColor.YELLOW + EC_API.__( "error.command-player-only" ) );
            return true;
        }

        //noinspection HardCodedStringLiteral
        if (
            (
                !EC_API.isModuleEnabled("channels") &&
                !EC_API.isModuleEnabled("default-chat-format") ||
                !EC_API.isModuleEnabled("broadcasts") ||
                !EC_API.isModuleEnabled("private-chat")
            ) ||
            !EC_API.isModuleEnabled("ignore-channel-command")
        ) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        // check parameters
        if ( 1 > args.length) {
            return false;
        }

        // check that we've provided a valid player
        Player ignored = Bukkit.getPlayer( args[0] );
        if ( null == ignored ) {
            sender.sendMessage( ChatColor.RED + EC_API.__( "general.player-not-found", args[0]) );
            return true;
        }

        if ( sender.getName().equals(ignored.getName() ) ) {
            sender.sendMessage( ChatColor.RED + EC_API.__( "chat.cannot-ignore-self" ) );
            return true;
        }

        // check whether the player is ignored already
        if ( EC_API.is_player_ignored( (Player) sender, ignored ) ) {
            // un-ignore this player
            EC_API.remove_player_ignore( (Player) sender, ignored );
            sender.sendMessage( ChatColor.GREEN + EC_API.__("chat.player-unignored", ignored.getName() ) );
        } else {
            // add this player to the list of ignores
            EC_API.add_player_ignore( (Player) sender, ignored );
            sender.sendMessage( ChatColor.GREEN + EC_API.__("chat.player-ignored", ignored.getName() ) );
        }

        return true;
    } // end method

} // end class