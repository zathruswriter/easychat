package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.Channel;
import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.Utils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Unsubscribes from a channel or lists channels to which the player is subscribed.
 *
 * @author Martin Ambrus
 */
public class Ec_unsubscribe implements CommandExecutor {

    /**
     * Instance of EasyChat.
     */
    private Plugin plugin;

    /**
     * Constructor, stores the plugin singleton instance.
     * @param ec EasyChat plugin instance.
     */
    public Ec_unsubscribe(final Plugin ec ) {
        this.plugin = ec;
    } // end method

    /***
     * /ec_unsubscribe - unsubscribes from a channel or lists channels to which the player is subscribed
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if (!EC_API.isModuleEnabled("channels") || !EC_API.isModuleEnabled("unsubscribe-channel-command")) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        // only players can unsubscribe from channels
        if ( !( sender instanceof Player ) ) {
            sender.sendMessage( ChatColor.YELLOW + EC_API.__( "error.command-player-only" ) );
            return true;
        }

        // if we don't have any parameters, list all channels to which the player is subscribed
        UUID player_uuid = ((Player) sender).getUniqueId();
        if ( args.length == 0 ) {
            if ( EC_API.get_player_subscriptions_list( player_uuid ).size() == 0 ) {
                sender.sendMessage( ChatColor.YELLOW + EC_API.__("channel.no-subscriptions") );
            } else {
                sender.sendMessage( EC_API.__("channel.your-subscriptions") + ":" );

                for ( Channel channel : EC_API.get_player_subscriptions_list( player_uuid ) ) {
                    String channel_color_real = Utils.translate_chat_colors( channel.getColor() );
                    sender.sendMessage(
                        "- " +
                        (!channel.getPassword().equals("") ? ChatColor.ITALIC + "$" : "") +
                        channel_color_real + channel.getName() + ChatColor.RESET +
                        " (" + channel_color_real + channel.getShortcut() + ChatColor.RESET + ")" +
                        ( !channel.getOne_time_msg_pattern().equals("") ? " " + channel.getOne_time_msg_pattern() : "" )
                    );
                }
            }

            return true;
        }

        // check if we have a valid channel name provided as parameter
        Channel validated_channel = null;
        String requested_channel  = args[0].toLowerCase();

        for ( Channel channel : EC_API.get_player_subscriptions_list( player_uuid ) ) {
            if ( requested_channel.equals( channel.getName().toLowerCase() ) || requested_channel.equals( (channel.getShortcut() + "").toLowerCase() ) ) {
                validated_channel = channel;
                break;
            }
        }

        // player is not subscribed to the channel provided
        if ( null == validated_channel ) {
            sender.sendMessage( ChatColor.RED + EC_API.__("channel.not-subscribed") );
            return true;
        }

        // unsubscribe from the channel
        EC_API.unsubscribe_channel( ((Player) sender).getUniqueId(), validated_channel );

        // store subscriptions
        List<String> channel_names = new ArrayList<>();

        for ( Channel channel : EC_API.get_player_subscriptions_list( ((Player) sender).getUniqueId() ) ) {
            channel_names.add( channel.getName() );
        }

        EC_API.set_player_subscriptions( (Player) sender, Utils.implode(channel_names, ",") );

        // let the player know
        sender.sendMessage( ChatColor.YELLOW + EC_API.__( "channel.successfully-unsubscribed", validated_channel.getColor() + validated_channel.getName() ) );

        return true;
    } // end method

} // end class