package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.Utils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.File;

/**
 * Displays MOTD from the motd.txt file.
 *
 * @author Martin Ambrus
 */
public class Ec_motd implements CommandExecutor {

    /**
     * Instance of EasyChat.
     */
    private Plugin plugin;

    /**
     * File name to read MOTD from.
     */
    private String motd_file_name = "motd.txt";

    /**
     * File name to read MOTD from.
     */
    private String motd_first_join_file_name = "motd-first-join.txt";

    /**
     * Constructor, stores the plugin singleton instance.
     * @param ec EasyChat plugin instance.
     */
    public Ec_motd( final Plugin ec ) {
        this.plugin = ec;

        // start join events listener, so we can display MOTD for joining players :)
        if ( EC_API.isModuleEnabled("motd") ) {
            EC_API.startRequiredListener( "motdJoinListener" );
        }
    } // end method

    /***
     * /ec_motd - displays MOTD from the motd.txt file
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true;
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if ( !EC_API.isModuleEnabled("motd") ) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        // if the MOTD file does not exist yet, save it from our plugin's resources
        File motd_file = new File( EC_API.getEcDataDir(), this.motd_file_name );
        if ( !motd_file.exists() ) {
            this.plugin.saveResource(this.motd_file_name, true);
            EC_API.generateConsoleWarning( EC_API.__( "motd.creating-new-file" ) );

            // also check MOTD first-join file
            File motd_first_join_file = new File( EC_API.getEcDataDir(), this.motd_first_join_file_name );
            if ( !motd_first_join_file.exists() ) {
                this.plugin.saveResource(this.motd_first_join_file_name, true);
                EC_API.generateConsoleWarning( EC_API.__( "motd.creating-new-first-join-file" ) );
            }
        }

        // read MOTD from the file
        String motd = Utils.read_file( EC_API.getEcDataDir() + File.separatorChar + this.motd_file_name, true );
        if ( !"".equals( motd ) ) {
            // replace translation placeholders
            motd = Utils.replace_translation_placeholders( motd );

            // replace our own + PAPI placeholders
            motd = EC_API.replace_placeholders( motd, sender, null );

            if ( !(sender instanceof Player) && EC_API.getConfigBoolean( "modules.console-strip-chat-colors" ) ) {
                motd = Utils.remove_chat_color_values( motd );
            }

            sender.sendMessage( motd );
        }

        return true;
    } // end method

} // end class