package com.martinambrus.easyChat.commands.bungee;

import com.martinambrus.easyChat.EasyChat_Bungee;
import com.martinambrus.easyChat.events.bungee.ECReloadEvent;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

/**
 * Command which will show current EC version
 * in the Bungee console.
 *
 * @author Martin Ambrus
 */
public class Ec_version extends Command {

    /**
     * {@link EasyChat_Bungee EasyChat_Bungee} instance.
     */
    private final EasyChat_Bungee plugin;

    /**
     * Constructor.
     *
     * @param plugin {@link EasyChat_Bungee EasyChat_Bungee} instance.
     */
    public Ec_version( EasyChat_Bungee plugin ) {
        super("Ec_version");
        this.plugin = plugin;
    } // end method

    /***
     * /ec_version - shows current EasyChat version
     *
     * @param sender The player who is calling this command.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true.
     */
    public void execute( final CommandSender sender, final String[] args) {
        //noinspection HardCodedStringLiteral
        sender.sendMessage( ChatColor.AQUA + plugin.getDescription().getName() + " version " + plugin.getDescription().getVersion() + ChatColor.RESET );

        // fire up the reload event for the updater to refresh current version and potentially inform us about a new one
        plugin.getProxy().getPluginManager().callEvent(new ECReloadEvent("Updater")); //NON-NLS
    } // end method

} // end class