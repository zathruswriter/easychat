package com.martinambrus.easyChat.commands;

import com.martinambrus.easyChat.EC_API;
import com.martinambrus.easyChat.events.ECReloadEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

/**
 * Command which sends a reload event that in turn reloads
 * all data for EasyChat.
 *
 * @author Martin Ambrus
 */
public class Ec_reload implements CommandExecutor {

    /**
     * /ec_reload - sends event to reload the EC configuration
     *
     * @param sender The player who is calling this command.
     * @param cmd The actual command that is being executed.
     * @param unused Name of the command which is being executed.
     * @param args Any arguments passed to this command.
     *
     * @return Always returns true.
     */
    @Override
    public boolean onCommand(final CommandSender sender, final Command cmd, final String unused, final String[] args) {
        //noinspection HardCodedStringLiteral
        if (!EC_API.isModuleEnabled("reload-easychat")) {
            sender.sendMessage(ChatColor.RED + EC_API.__("general.feature-disabled"));
            return true;
        }

        sender.sendMessage(ChatColor.GREEN + EC_API.__("commands.reload-init", EC_API.getEcName()));

        // fire up the reload event to clear up caches
        Bukkit.getScheduler()
              .scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin(EC_API.getEcName()), new Runnable() {

            @Override
            public void run() {
                // perform the check
                Bukkit.getPluginManager().callEvent(new ECReloadEvent(""));

                sender.sendMessage(ChatColor.GREEN + EC_API.__("commands.reload-complete", EC_API.getEcName()));
            }

        }, 20); // 0 = will be run as soon as the server finished loading

        return true;
    } // end method

} // end class
