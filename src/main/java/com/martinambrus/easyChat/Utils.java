package com.martinambrus.easyChat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.util.StringUtil;

import java.io.*;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.bukkit.ChatColor.COLOR_CHAR;

/**
 * Various common utilities class used thorough the whole EC plugin.
 *
 * @author Martin Ambrus
 */
public enum Utils {
    ;

    /**
     * The actual (cached) version of the server we're running.
     */
    private static String mcVersion;

    /**
     * Minecraft colors pattern which uses the ampersand syntax (&b, &1...)
     */
    private static String and_colors_pattern = "&[1-9a-fk-or]";

    /**
     * Minecraft colors pattern which uses original chat character.
     */
    private static String chat_colors_pattern = COLOR_CHAR + "[0-9a-fk-orx]";

    /**
     * Minecraft HEX colors pattern prefix which uses the ampersand syntax (&#FFFFFF...)
     */
    public static String hex_colors_pattern_prefix = "&#";

    /**
     * Minecraft HEX colors pattern which uses the ampersand syntax (&#FFFFFF...)
     */
    public static String hex_colors_pattern = hex_colors_pattern_prefix + "([A-Fa-f0-9]{6})";

    /**
     * Returns current unix timestamp.
     * @return Returns current unix timestamp as an Integer.
     */
    public static int getUnixTimestamp() {
        return getUnixTimestamp(0L);
    } // end method

    /***
     * Converts given timestamp in milliseconds to Unix Timestamp in seconds.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * int lastUpdateTimestamp = Utils.getUnixTimestamp(0L);
     * </pre>
     *
     * @param i Integer timestamp in milliseconds to convert.
     * @return Returns Unix Timestamp in seconds.
     */
    public static int getUnixTimestamp(Long i) {
        if (0 == i) {
            i = System.currentTimeMillis();
        }

        //noinspection IntegerDivisionInFloatingPointContext
        return (int) Math.floor(i / 1000L);
    } // end method

    /**
     * Grabs the numerical version portion of the full server's version.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * String serverVersion = Utils.getMinecraftVersion();
     * </pre>
     *
     * @return Returns server version as a string (for example "1.11.2").
     */
    public static String getMinecraftVersion() {
        return getMinecraftVersion(Bukkit.getServer().getBukkitVersion());
    } // end method

    /**
     * Grabs the numerical version portion of the full server's version.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * Utils.getMinecraftVersion(Bukkit.getServer().getBukkitVersion());
     * </pre>
     *
     * @param serverVersion An optional version string to extract the version from.
     *
     * @return Returns server version as a string (for example "1.11.2").
     */
    public static String getMinecraftVersion(final String serverVersion) {
        if (null != mcVersion) {
            return mcVersion;
        }

        final Pattern pattern = Pattern.compile("\\(.*?\\) ?");
        final Matcher matcher = pattern.matcher(serverVersion);
        String regex = null;
        if (matcher.find()) {
            //noinspection Annotator
            regex = matcher.group(0).replaceAll("\\([M][C][:][\" \"]", "").replace(')', ' ').trim(); //NON-NLS
        }

        if (null == regex) {
            try {
                regex = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3];
            } catch (final IndexOutOfBoundsException e) {
                regex = serverVersion.replaceAll("\\.", "_");
            }
        }

        mcVersion = regex;

        return mcVersion;
    } // end method

    /***
     * Returns a flat version of a list, similarly to how .toString() does it.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * // the following line could also be written as: HashSet&lt;String&gt; cmds = new HashSet&lt;String&gt;();
     * ArrayList&lt;String&gt; cmds = new ArrayList&lt;String&gt;();
     * cmds.add("ban");
     * cmds.add("say");
     * cmds.add("pardon");
     *
     * // prints out: "you have requested to disable the following commands: ban, say, pardon"
     * System.out.println("you have requested to disable the following commands: " + Utils.flatten(cmds, true));
     * </pre>
     *
     * @param l The list to convert into a string.
     * @param addCommas If this parameter is set, commas will be used to join list elements together.
     *
     * @return Returns a comma-separated list of values from the String List given.
     */
    public static String flatten(final Collection<String> l, final boolean... addCommas) {
        if (l.isEmpty()) {
            return "";
        }

        final StringBuilder builder = new StringBuilder();
        for (final String value : l) {
            builder.append(value).append((0 < addCommas.length) ? ", " : " ");
        }

        final String out = builder.toString();
        return 0 < addCommas.length ? out.substring(0, out.length() - 2) : out.substring(0, out.length() - 1);
    } // end method

    /***
     * Returns a flat version of an array, similarly to how .toString() does it.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * String[] cmds = new String[3];
     * cmds[0] = "ban";
     * cmds[1] = "say";
     * cmds[2] = "pardon";
     *
     * // prints out: "you have requested to disable the following commands: ban, say, pardon"
     * System.out.println("you have requested to disable the following commands: " + Utils.flatten(cmds, true));
     * </pre>
     *
     * @param l The array to convert into a string.
     * @param addCommas If this parameter is set, commas will be used to join array elements together.
     *
     * @return Returns a comma-separated list of values from the String[] array given.
     */
    public static String flatten(final String[] l, final boolean... addCommas) {
        if (0 == l.length) {
            return "";
        }

        final StringBuilder builder = new StringBuilder();
        for (final String value : l) {
            builder.append(value).append((0 < addCommas.length) ? ", " : " ");
        }

        final String out = builder.toString();
        return 0 < addCommas.length ? out.substring(0, out.length() - 2) : out.substring(0, out.length() - 1);
    } // end method

    /***
     * Returns a flat version of an array, similarly to how .toString() does it.
     * This method signature allows for choosing your own delimiter.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * String[] cmds = new String[2];
     * cmds[0] = "minecraft";
     * cmds[1] = "ban";
     *
     * // prints out: "the plugin command was minecraft:ban"
     * System.out.println("the plugin command was " + Utils.flatten(cmds, ":"));
     * </pre>
     *
     * @param l The array to convert into a string.
     * @param delimiter The actual delimiter to use when joining array elements together.
     *
     * @return Returns a delimiter-separated list of values from the String[] array given.
     */
    public static String flatten(final String[] l, final String delimiter) {
        if (0 == l.length) {
            return "";
        }

        final StringBuilder builder = new StringBuilder();
        for (final String value : l) {
            builder.append(value).append(delimiter);
        }

        final String out = builder.toString();
        return out.substring(0, out.length() - 1);
    } // end method

    /**
     * Takes an unmodifiable list and turns it into a modifiable one.
     *
     * @param theList The list we want to clone and make modifiable.
     *
     * @return Returns an ordinary cloned, modifiable list.
     */
    public static List<String> makeListMutable(Iterable<String> theList) {
        return new ArrayList<String>((Collection<String>) theList);
    } // end method

    /***
     * Takes command arguments that may contain quotes and will compact them,
     * so for example "hello dolly" will become a single argument.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * String[] args = new String[3];
     * args[0] = "command";
     * args[1] = "\"hello";
     * args[2] = "dolly\"";
     *
     * // newArgs will be: ["command", "\"hello dolly\""]
     * String[] newArgs = Utils.compactQuotedArgs(args);
     * </pre>
     *
     * @param args A String[] array with all of the command's arguments.
     *
     * @return Returns a String[] array with quoted original arguments consolidated
     *         and the rest untouched.
     */
    @SuppressWarnings("NonConstantStringShouldBeStringBuffer")
    public static String[] compactQuotedArgs(final String[] args) {
        String tmpArg = "";
        boolean tmpArgDone = true;
        boolean beginningFound = false;
        boolean endingFound = false;
        List<String> newArgs = new ArrayList<String>();

        for (String arg : args) {
            // if the argument doesn't contain spaces but does contain quotes, simply unquote it
            final boolean hasSpaces = arg.contains(" ");
            final boolean startsWithQuote = arg.startsWith("\"");
            final boolean endsWithQuote = arg.endsWith("\"");

            if (!hasSpaces && startsWithQuote && endsWithQuote && !"\"".equals( arg )) {
                newArgs.add(arg.replace("\"", ""));
            }

            if (hasSpaces) {
                arg = arg.substring(0, arg.indexOf(' '));
            }

            if (startsWithQuote && tmpArgDone ) {
                tmpArg = arg.substring(1);
                tmpArgDone = false;
                beginningFound = true;
            } else if (endsWithQuote) {
                if ( !"\"".equals( arg ) ) {
                    tmpArg += ' ' + arg.substring(0, arg.length() - 1);
                }
                newArgs.add(tmpArg);
                tmpArgDone = true;
                endingFound = true;
            } else {
                if (!tmpArgDone) {
                    tmpArg += ' ' + arg;
                } else {
                    // tmp argument is done and this one does not contain quotes,
                    // so we're starting a new argument
                    newArgs.add(arg);
                }
            }
        }

        // if we've not found a closing bracket, add one ourselves
        if ( beginningFound && !endingFound ) {
            newArgs.add( tmpArg );
        }

        // if we've not found an opening bracket but we did find a closing one,
        // make everything except the first argument a redirect
        if ( !beginningFound && endingFound ) {
            final List<String> tmpArgs = new ArrayList<String>();
            boolean firstArgDone = false;
            String firstArg = "";

            for (String arg : newArgs) {
                if ( !firstArgDone ) {
                    firstArgDone = true;
                    firstArg = arg;
                    continue;
                }

                tmpArgs.add( arg );
            }

            newArgs.clear();
            newArgs.add( firstArg );
            newArgs.add( implode( tmpArgs, " " ) );
        }

        return newArgs.toArray(new String[newArgs.size()]);
    } // end method

    /***
     * Logs a debug message into the console if debug is enabled in EasyChat.
     *
     * @param msg The message to log.
     * @param ec Instance of {@link EasyChat EasyChat}.
     */
    public static void logDebug(final String msg, final EasyChat ec) {
        if (ec.getDebug()) {
            //noinspection UseOfSystemOutOrSystemErr
            System.out.println(msg);
        }
    } // end method

    /***
     * Copies the original array into a new array, shortening it to the given length.
     * Copy will start at index 0.
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * String[] cmds = new String[3];
     * cmds[0] = "ban";
     * cmds[1] = "say";
     * cmds[2] = "pardon";
     *
     * // cmds2 will be ["ban", "say"]
     * String[] cmds2 = Utils.shortenStringArray(cmds, 2);
     * </pre>
     *
     * @param orig Original array to copy from.
     * @param len Length to which we want to shorten the original array.
     *
     * @return Returns a shortened version of the original array.
     */
    public static String[] shortenStringArray(final String[] orig, final Integer len) {
        final String[] ret = new String[len];
        System.arraycopy(orig, 0, ret, 0, len);
        return ret;
    } // end method

    /**
     * Checks whether the given string has a matching set of parenthesis (for our purposes, brackets only).
     *
     * <br><br><strong>Example:</strong>
     * <pre>
     * {@code
     * // as used in the Permissions class
     * String query = "(I open but I won't close";
     *
     * if (!Utils.CheckParentesis(query)) {
     *   Bukkit.getLogger().severe("[EasyChat] Invalid permissions query check passed to the checkPerms() method: " + query);
     *   return false;
     * }
     * }
     * </pre>
     *
     * @param str The actual query string to count matching brackets for.
     *
     * @return Returns true if the given query string has a matching number of brackets, false otherwise.
     */
    @SuppressWarnings({"ConstantConditions", "PointlessBooleanExpression"})
    public static boolean CheckParentesis(final String str) {
        // empty string will always have matching parenthesis count :P
        if (str.isEmpty()) {
            return true;
        }

        // create a new stack to store opening and closing parenthesis found
        final Stack<Character> stack = new Stack<Character>();

        // iterate over all characters
        for (int i = 0; i < str.length(); i++) {
            final char current = str.charAt(i);
            //if (current == '{' || current == '(' || current == '[')
            if ('(' == current) {
                // store opening parenthesis
                stack.push('(');
            }


            //if (current == '}' || current == ')' || current == ']')
            if (')' == current) {
                // closing bracket found but none was opened
                if (stack.isEmpty()) {
                    return false;
                }

                final char last = stack.peek();
                //if (current == '}' && last == '{' || current == ')' && last == '(' || current == ']' && last == '[')
                if ((')' == ')') && ('(' == last)) {
                    // we found a correct matching parenthesis
                    stack.pop();
                } else {
                    // the parenthesis found is not a matching pair - bail out
                    // note: this is only used when multiple parenthesis types are checked
                    return false;
                }
            }
        }

        // if the stack is empty, we found all the opening and closing parenthesis and they match
        return stack.isEmpty();
    } // end method

    /**
     * Method to join array elements of type string
     * @author Hendrik Will, imwill.com
     * @param inputArray Array which contains strings
     * @param glueString String between each array element
     * @return String containing all array elements seperated by glue string
     */
    public static String implode(Object[] inputArray, String glueString) {
        /** Output variable */
        String output = "";

        if (inputArray.length > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(inputArray[0]);

            for (int i=1; i<inputArray.length; i++) {
                sb.append(glueString);
                sb.append(inputArray[i]);
            }
            output = sb.toString();
        }
        return output;
    } // end method

    /**
     * Method to join list elements of type string
     * @author Hendrik Will, imwill.com, updated by Zathrus_Writer
     * @param listInputArray List&lt;String&gt; which contains strings
     * @param glueString String between each array element
     * @return String containing all array elements seperated by glue string
     */
    public static String implode(List<?> listInputArray, String glueString) {
        /** Output variable */
        Object[] inputArray = listInputArray.toArray();
        return implode(inputArray, glueString);
    } // end method

    /**
     * Makes the given string capitalized with the first letter being uppercased.
     *
     * @param txt The actual text to have capitalized.
     *
     * @return Returns a capitalized string, for example return "Hello" from the string "hello".
     */
    public static String capitalize(String txt) {
        return txt.substring(0, 1).toUpperCase() + txt.substring(1);
    } // end method

    /**
     * Removes all &a, &1, &b color codes from the message.
     *
     * @param message Message to remove ampersand chat colors from.
     *
     * @return Returns clear chat message without any ampersand color placeholders.
     */
    public static String remove_ampersand_chat_color_values( String message ) {
        return message.replaceAll(and_colors_pattern, "");
    } // end method

    /**
     * Removes all §a, §1, §b color codes from the message.
     *
     * @param message Message to remove chat color codes from.
     *
     * @return Returns clear chat message without any color placeholders.
     */
    public static String remove_chat_color_values( String message ) {
        return message.replaceAll(chat_colors_pattern, "");
    } // end method

    /**
     * Returns tab-completions for a command from the provided list of possible completion values.
     *
     * @param cmd_line_arg Argument on the command line that we want to auto-complete.
     * @param possible_values Possible values to auto-complete the argument with.
     *
     * @return Returns a sorted filtered list of possible command completions.
     */
    public static List<String> get_filtered_command_completions( String cmd_line_arg, List<String> possible_values ) {
        List<String> completions = new ArrayList<>();
        StringUtil.copyPartialMatches(cmd_line_arg, possible_values, completions);
        Collections.sort(completions);
        return completions;
    } // end method

    /**
     * Translates HEX chat colors into real colors for MC.
     *
     * @param message The actual message to translate HEX colors
     *
     * @return Returns a message formatted with HEX codes supported by the server.
     */
    public static String translateHexColorCodes(String message)
    {
        final Pattern hexPattern = Pattern.compile( hex_colors_pattern );
        Matcher matcher = hexPattern.matcher(message);
        StringBuffer buffer = new StringBuffer(message.length() + 4 * 8);
        while (matcher.find())
        {
            String group = matcher.group(1);
            matcher.appendReplacement(buffer, COLOR_CHAR + "x"
                + COLOR_CHAR + group.charAt(0) + COLOR_CHAR + group.charAt(1)
                + COLOR_CHAR + group.charAt(2) + COLOR_CHAR + group.charAt(3)
                + COLOR_CHAR + group.charAt(4) + COLOR_CHAR + group.charAt(5)
            );
        }
        return matcher.appendTail(buffer).toString();
    } // end method

    /**
     * Translates all known chat color codes into their color representations.
     *
     * @param message The message to translate chat colors in.
     *
     * @return Returns a message with color codes that can be used to send
     *         the message directy to player.
     */
    public static String translate_chat_colors( String message ) {
        // translate HEX colors first
        message = translateHexColorCodes( message );

        // then translate good old & colors
        message = ChatColor.translateAlternateColorCodes( '&', message );

        return message;
    } // end method


    /**
     * Reads a file line by line, returning it in a string.
     *
     * @param file_name File name to read.
     * @param replace_chat_colors If true, the & chat colors will be replaced in the resulting text
     *                            by real chat color representations.
     *
     * @return Empty string is returned if file does not exist, otherwise the file content is returned.
     */
    public static String read_file( String file_name, boolean replace_chat_colors ) {
        String ret     = "";

        try {
            FileInputStream fstream = new FileInputStream( file_name );
            BufferedReader  br      = new BufferedReader( new InputStreamReader( fstream ) );

            String strLine;
            while ( (strLine = br.readLine()) != null ) {
                if ( replace_chat_colors ) {
                    strLine = Utils.translate_chat_colors( strLine );
                }

                ret += strLine + "\n";
            }

            //Close the input stream
            fstream.close();
        } catch ( FileNotFoundException ex ) {
            // file does not (yet?) exist
            return "";
        } catch ( IOException e ) {
            // error while reading file - this should get displayed on console
            e.printStackTrace();
        }

        // replace all placeholders in the file
        return ret;
    } // end method

    /**
     * Replaces all matched translation placeholders ( ${placeholder} )
     * by their real values from the current language file.
     *
     * @param txt The text to replace translation placeholders in.
     *
     * @return Returns the given text with translation placeholders replaced by their real values.
     */
    public static String replace_translation_placeholders( String txt ) {
        final Pattern pattern = Pattern.compile("\\$\\{([^}]+)\\}");
        final Matcher matcher = pattern.matcher( txt );
        while ( matcher.find() ) {
            txt = txt.replace( matcher.group(0), EC_API.__( matcher.group(1) ) );
        }

        return txt;
    } // end method

} // end class