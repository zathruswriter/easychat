package com.martinambrus.easyChat;

import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.sql.*;
import java.util.*;
import java.util.Date;

public class LocalStorage {

    // instance of EasyChat
    private Plugin plugin;

    // SQLite connection
    private Connection conn;

    /***
     * Constructor, sets the main plugin class locally and initiates an SQLite connection.
     * @param p EasyChat instance.
     */
    LocalStorage(Plugin p) {
        this.plugin = p;
        try {
            Class.forName("org.sqlite.JDBC");
            this.conn = DriverManager.getConnection("jdbc:sqlite:" + p.getDataFolder() + File.separatorChar + "player_data.db");
        } catch (Throwable e) {
            this.conn = null;
            EC_API.generateConsoleWarning( EC_API.__( "error.localstorage-cant-connect" ) );

            if ( EC_API.getDebug() ) {
                e.printStackTrace();
            }
        }

        // create tables
        query("CREATE TABLE IF NOT EXISTS players (player_uuid varchar(36) NOT NULL, channel int(25) NOT NULL, subscriptions TEXT NULL, PRIMARY KEY (player_uuid))" );
        query("CREATE TABLE IF NOT EXISTS ignores (player_uuid varchar(36) NOT NULL, ignored_uuid varchar(36) NOT NULL, PRIMARY KEY (player_uuid, ignored_uuid))" );
    }

    /***
     * Closes connection on plugin disable.
     */
    void onDisable() {
        try {
            conn.close();
        } catch (Exception ex) {
            EC_API.generateConsoleWarning( "error.localstorage-cant-close" );

            if ( EC_API.getDebug() ) {
                ex.printStackTrace();
            }
        }
    }

    /***
     * Executes a prepared query that doesn't return a resultset.
     *
     * @param query The query to run on database.
     * @param params Any parameters that we need to pass to the query.
     *
     * @return Returns true if the query was executed successfully, false otherwise.
     */
    boolean query(String query, Object... params) {
        if ( null == this.conn ) {
            return false;
        }

        // no parameters, execute simple query
        if (params.length == 0) {
            try {
                Statement stat = conn.createStatement();
                stat.executeUpdate(query);
                stat.close();
            } catch (Throwable e) {
                EC_API.generateConsoleWarning( EC_API.__("error.localstorage-query-error") );

                if ( EC_API.getDebug() ) {
                    e.printStackTrace();
                }

                return false;
            }
        } else {
            // if we have only 1 parameter that is an ArrayList, make an array of objects out of it
            if ((params.length == 1) && ((params[0] instanceof List) || (params[0] instanceof ArrayList))) {
                params = ((List<?>) params[0]).toArray();
            }

            try {
                PreparedStatement prep = conn.prepareStatement(query);
                Integer           i    = 1;
                for (Object o : params) {
                    if (o instanceof Integer) {
                        prep.setInt(i, (Integer)o);
                    } else if (o instanceof String) {
                        prep.setString(i, (String)o);
                    } else if (o instanceof Double) {
                        prep.setDouble(i, (Double)o);
                    } else if (o instanceof Float) {
                        prep.setFloat(i, (Float)o);
                    } else if (o instanceof Long) {
                        prep.setLong(i, (Long)o);
                    } else if (o instanceof Boolean) {
                        prep.setBoolean(i, (Boolean)o);
                    } else if (o instanceof Date) {
                        prep.setTimestamp(i, new Timestamp(((Date) o).getTime()));
                    } else if (o instanceof Timestamp) {
                        prep.setTimestamp(i, (Timestamp) o);
                    } else if (o == null) {
                        prep.setNull(i, 0);
                    } else {
                        // unhandled variable type
                        EC_API.generateConsoleWarning( EC_API.__("error.localstorage-query-error") );

                        if ( EC_API.getDebug() ) {
                            EC_API.generateConsoleWarning( "Unknown variable type: " + o );
                        }

                        prep.clearBatch();
                        prep.close();
                        return false;
                    }
                    i++;
                }

                prep.addBatch();
                conn.setAutoCommit(false);
                prep.executeBatch();
                conn.commit();
                prep.close();
                prep = null;
            } catch (Throwable e) {
                EC_API.generateConsoleWarning( EC_API.__("error.localstorage-query-error") );

                if ( EC_API.getDebug() ) {
                    e.printStackTrace();
                }

                return false;
            }
        }

        return true;
    }

    /***
     * Executes a selection SQL statement and returns a resultset.
     *
     * @param query Query to execute.
     * @param params Any parameters we want to pass to the query.
     *
     * @return Returns a ResultSet for the given query.
     */
    ResultSet query_res(String query, Object... params) {
        if ( null == conn ) {
            return null;
        }

        if (params.length == 0) {
            try {
                Statement stat = conn.createStatement();
                ResultSet res = stat.executeQuery(query);
                return res;
            } catch (Throwable e) {
                EC_API.generateConsoleWarning( EC_API.__("error.localstorage-write-error") );

                if ( EC_API.getDebug() ) {
                    e.printStackTrace();
                }
            }
        } else {
            // if we have only 1 parameter that is an ArrayList, make an array of objects out of it
            if ((params.length == 1) && ((params[0] instanceof List) || (params[0] instanceof ArrayList))) {
                params = ((List<?>) params[0]).toArray();
            }

            try {
                PreparedStatement prep = conn.prepareStatement(query);
                Integer i = 1;
                for (Object o : params) {
                    if (o instanceof Integer) {
                        prep.setInt(i, (Integer)o);
                    } else if (o instanceof String) {
                        prep.setString(i, (String)o);
                    } else if (o instanceof Double) {
                        prep.setDouble(i, (Double)o);
                    } else if (o instanceof Float) {
                        prep.setFloat(i, (Float)o);
                    } else if (o instanceof Long) {
                        prep.setLong(i, (Long)o);
                    } else if (o == null) {
                        prep.setNull(i, 0);
                    } else {
                        EC_API.generateConsoleWarning( EC_API.__("error.localstorage-query-error") );

                        if ( EC_API.getDebug() ) {
                            EC_API.generateConsoleWarning( "Unknown variable type: " + o );
                        }

                        prep.close();
                        return null;
                    }
                    i++;
                }
                return prep.executeQuery();
            } catch (Throwable e) {
                EC_API.generateConsoleWarning( EC_API.__("error.localstorage-write-error") );

                if ( EC_API.getDebug() ) {
                    e.printStackTrace();
                }
            }
        }

        return null;
    }

    /**
     * Retrieves last used channel for the given player.
     *
     * @param player Player to retrieve the channel for.
     *
     * @return Returns last used channel for the given player.
     */
    String get_player_channel( Player player ) {
        if ( null != this.conn ) {
            ResultSet res = this.query_res( "SELECT channel FROM players WHERE player_uuid = \"" + player.getUniqueId() + "\" LIMIT 1" );
            try {
                return res.getString( "channel" );
            } catch (SQLException ex) {
                EC_API.generateConsoleWarning( EC_API.__("error.localstorage-query-error") );

                if ( EC_API.getDebug() ) {
                    ex.printStackTrace();
                }

                return null;
            }
        } else {
            // get this from player meta if our DB doesn't work
            for ( MetadataValue meta : player.getMetadata("easychat_default_channel") ) {
                if ( meta.getOwningPlugin().equals(this.plugin) ) {
                    return meta.asString();
                }
            }

            return null;
        }
    } // end method

    /**
     * Stores last used channel for the given player.
     *
     * @param player Player to store the channel for.
     */
    void set_player_channel( Player player, String channel_name ) {
        if ( null != this.conn ) {
            this.query( "INSERT OR IGNORE INTO players VALUES(\"" + player.getUniqueId() + "\", \"" + channel_name + "\", \"\")" );
            this.query( "UPDATE players SET channel = \"" + channel_name + "\" WHERE player_uuid = \"" + player.getUniqueId() + "\"" );
        } else {
            // store this in player meta if our DB doesn't work
            player.setMetadata("easychat_default_channel", new FixedMetadataValue( this.plugin, channel_name ));
        }
    } // end method

    /**
     * Retrieves channel subscriptions for the given player.
     *
     * @param player Player to retrieve the channel for.
     *
     * @return Returns channel subscriptions for the given player.
     */
    List<String> get_player_subscriptions( Player player ) {
        if ( null != this.conn ) {
            ResultSet res = this.query_res( "SELECT subscriptions FROM players WHERE player_uuid = \"" + player.getUniqueId() + "\" LIMIT 1" );
            try {
                return Arrays.asList( res.getString("subscriptions").split(",") );
            } catch (SQLException ex) {
                EC_API.generateConsoleWarning( EC_API.__("error.localstorage-query-error") );

                if ( EC_API.getDebug() ) {
                    ex.printStackTrace();
                }

                return null;
            }
        } else {
            // get this from player meta if our DB doesn't work
            for ( MetadataValue meta : player.getMetadata("easychat_channel_subscriptions") ) {
                if (meta.getOwningPlugin().equals(this.plugin)) {
                    // split subscriptions and check them 1 by 1
                    return Arrays.asList(meta.asString().split(","));
                }
            }

            return null;
        }
    } // end method

    /**
     * Stores subscriptions for the given player.
     *
     * @param player Player to store subscriptions for.
     */
    void set_player_subscriptions( Player player, String subscriptions ) {
        if ( null != this.conn ) {
            this.query( "INSERT OR IGNORE INTO players VALUES(\"" + player.getUniqueId() + "\", \"\", \"" + subscriptions + "\")" );
            this.query( "UPDATE players SET subscriptions = \"" + subscriptions + "\" WHERE player_uuid = \"" + player.getUniqueId() + "\"" );
        } else {
            // store this in player meta if our DB doesn't work
            player.setMetadata("easychat_channel_subscriptions", new FixedMetadataValue(this.plugin, subscriptions ));
        }
    } // end method

    /**
     * Retrieves ignore list for the given player.
     *
     * @param player Player to retrieve the ignore list for.
     *
     * @return Returns ignore list for the given player.
     */
    Map<String, Boolean> get_player_ignores_list( Player player ) {
        if ( null != this.conn ) {
            ResultSet res = this.query_res( "SELECT * FROM ignores WHERE player_uuid = \"" + player.getUniqueId() + "\" LIMIT 1" );
            try {
                Map<String, Boolean> ignores = new HashMap<>();
                UUID player_uuid = player.getUniqueId();
                while ( res.next() ) {
                    ignores.put( player_uuid + "_" + res.getString("ignored_uuid"), true );
                }

                return ignores;
            } catch (SQLException ex) {
                EC_API.generateConsoleWarning( EC_API.__("error.localstorage-query-error") );

                if ( EC_API.getDebug() ) {
                    ex.printStackTrace();
                }

                return new HashMap<>();
            }
        } else {
            // player ignores can likely outgrow player meta, so just return an empty list
            return new HashMap<>();
        }
    } // end method

    /**
     * Stores ignore list for the given player.
     *
     * @param player Player to store ignore list for.
     */
    void set_player_ignores_list( Player player, List<String> ignores ) {
        if ( null != this.conn ) {
            for ( String ignore : ignores ) {
                this.query( "INSERT OR IGNORE INTO ignores VALUES(\"" + player.getUniqueId() + "\", \"" + ignore + "\")" );
            }

            // remove ignores not in this list
            this.query( "DELETE FROM ignores WHERE player_uuid = \"" + player.getUniqueId() + "\" AND ignored_uuid NOT IN (\"" + Utils.implode( ignores, ",") + "\")" );
        }
    } // end method
}
