package com.martinambrus.easyChat;

import com.martinambrus.easyChat.events.bungee.ECReloadEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.event.EventHandler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Spigot plugin version checker. Compares latest version on Spigot
 * with the current one and warns in console and chatAliasing as needed.
 *
 * @author Martin Ambrus
 */
public final class UpdaterBungee implements Runnable, Listener {

    /**
     * Is set to FALSE after first display of the "getting update info" text.
     * This is to prevent console spam when update interval is too short.
     */
    private boolean firstRun = true;

    /**
     * URL of this resource on Spigot.
     */
    @SuppressWarnings("HardCodedStringLiteral")
    private final String spigotURL = "https://www.spigotmc.org/resources/easychat-go-crazy-with-your-chat.88859/";

    /**
     * API URL for this resource on Spigot to check for new versions.
     */
    private final String spigotAPICheckURL = "https://api.spigotmc.org/legacy/update.php?resource=88859";

    /**
     * {@link EasyChat_Bungee EasyChat_Bungee} instance.
     */
    private final EasyChat_Bungee plugin;

    /**
     * Scheduled task ID, used for periodic update checks.
     */
    private ScheduledTask scheduledTaskID = null;

    /**
     * True if we registered this class to listen for player joins,
     * false otherwise.
     */
    private boolean eventListenerRegistered = false;

    /**
     * Constructor.
     * Starts listening to player join events to possibly
     * inform them about a new version availability.
     *
     * @param ec {@link EasyChat_Bungee EasyChat_Bungee} instance.
     */
    UpdaterBungee( final EasyChat_Bungee ec) {
        plugin = ec;

        // do all the things that are usually done when ec_reload is performed
        onReload();
    } // end method

    /**
     * Asks the Spigot HTTP API for newest version of this plugin
     * and notifies console if any are found.
     */
    @Override
    public void run() {
        if (firstRun) {
            plugin.getLogger().info("Checking for Updates...");
        }

        try {
            final HttpURLConnection con = (HttpURLConnection) new URL(this.spigotAPICheckURL).openConnection();

            con.setDoOutput(true);
            con.setRequestMethod("GET"); //NON-NLS

            final String version = new BufferedReader(new InputStreamReader(con.getInputStream())).readLine();
            // only version number would be the output of this, so it'll always be less than 10 characters
            if (10 >= version.length()) {
                final String ecVersion = plugin.getDescription().getVersion();
                final VersionComparator cmp = new VersionComparator();

                if (cmp.compare(version, ecVersion) == 1) {
                    plugin.getLogger().warning( "There is a new version (" + version + ") available at " + spigotURL + " - you are using version " + ecVersion );
                } else {
                    if (firstRun) {
                        plugin.getLogger().info("No new version available." );
                    }
                }
            } else {
                plugin.getLogger().warning( "Failed to check for new version on Spigot. Maybe the Spigot API or website is down?" );
            }
        } catch (final Exception ex) {
            plugin.getLogger().warning( "Failed to check for new version on Spigot. Maybe the Spigot API or website is down?" );
        }

        firstRun = false;
    } // end method

    /**
     * Registers event listener for the join event and starts
     * a scheduled task that will check for this plugin's updated
     * version online every hour.
     *
     * @param forceShowVersionInfo If set, an one-time update check will be triggered.
     */
    void onReload(final boolean... forceShowVersionInfo) {
        if (!eventListenerRegistered) {
            plugin.getProxy().getPluginManager().registerListener(this.plugin, this);
            eventListenerRegistered = true;
        }

        int period = 7200;

        // run periodically
        if (null == scheduledTaskID) {
            plugin.getProxy().getScheduler().schedule(this.plugin, this, 1, period, TimeUnit.SECONDS);
        } else if (0 < forceShowVersionInfo.length) {
            plugin.getProxy().getScheduler().runAsync( this.plugin, new Runnable() {
                @Override
                public void run() {
                    UpdaterBungee.this.run();
                }
            });
        }
    } // end method

    /***
     * Disables update checking. Used when disabling the plugin.
     */
    void unregister() {
        if (null != scheduledTaskID) {
            scheduledTaskID.cancel();
            scheduledTaskID = null;
        }
    } // end method

    @EventHandler
    public void reload( ECReloadEvent event ) {
        run();
    } // end method

} // end class