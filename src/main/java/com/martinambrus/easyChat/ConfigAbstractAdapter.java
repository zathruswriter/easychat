package com.martinambrus.easyChat;

import org.bukkit.event.Listener;
import org.bukkit.plugin.PluginDescriptionFile;

import java.util.*;

/**
 * Base configuration class containing all common functionality
 * and properties for extending config sub-classes.
 *
 * @author Martin Ambrus
 */
@SuppressWarnings("HardCodedStringLiteral")
abstract class ConfigAbstractAdapter implements Listener {

    /**
     * Instance of {@link EasyChat}.
     */
    EasyChat plugin = null;

    /**
     * Plugin description file, contains things like commands and permissions
     * from the plugin.yml file of this plugin.
     */
    transient PluginDescriptionFile yml = null;

    /**
     * Holds the text "EasyChat" for logging purposes.
     */
    String pluginName = "EasyChat";

    /**
     * List of all modules that are currently disabled in EasyChat's user config,
     * i.e. config.yml file in Plugins folder.
     */
    final Collection<String> disabledModules = new ArrayList<String>();

    /**
     * ENUM of some valid configuration values
     * to prevent duplication of string literals in code.
     */
    enum CONFIG_VALUES {
        /**
         * Whether or not the debug mode is enabled.
         */
        DEBUGENABLED(
                "modules.debug-mode-command.debug-mode-active"
                ),
        /*
          Language translation string from config.
         */
        /*LANGVISUALIZERDISABLED(
                "lang.visualizerDisabled"
        ),*/
        ;

        /**
         * The string value of an ENUM.
         */
        private final String configValue;

        /**
         * Constructor for String ENUMs.
         * @param value String value for the ENUM.
         */
        CONFIG_VALUES(final String value) {
            configValue = value;
        }

        /* (non-Javadoc)
         * @see java.lang.Enum#toString()
         */
        @Override
        public String toString() {
            return configValue;
        }

        private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
            throw new java.io.NotSerializableException("com.martinambrus.easyChat.Config.CONFIG_VALUES");
        }
    }

    /**
     * Plugin debug mode flag.
     */
    boolean debug = false;

    /**
     * Configuration identifiers and their respective file names.
     */
    final HashMap<String, String> configs = new HashMap<String, String>() {{
        put("main", "config-file.yml");
    }};

    /**
     * Performs data type validation on values coming from a user-defined config file
     * and generates console warnings if it's not the type we need.
     *
     * @param class_config_option The actual configuration variable from this class.
     * @param config_path The full path to the option's value in the config.
     * @param config_section_name The name for which informative error messages will be displayed.
     *
     * @return Returns the same value if an error has occurred or a value from the configuration
     *         if we were able to retrieve the correct value type.
     */
    Object validateConfigOptionValue(Object class_config_option, String config_path, String config_section_name) {
        try {
            if ( class_config_option instanceof Boolean ) {
                return (Boolean) this.getValueFromConfig(config_path, class_config_option);
            } else if ( class_config_option instanceof String ) {
                return (String) this.getValueFromConfig(config_path, class_config_option);
            } else if ( class_config_option instanceof Integer ) {
                Object o = this.getValueFromConfig(config_path, class_config_option);
                if ( o instanceof Integer ) {
                    return (Integer) o;
                } else {
                    return Integer.parseInt( (String) o );
                }
            } else if ( class_config_option instanceof List) {
                return (List) this.getValueFromConfig(config_path, class_config_option);
            }
        } catch (ClassCastException ex) {
            // value remains at its default but warn server owner
            String[] path_exploded = config_path.split("\\.");
            if ( class_config_option instanceof Boolean ) {
                EC_API.generateConsoleWarning( EC_API.__( "error.channel-invalid-boolean-in-config", config_section_name, path_exploded[ path_exploded.length - 1 ] ) );
            } else if ( class_config_option instanceof String ) {
                EC_API.generateConsoleWarning( EC_API.__( "error.channel-invalid-string-in-config", config_section_name, path_exploded[ path_exploded.length - 1 ] ) );
            } else if ( class_config_option instanceof Integer ) {
                EC_API.generateConsoleWarning( EC_API.__( "error.channel-invalid-number-in-config", config_section_name, path_exploded[ path_exploded.length - 1 ] ) );
            } else if ( class_config_option instanceof List ) {
                EC_API.generateConsoleWarning( EC_API.__( "error.channel-invalid-list-in-config", config_section_name, path_exploded[ path_exploded.length - 1 ] ) );
            }
        }

        return class_config_option;
    }

    /**
     * Returns a configuration value from the config file, if present.
     * Otherwise returns the same default value.
     *
     * @param section Path to the configuration section to get the value from.
     * @param default_value Default value to return if the config section does not exist.
     *
     * @return Returns string value given by the section name or value of default_value
     *         if that configuration section does not exist.
     */
    Object getValueFromConfig(String section, Object default_value ) {
        List<String> test_for_string_list = EC_API.getConfigStringList( section );
        Object val = EC_API.getConfigString( section );
        if ( null == val ) {
            return default_value;
        } else {
            // check that we don't have a string list instead of a string
            boolean is_list = (test_for_string_list.toString().equals( val ));
            if ( !is_list && ( val.equals("true") || val.equals("false") ) ) {
                return ( val.equals("true") ? true : false );
            } else {
                if ( is_list ) {
                    // we've got a config string list
                    return test_for_string_list;
                } else {
                    // we've got a config string
                    return val;
                }
            }
        }
    }

    /**
     * Gets this plugin's debug state.
     *
     * @return Returns true if debug is turned ON, false otherwise.
     */
    boolean getDebug() {
        return debug;
    } //end method

    /**
     * Sets this plugin's debug state.
     *
     * @param value New value for plugin's debug state - true to turn ON, false to turn OFF.
     */
    abstract void setDebug(final boolean value);

    /**
     * Gets the plugin's display name, as it's being used in many places for logging purposes.
     *
     * @return Returns the text "EasyChat".
     */
    String getPluginName() {
        return pluginName;
    } //end method

    /**
     * Checks whether a module of EasyChat is disabled.
     *
     * @param module The name of the module to check for disabled status.
     * @return Returns true if the module is disabled, false otherwise.
     */
    boolean isDisabled(final String module) {
        return disabledModules.contains(module);
    } //end method

    /**
     * Gets internal YAML plugin configuration for EasyChat.
     *
     * @return Returns the internal YAML plugin configuration for EasyChat
     * from the plugin.yml file located inside the JAR file of the plugin.
     */
    PluginDescriptionFile getInternalConf() {
        return yml;
    } //end method

    /**
     * Gets user plugin configuration for EasyChat.
     *
     * @return Returns plugin configuration for EasyChat, loaded either from
     * the config.yml file located in [pluginsFolder]/EasyChat
     * or from a DB backend.
     */
    abstract ConfigSectionAbstractAdapter getConf();

    /***
     * Closes any DB or file connections that the config class
     * have open upon disabling the plugin.
     */
    abstract void onClose();

    /**
     * Reloads configuration from the source, overwriting any old values we might have cached in memory.
     * Used when EasyChat is being disabled to prevent storing old values into the DB.
     */
    abstract void reloadConfig(); // end method

} // end class