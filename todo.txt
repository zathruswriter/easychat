    BungeeCord support
    Global Chat to communicate between any number of your servers
    Local Chat if you wish to only see the messages of people on 1 server
    Private Messaging between any number of your servers
    Group Chats that your players can make and customise to socialise with their friends
    Network Join and Quit messages
    Execute commands over all your servers at once!
    cross-server broadcast
    use & chat colors in chat by players, based on their permissions (easy to implement, needs some performance testing to make sure cached permissions can be used)